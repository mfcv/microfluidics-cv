#############################################################################
#  Copyright (C)  2019 - Simon Dallaire
#
#  This program is free software: you can redistribute it and/or modify
#  it under the terms of the GNU Affero General Public License as
#  published by the Free Software Foundation, either version 3 of the
#  License, or (at your option) any later version.
#
#  This program is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#  GNU Affero General Public License for more details.
#
#  You should have received a copy of the GNU Affero General Public License
#  along with this program.  If not, see <https://www.gnu.org/licenses/>.
#
#############################################################################
cmake_minimum_required(VERSION 3.13.0) # to enable policy CMP0077
cmake_policy(SET CMP0077 NEW)

project(MicrofluidcsCV
        VERSION 0.0
        DESCRIPTION "MicrofluidicsCV helps microfluidics researchers to gather and analyze data."
        LANGUAGES CXX)

set(CMAKE_AUTOMOC ON)
set(CMAKE_AUTORCC ON)
find_package(Qt5 COMPONENTS Core REQUIRED)
set(CMAKE_CXX_STANDARD 17)
set(CMAKE_CXX_STANDARD_REQUIRED ON)
set(CMAKE_INCLUDE_CURRENT_DIR ON) # Find includes in corresponding build directories
set(EXECUTABLE_OUTPUT_PATH "${CMAKE_CURRENT_SOURCE_DIR}/${CMAKE_BUILD_TYPE}")

#######################################
# Custom options
option(WITH_PYLON "WITH_PYLON" OFF)
#######################################

#######################################
# Enable optional lib
if(WITH_PYLON)
    add_compile_definitions(WITH_PYLON)
endif()
#######################################

#######################################
# Custom variables
set(EXE_NAME MicrofluidcsCV)
set(TEST_EXE_NAME test-m)
set(MFCV_LIB_NAME mfcv)
#######################################

#######################################
# Custom content to fetch
include(FetchContent)
FetchContent_Declare(
  googletest
  GIT_REPOSITORY "https://github.com/google/googletest.git"
  GIT_TAG        release-1.8.1
)
#FIXME Merge warnings fix to the main project and use the flag
FetchContent_Declare(
  g3log
  GIT_REPOSITORY https://github.com/dallaires/g3log
  GIT_TAG        1.3.x
)
FetchContent_Declare(
  g3sinks
  GIT_REPOSITORY https://github.com/dallaires/g3sinks
  GIT_TAG        1.1.1
)
# TODO Maybe a partial build of opencv or use distro version instead.
#FetchContent_Declare(
#  opencv_contrib
#  GIT_REPOSITORY https://github.com/opencv/opencv_contrib.git
#  GIT_TAG        4.2.0
#)
#FetchContent_Declare(
#  opencv
#  GIT_REPOSITORY https://github.com/opencv/opencv.git
#  GIT_TAG        4.2.0
#)
# TODO fetch from gitlab if allowed by the license
#FetchContent_Declare(
#  pylon
#  URL "https://www.baslerweb.com/fp-1535524609/media/downloads/software/pylon_software/pylon-5.1.0.12682-x86_64.tar.gz"
#)

#######################################
# Fetch macro
if(${CMAKE_VERSION} VERSION_LESS 3.14)
    macro(FetchContent_MakeAvailable NAME)
        FetchContent_GetProperties(${NAME})
        if(NOT ${NAME}_POPULATED)
            FetchContent_Populate(${NAME})
            add_subdirectory(${${NAME}_SOURCE_DIR} ${${NAME}_BINARY_DIR})
        endif()
    endmacro()
endif()

macro(FetchContent_g3log)
FetchContent_GetProperties(g3log)
if(NOT g3log_POPULATED)
  message("============ Populating g3log ============")
  FetchContent_Populate(g3log)
  # Default options
  #ADD_FATAL_EXAMPLE:BOOL=ON
  #ADD_G3LOG_BENCH_PERFORMANCE:BOOL=OFF
  #ADD_G3LOG_UNIT_TEST:BOOL=OFF
  #CHANGE_G3LOG_DEBUG_TO_DBUG:BOOL=OFF
  #ENABLE_FATAL_SIGNALHANDLING:BOOL=ON
  #G3_IOS_LIB:BOOL=OFF
  #G3_LOG_FULL_FILENAME:BOOL=OFF
  #G3_SHARED_LIB:BOOL=ON
  #G3_SHARED_RUNTIME:BOOL=ON
  #USE_DYNAMIC_LOGGING_LEVELS:BOOL=OFF
  #USE_G3_DYNAMIC_MAX_MESSAGE_SIZE:BOOL=OFF

  # ------------ Custom options ------------- #
  set(ADD_FATAL_EXAMPLE OFF CACHE BOOL "Disable fatal example")
  add_subdirectory(${g3log_SOURCE_DIR} ${g3log_BINARY_DIR})
  message("============ g3log populated ============")
endif()
endmacro()

macro(target_link_g3logrotate TARGET)
FetchContent_g3log()
FetchContent_GetProperties(g3sinks)
if(NOT g3sinks_POPULATED)
  message("============ Populating g3sinks ============")
  FetchContent_Populate(g3sinks)
  # Default options
  #ADD_LOGROTATE_UNIT_TEST:BOOL=OFF
  #G3LOG_LIBRARY:FILEPATH=/usr/lib/libg3logger.so
  #set(BUILD_STATIC ON CACHE BOOL "Link static")

  # ------------ Custom options ------------- #
  set(G3LOG_LIBRARY "${g3log_BINARY_DIR}/libg3logger.so" CACHE STRING "Enable full filename")
  add_subdirectory(${g3sinks_SOURCE_DIR}/logrotate ${g3sinks_BINARY_DIR})
  target_include_directories(g3logrotate PUBLIC "${g3log_SOURCE_DIR}/src")
  target_include_directories(g3logrotate PUBLIC "${g3log_BINARY_DIR}/include")
  message("============ g3sinks populated  ============")
endif()
target_link_libraries("${TARGET}" PUBLIC g3logger g3logrotate)
target_include_directories("${TARGET}" PRIVATE "${g3sinks_SOURCE_DIR}/logrotate/src")
endmacro()

#######################################
# Enable all warnings and treat then as errors

# FIXME ensure portability
set(CMAKE_CXX_FLAGS_DEBUG "${CMAKE_CXX_FLAGS_DEBUG} -Wall -Wextra -Werror -pedantic-errors")
set(CMAKE_CXX_FLAGS_RELEASE "${CMAKE_CXX_FLAGS_RELEASE} -Wall -Wextra -Werror -pedantic-errors -O3")


# maybe add GCC option for dev release  (-march=native is for local machine)
# https://stackoverflow.com/questions/36605576/which-gcc-optimization-flags-should-i-use

# Old flag settings
# Source: https://foonathan.net/blog/2018/10/17/cmake-warnings.html
#set(MFCV_COMPILE_OPTIONS 
#    # clang/GCC warnings
#    $<$<OR:$<CXX_COMPILER_ID:Clang>,$<CXX_COMPILER_ID:GNU>>:
#      "-Wall" "-Wextra" "-Werror" "-pedantic-errors" >
#    # MSVC warnings
#    $<$<CXX_COMPILER_ID:MSVC>:
#      "/W4" "/Wx" >
#)
#set(MFCV_COMPILE_OPTIONS_RELEASE
#    # clang/GCC warnings
#    $<$<OR:$<CXX_COMPILER_ID:Clang>,$<CXX_COMPILER_ID:GNU>>:
#      "-O3" >
#    # MSVC warnings
#    $<$<CXX_COMPILER_ID:MSVC>:
#      "/00" >
#)

#######################################

add_subdirectory("src")
