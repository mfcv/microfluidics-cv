MicrofluidicsCV
=================

# Table of contents

1. [Introduction](#1-introduction)
2. [Installation](#2-installation)
3. [Building MicrofluidicsCV](#3-building-microfluidicscv)
    1. [Building inside Docker](#31-building-inside-docker)

# 1. Introduction
MicrofluidicsCV is meant to helps researchers to acquire and analyze data from a digital camera for microfluidics research.

# 2. Installation
This project must be compile from source. There is no user interface yet, so the
source code must be updated and recompile to change it behavior.

# 3 Building MicrofluidicsCV
Right now Ubuntu 18.04 is officially supported. To instal dependencies, open a
terminal then run:
```bash
    ConfigDoc/Ubuntu/InstallDependencies.sh;
    mkdir build;
    cd build;
    cmake ..;
    make
```
## 3.1 Building inside Docker

```bash
# Build the Docker image.
reset && docker build --tag mfcv-dev ConfigDoc/Docker;
# Make a build directory.
mkdir docker-build && cd docker-build;
# Create project inside Docker with CMake:
docker run --rm -v "$PWD":/usr/src/myapp  \
     -v "$PWD"/docker-build:/usr/src/myapp/build \
     -w /usr/src/myapp/build \
     mfcv-dev cmake ..;
# Build inside Docker with make:
docker run --rm -v "$PWD":/usr/src/myapp \
     -v "$PWD"/docker-build:/usr/src/myapp/build \
     -w /usr/src/myapp/build \
     mfcv-dev make;
# Run inside Docker
xhost +; reset; docker run --rm -it -v /tmp/.X11-unix:/tmp/.X11-unix \
    -e DISPLAY=$DISPLAY -e XDG_RUNTIME_DIR=$XDG_RUNTIME_DIR \
    -v $XDG_RUNTIME_DIR=$XDG_RUNTIME_DIR --device=/dev/video0:/dev/video0 \
    -v "$PWD":/usr/src/myapp -w /usr/src/myapp \
    mfcv-dev ./MicrofluidcsCV;
```