//////////////////////////////////////////////////////////////////////////////
/// \file       main.qml
/// \brief      Main MicrofluidicsCV GUI.
/// \copyright  2019 - Simon Dallaire (AGPLv3)
//////////////////////////////////////////////////////////////////////////////
//  This program is free software: you can redistribute it and/or modify
//  it under the terms of the GNU Affero General Public License as
//  published by the Free Software Foundation, either version 3 of the
//  License, or (at your option) any later version.
//
//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU Affero General Public License for more details.
//
//  You should have received a copy of the GNU Affero General Public License
//  along with this program.  If not, see <https://www.gnu.org/licenses/>.
//////////////////////////////////////////////////////////////////////////////

import QtQuick 2.12
import QtQuick.Window 2.12
import QtQuick.Dialogs 1.2
import QtQuick.Layouts 1.12
import QtMultimedia 5.12
import QtQuick.Controls 2.12

import mfcv.gui 1.0
// FIXME Move all logical code in C++ (ex: sliders)
// FIXME Create sub QML files
ApplicationWindow {
    GuiLogger {
        id: guiConsole
        onUpdated: {
            consoleText.append(text)
        }
    }
    Icons {
        id: icons
    }
    Greek {
        id: greek
    }
    VideoSource {
        id: videoSource
        onSourceChanged: {
            visionEngine.setROI(0,0, 0, 0);
            roiXY0.text = visionEngine.ROI_X0 + "," + visionEngine.ROI_Y0;
            roiXY1.text = visionEngine.ROI_X1 + "," + visionEngine.ROI_Y1;
        }
    }
    VideoRecorder {
        id: videoRecorder
        onFrameListenerChanged: {
            videoSource.addRawFrameListener(callbackRawPtr)
        }
    }
    VideoRecorder {
        id: transformedVideoRecorder
        customRecordIcon: icons.EN_VIDEO
        fileSufix: "_Transformed"
        onFrameListenerChanged: {
            videoSource.addTransformedFrameListener(callbackRawPtr)
        }
    }
    VisionProxy {
        id: visionEngine
        onTransformListenerChanged: {
            videoSource.addTransform(callbackRawPtr)
        }
        onTransformChanged: {
            videoSource.refreshImage()
        }
    }
    FileDialog {
        id: videoFileDialog
        title: "Please choose a video file"
        folder: shortcuts.home
        selectFolder: false
        selectMultiple: false
        selectExisting: true
        onAccepted: {
            displayingContent.setText(videoFileDialog.fileUrl); // FIXME Display only the file name
            videoSource.openVideoFile(videoFileDialog.fileUrl);
        }
        onRejected: {
            displayingContent.setText("");
        }
        Component.onCompleted: visible = false
    }
    FileDialog {
            id: imageFileDialog
            title: "Please choose an image file"
            folder: shortcuts.home
            selectFolder: false
            selectMultiple: false
            selectExisting: true
            onAccepted: {
                displayingContent.setText(imageFileDialog.fileUrl); // FIXME Display only the file name
                videoSource.openImageFile(imageFileDialog.fileUrl);
            }
            onRejected: {
                displayingContent.setText("");
            }
            Component.onCompleted: visible = false
        }
    Dialog {
        id: cameraDialog
        title: "Please select a camera"

        contentItem: Rectangle {
            id: camDialogContent
            color: "#dddddd"
            implicitWidth: 400
            implicitHeight: 100
            ListView {
                id: camList
                model: videoSource.CamLst
                anchors.fill: parent
                highlight: Rectangle { color: "lightsteelblue"; radius: 5 }
                Keys.onReturnPressed: {
                    videoSource.openCamera(camList.currentIndex);
                    displayingContent.setText(camList.currentItem.text);
                    cameraDialog.close();
                }
                delegate: Text {
                    text: modelData
                    font.pixelSize: 20
                    MouseArea {
                        hoverEnabled: true
                        anchors.fill: parent
                        onClicked: {
                            videoSource.openCamera(index);
                            displayingContent.setText(modelData);
                            cameraDialog.close();
                        }
                        onContainsMouseChanged: { if(containsMouse) camList.currentIndex = index }
                    }
                }
            }
        }
        Component.onCompleted: visible = false
    }

    id: mainWindow
    color: "#000000"
    visible: true
    minimumWidth: 640
    minimumHeight: 480
    width: 840
    height: 540
    title: qsTr("MicrofluidicsCV")

    // Menu bar
    Rectangle {
        id: mainMenu
        height: 26
        color: "#dddddd"
        gradient: Gradient {
            GradientStop { position: 0.0; color: "#dddddd" }
            GradientStop { position: 0.85; color: "#dddddd" }
            GradientStop { position: 1.0; color: "#444444" }
        }
        anchors.top: parent.top
        anchors.right: parent.right
        anchors.left: parent.left
        RowLayout
        {
            anchors.left: parent.left
            anchors.leftMargin: 5
            anchors.top: parent.top
            anchors.bottom: parent.bottom
            anchors.bottomMargin: 4
            spacing: 10
            Text {
                id: fileButton
                text: "File"
                MouseArea {
                    anchors.fill: parent
                    acceptedButtons: Qt.LeftButton
                    onClicked: menu.open()
                    hoverEnabled: true
                }
                Menu {
                    id: menu
                    y: fileButton.height

                    MenuItem {
                        text: "New"
                        hoverEnabled: true
                    }
                    MenuItem {
                        text: "Open"
                        hoverEnabled: true
                    }
                    MenuItem {
                        text: "Save"
                        hoverEnabled: true
                    }
                }
            }
            Text {
                id: helpButton
                text: "Help"
                MouseArea {
                    anchors.fill: parent
                    acceptedButtons: Qt.LeftButton
                    onClicked: helpMenu.open()
                    hoverEnabled: true
                    onHoveredChanged: {
                        if(containsMouse) {

                        }
                    }
                }
                Menu {
                    id: helpMenu
                    y: fileButton.height

                    MenuItem {
                        text: "About"
                        hoverEnabled: true
                    }
                }
            }
        }
    }

    // Console and toobar
    Rectangle {
        id: bottomInfo
        x: 0
        y: 295
        width: 640
        height: 185
        anchors.right: sidebar.left
        anchors.left: parent.left
        anchors.bottom: parent.bottom

        //TODO Video progress bar

        // Menu bar
        Rectangle {
            id: menuBar
            y: 0
            height: 26
            color: "#ffffff"
            anchors.bottom: parent.bottom
            anchors.bottomMargin: 159
            anchors.right: parent.right
            anchors.left: parent.left
            RowLayout
            {
                anchors.fill: parent
                spacing: 3

                // TODO Must be able to chose between a file or a camera
                // TODO Fix focus: https://doc.qt.io/qt-5/qtquick-input-focus.html#acquiring-focus-and-focus-scopes
                // Input selector
                Rectangle {
                    id: inputSelector
                    color: "#dddddd"
                    Layout.fillWidth: true
                    Layout.fillHeight: true

                    // AW_IMAGE_FILE
                    Text {
                        id: iconOpenimage
                        color: "#000000"
                        text: icons.AW_IMAGE_FILE
                        renderType: Text.NativeRendering
                        verticalAlignment: Text.AlignVCenter
                        horizontalAlignment: Text.AlignHCenter
                        font.family: icons.FAM_AWESOME
                        font.pixelSize: 20

                        anchors.verticalCenter: parent.verticalCenter 
                        anchors.left: parent.left
                        anchors.leftMargin: 2

                        MouseArea {
                            hoverEnabled: true
                            anchors.fill: parent
                            onClicked: imageFileDialog.open();
                        }
                    }

                    // AW_VIDEO_FILE
                    Text {
                        id: iconOpencamera
                        color: "#000000"
                        text: icons.AW_VIDEO_FILE
                        renderType: Text.NativeRendering
                        verticalAlignment: Text.AlignVCenter
                        horizontalAlignment: Text.AlignHCenter
                        font.family: icons.FAM_AWESOME
                        font.pixelSize: 20

                        anchors.verticalCenter: parent.verticalCenter 
                        anchors.left: iconOpenimage.right
                        anchors.leftMargin: 2

                        MouseArea {
                            hoverEnabled: true
                            anchors.fill: parent
                            onClicked: videoFileDialog.open();
                        }
                    }

                    // icon-videocam
                    Text {
                        id: iconVideocam
                        color: "#000000"
                        text: icons.AW_CAMCORDER
                        renderType: Text.NativeRendering
                        verticalAlignment: Text.AlignVCenter
                        horizontalAlignment: Text.AlignHCenter
                        font.family: icons.FAM_AWESOME
                        font.pixelSize: 25

                        anchors.verticalCenter: parent.verticalCenter 
                        anchors.left: iconOpencamera.right
                        anchors.leftMargin: 2

                        MouseArea {
                            hoverEnabled: true
                            anchors.fill: parent
                            onClicked: { videoSource.refreshCamList(false); cameraDialog.open(); camList.focus = true}
                        }
                    }

                    Text {
                        id: displayingContent
                        anchors.verticalCenter: parent.verticalCenter 
                        anchors.left: iconVideocam.right
                        anchors.leftMargin: 2

                        text: "Waiting for input..."
                        font.pixelSize: 15

                        function setText(content) {
                            if(content == "" || content == "None") {
                                displayingContent.text = "Waiting for input..."
                            }
                            else {
                                displayingContent.text = content
                            }
                        }
                    }
                }

                // icon-record
                Rectangle {
                    id: rectRecord
                    color: "#dddddd"
                    Layout.fillWidth: true
                    Layout.fillHeight: true

                    Text {
                        id: iconRecord
                        color: "#aa0000"
                        text: videoRecorder.RecordIcon
                        anchors.verticalCenter: parent.verticalCenter 
                        anchors.left: parent.left
                        anchors.leftMargin: 2
                        renderType: Text.NativeRendering
                        verticalAlignment: Text.AlignVCenter
                        horizontalAlignment: Text.AlignHCenter
                        font.family: videoRecorder.IconsFamily
                        font.pixelSize: 25

                        MouseArea {
                            hoverEnabled: true
                            anchors.fill: parent
                            onClicked: videoRecorder.toggleRecord(fileNameRecord.text)
                        }
                    }
                    Text {
                        id: iconRecordTransformedVideo
                        color: "#aa0000"
                        text: transformedVideoRecorder.RecordIcon
                        anchors.verticalCenter: parent.verticalCenter
                        anchors.left: iconRecord.right
                        anchors.leftMargin: 2
                        renderType: Text.NativeRendering
                        verticalAlignment: Text.AlignVCenter
                        horizontalAlignment: Text.AlignHCenter
                        font.family: transformedVideoRecorder.IconsFamily
                        font.pixelSize: 25

                        MouseArea {
                            hoverEnabled: true
                            anchors.fill: parent
                            onClicked: transformedVideoRecorder.toggleRecord(fileNameRecord.text)
                        }
                    }
                    Text {
                        id: iconPicture
                        color: "#aa0000"
                        text: icons.AW_CAMERA
                        anchors.verticalCenter: parent.verticalCenter 
                        anchors.left: iconRecordTransformedVideo.right
                        anchors.leftMargin: 2
                        renderType: Text.NativeRendering
                        verticalAlignment: Text.AlignVCenter
                        horizontalAlignment: Text.AlignHCenter
                        font.family: icons.FAM_AWESOME
                        font.pixelSize: 20

                        MouseArea {
                            hoverEnabled: true
                            anchors.fill: parent
                            onClicked: videoSource.takePicture(fileNameRecord.text)
                        }
                    }

                    TextInput {
                        id: fileNameRecord
                        anchors.verticalCenter: parent.verticalCenter 
                        anchors.left: iconPicture.right
                        anchors.leftMargin: 2
                        text: videoRecorder.defaultDir()
                        font.pixelSize: 15
                    }
                }
            }
        }

        // Console
        Rectangle {
            color: "#333333"
            anchors.top: menuBar.bottom
            anchors.right: parent.right
            anchors.bottom: parent.bottom
            anchors.left: parent.left
            anchors.topMargin: 0
            Flickable {
                id: flick
                anchors.fill: parent
                contentWidth: consoleText.paintedWidth
                contentHeight: consoleText.paintedHeight
                clip: true

                function ensureVisible(r)
                {
                    if (contentX+width <= r.x+r.width)
                        contentX = r.x+r.width-width;
                    if (contentY >= r.y)
                        contentY = r.y;
                    else if (contentY+height <= r.y+r.height)
                        contentY = r.y+r.height-height;
                }

                TextEdit {
                    id: consoleText
                    focus: true
                    color: "#eeeeee"
                    anchors.fill: parent
                    cursorVisible: true
                    readOnly: true
                    selectionColor: "#0000a0"
                    font.pixelSize: 14
                    selectByMouse: true
                    textFormat: TextEdit.RichText
                    onCursorRectangleChanged: flick.ensureVisible(cursorRectangle)
                }
            }
        }
    }

    // Video display
    Rectangle {
        color: "#000000"
        anchors.bottom: bottomInfo.top
        anchors.top: mainMenu.bottom
        anchors.right: sidebar.left
        anchors.left: parent.left
        VideoOutput {
            id: videoOutput
            source: videoSource
            anchors.fill: parent
            MouseArea {
                id: imgArea
                hoverEnabled: false
                anchors.fill: parent
                acceptedButtons: Qt.LeftButton | Qt.RightButton
                onClicked: {
                    if(mouse.button == Qt.LeftButton) {
                        var mousePoint = Qt.point(imgArea.mouseX, imgArea.mouseY)
                        var normPoint = videoOutput.mapPointToSourceNormalized(mousePoint)
                        guiConsole.write(videoSource.getColor(normPoint.x, normPoint.y))
                    }
                }
                onPressed: {
                    if(mouse.button == Qt.RightButton) {
                        highlightItem = highlightComponent.createObject (imgArea, {
                            "x" : mouse.x,
                            "y" : mouse.y 
                        });
                    }
                }
                onPositionChanged: {
                    if (highlightItem !== null) {
                        // FIXME Handle right to left and down to up...
                        highlightItem.width = (mouse.x - highlightItem.x);
                        highlightItem.height = (mouse.y - highlightItem.y);
                    }
                }
                onReleased: {
                    if(mouse.button == Qt.RightButton) {
                        var point0 = Qt.point(highlightItem.x, highlightItem.y)
                        var normPoint0 = videoOutput.mapPointToSourceNormalized(point0)
                        point0 = videoSource.toSourcePoint(normPoint0.x, normPoint0.y)
                        if(point0.x < 0) { point0.x = 0 }
                        if(point0.y < 0) { point0.y = 0 }
                        
                        var point1 = Qt.point(mouse.x, mouse.y)
                        var normPoint1 = videoOutput.mapPointToSourceNormalized(point1)
                        point1 = videoSource.toSourcePoint(normPoint1.x, normPoint1.y)
                        if(point1.x < 0) { point1.x = 0 }
                        if(point1.y < 0) { point1.y = 0 }

                        visionEngine.setROI(point0.x, point0.y, point1.x, point1.y)
                        roiXY0.text = visionEngine.ROI_X0 + "," + visionEngine.ROI_Y0;
                        roiXY1.text = visionEngine.ROI_X1 + "," + visionEngine.ROI_Y1;

                        if (highlightItem !== null) {
                            highlightItem.destroy (); 
                        }
                    }
                }

                property Rectangle highlightItem : null;
                Component {
                    id: highlightComponent;

                    Rectangle {
                        color: "transparent"
                        border.width: 5;
                        border.color: "blue";
                    }
                }
            }
        }
    }

    // Sidebar
    Rectangle {
        id: sidebar
        anchors.top: mainMenu.bottom
        anchors.right: parent.right
        anchors.bottom: parent.bottom
        width: 275
        color: "#555555"

        ColumnLayout {
            anchors.left: parent.left
            anchors.right: parent.right
            spacing: 0
            ComboBox {
                id: transformBox
                Layout.fillWidth: true
                model: ListModel {
                    id: model
                    ListElement { text: "No Transform" }
                    ListElement { text: "Gray Thresold" }
                    ListElement { text: "HSV Thresold" }
                }
                onActivated: {
                    vMenu.visible = (index != 0);
                    hMenu.visible = (index == 2);
                    sMenu.visible = hMenu.visible;
                    if(hMenu.visible) {
                        visionEngine.setTmiTransformHsv();
                    }
                    else if (vMenu.visible) {
                        visionEngine.setTmiTransformGray();
                    }
                    else {
                        visionEngine.stopTransform();
                    }
                    videoSource.refreshImage() // FIXME Should not be mendatory
                }
            }
            RowLayout {
                Layout.alignment: Qt.AlignHCenter
                spacing: 1
                Text { text: "ROI:"; font.pixelSize: 20; }
                Text { text: "(";}
                TextInput {
                    id: roiXY0
                    text: "X0,Y0"
                    onAccepted: {
                        var values0 = roiXY0.text.split(',');
                        var X0 = parseInt(values0[0])
                        var Y0 = parseInt(values0[1])
                        
                        var values1 = roiXY1.text.split(',');
                        var X1 = parseInt(values1[0])
                        var Y1 = parseInt(values1[1])
                        if( isNaN(X0) || isNaN(Y0)) {
                            guiConsole.write("Please enter an initial point (X0,Y0)");
                        }
                        else if( isNaN(X1) || isNaN(Y1)) {
                            guiConsole.write("Please enter a final point (X1,Y1)");
                        }
                        else {
                            visionEngine.setROI(X0,Y0, X1, Y1);
                            roiXY0.text = visionEngine.ROI_X0 + "," + visionEngine.ROI_Y0;
                            roiXY1.text = visionEngine.ROI_X1 + "," + visionEngine.ROI_Y1;
                        }
                    }
                }
                Text { text: ")";}
                Text { text: "(";}
                TextInput {
                    id: roiXY1
                    text: "X1,Y1"
                    onAccepted: {
                        var values0 = roiXY0.text.split(',');
                        var X0 = parseInt(values0[0])
                        var Y0 = parseInt(values0[1])
                        
                        var values1 = roiXY1.text.split(',');
                        var X1 = parseInt(values1[0])
                        var Y1 = parseInt(values1[1])
                        if( isNaN(X0) || isNaN(Y0)) {
                            guiConsole.write("Please enter an initial point (X0,Y0)");
                        }
                        else if( isNaN(X1) || isNaN(Y1)) {
                            guiConsole.write("Please enter a final point (X1,Y1)");
                        }
                        else {
                            visionEngine.setROI(X0,Y0, X1, Y1);
                            roiXY0.text = visionEngine.ROI_X0 + "," + visionEngine.ROI_Y0;
                            roiXY1.text = visionEngine.ROI_X1 + "," + visionEngine.ROI_Y1;
                        }
                    }
                }
                Text { text: ")";}
            }
            ColumnLayout {
                id: hMenu;
                visible:false;
                spacing: 0
                RowLayout {
                    Layout.alignment: Qt.AlignHCenter
                    spacing: 1
                    Text { text: "Hue"; }
                    Text { text: "("; }
                    TextInput {
                        id: hTxt
                        text: "0,255";
                        onAccepted: {
                            var values = text.split(',');
                            var v1 = parseInt(values[0])
                            var v2 = parseInt(values[1])
                            visionEngine.setHRange(v1, v2)
                            hTxt.text = visionEngine.HMin + "," + visionEngine.HMax;
                            hRange.text = Math.round((visionEngine.HMax-visionEngine.HMin)/255*1000)/10;
                        }
                        validator: RegExpValidator { regExp: /(:?25[0-5]|2[0-4][0-9]|[0-1]?[0-9]?[0-9]?)\,(:?25[0-5]|2[0-4][0-9]|[0-1]?[0-9]?[0-9]?)/ }
                    }
                    Text { text: ")"; }
                    TextInput {
                        id: hRange
                        text: "100";
                        onAccepted: {
                            visionEngine.setHRangeFromPercentage(parseFloat(text))
                            hTxt.text = visionEngine.HMin + "," + visionEngine.HMax;
                        }
                        validator: RegExpValidator { regExp: /(:?100|[0-9]?[0-9]|[0-9]?[0-9]\.?[0-9]*)/ }
                    }
                    Text { text: "%"; }
                    Text {
                        id: hLock
                        function isLocked() {
                            return hLock.text === icons.EN_LOCK_CLOSED;
                        }
                        font.family: icons.FAM_ENTYPO
                        font.pixelSize: 20
                        text: icons.EN_LOCK_OPENED;
                        MouseArea {
                            anchors.fill: parent
                            cursorShape: Qt.PointingHandCursor
                            onClicked: {
                                if(hLock.isLocked()) {
                                    hLock.text = icons.EN_LOCK_OPENED;
                                }
                                else {
                                    hLock.text = icons.EN_LOCK_CLOSED;
                                }
                            }
                        }
                    }
                }
                RangeSlider {
                    id: hSlide
                    from: 0.0
                    to: 255.0
                    first.value: visionEngine.HMin
                    second.value: visionEngine.HMax
                    snapMode: RangeSlider.SnapAlways
                    stepSize: 1
                    Layout.fillWidth: true
                    first.onMoved: {
                        if(hLock.isLocked()){
                            visionEngine.setHRangeLocked(first.value,second.value)
                        }
                        else {
                            visionEngine.HMin = first.value;
                        }
                        hTxt.text = visionEngine.HMin + "," + visionEngine.HMax;
                        hRange.text = Math.round((visionEngine.HMax-visionEngine.HMin)/255*1000)/10;
                    }
                    second.onMoved: {
                        if(hLock.isLocked()){
                            visionEngine.setHRangeLocked(first.value,second.value)
                        }
                        else {
                            visionEngine.HMax = second.value;
                        }
                        hTxt.text = visionEngine.HMin + "," + visionEngine.HMax;
                        hRange.text = Math.round((visionEngine.HMax-visionEngine.HMin)/255*1000)/10;
                    }
                }
            }
            ColumnLayout {
                id: sMenu;
                visible:false;
                spacing: 0
                RowLayout {
                    Layout.alignment: Qt.AlignHCenter
                    Text { text: "Saturation"; }
                    Text { text: "("; }
                    TextInput {
                        id: sTxt
                        text: "0,255";
                        onAccepted: {
                            var values = text.split(',');
                            var v1 = parseInt(values[0])
                            var v2 = parseInt(values[1])
                            visionEngine.setSRange(v1, v2)
                            sTxt.text = visionEngine.SMin + "," + visionEngine.SMax;
                            sRange.text = Math.round((visionEngine.SMax-visionEngine.SMin)/255*1000)/10;
                        }
                        validator: RegExpValidator { regExp: /(:?25[0-5]|2[0-4][0-9]|[0-1]?[0-9]?[0-9]?)\,(:?25[0-5]|2[0-4][0-9]|[0-1]?[0-9]?[0-9]?)/ }
                    }
                    Text { text: ")"; }
                    TextInput {
                        id: sRange
                        text: "100";
                        onAccepted: {
                            visionEngine.setSRangeFromPercentage(parseFloat(text))
                            sTxt.text = visionEngine.SMin + "," + visionEngine.SMax;
                        }
                        validator: RegExpValidator { regExp: /(:?100|[0-9]?[0-9]|[0-9]?[0-9]\.?[0-9]*)/ }
                    }
                    Text { text: "%"; }
                    Text {
                        id: sLock
                        function isLocked() {
                            return sLock.text === icons.EN_LOCK_CLOSED;
                        }
                        font.family: icons.FAM_ENTYPO
                        font.pixelSize: 20
                        text: icons.EN_LOCK_OPENED;
                        MouseArea {
                            anchors.fill: parent
                            cursorShape: Qt.PointingHandCursor
                            onClicked: {
                                if(sLock.isLocked()) {
                                    sLock.text = icons.EN_LOCK_OPENED;
                                }
                                else {
                                    sLock.text = icons.EN_LOCK_CLOSED;
                                }
                            }
                        }
                    }
                }
                RangeSlider {
                    id: sSlide
                    from: 0
                    to: 255
                    first.value: visionEngine.SMin
                    second.value: visionEngine.SMax
                    snapMode: RangeSlider.SnapAlways
                    stepSize: 1
                    Layout.fillWidth: true

                    first.onMoved: {
                        if(sLock.isLocked()){
                            visionEngine.setSRangeLocked(first.value,second.value)
                        }
                        else {
                            visionEngine.SMin = first.value
                        }
                        sTxt.text = visionEngine.SMin + "," + visionEngine.SMax;
                        sRange.text = Math.round((visionEngine.SMax-visionEngine.SMin)/255*1000)/10;
                    }
                    second.onMoved: {
                        if(sLock.isLocked()){
                            visionEngine.setSRangeLocked(first.value,second.value)
                        }
                        else {
                            visionEngine.SMax = second.value
                        }
                        sTxt.text = visionEngine.SMin + "," + visionEngine.SMax;
                        sRange.text = Math.round((visionEngine.SMax-visionEngine.SMin)/255*1000)/10;
                    }
                }
            }
            ColumnLayout {
                id: vMenu;
                visible:false;
                spacing: 0
                RowLayout {
                    Layout.alignment: Qt.AlignHCenter
                    Text { text: "Value"; }
                    Text { text: "("; }
                    TextInput {
                        id: vTxt
                        text: "0,255";
                        onAccepted: {
                            var values = text.split(',');
                            var v1 = parseInt(values[0])
                            var v2 = parseInt(values[1])
                            visionEngine.setVRange(v1, v2)
                            vRange.text = Math.round((visionEngine.VMax-visionEngine.VMin)/255*1000)/10;
                        }
                        validator: RegExpValidator { regExp: /(:?25[0-5]|2[0-4][0-9]|[0-1]?[0-9]?[0-9]?)\,(:?25[0-5]|2[0-4][0-9]|[0-1]?[0-9]?[0-9]?)/ }
                    }
                    Text { text: ")"; }
                    TextInput {
                        id: vRange
                        text: "100";
                        onAccepted: {
                            visionEngine.setVRangeFromPercentage(parseFloat(text))
                            vTxt.text = visionEngine.VMin + "," + visionEngine.VMax;
                        }
                        validator: RegExpValidator { regExp: /(:?100|[0-9]?[0-9]|[0-9]?[0-9]\.?[0-9]*)/ }
                    }
                    Text { text: "%"; }
                    Text {
                        id: vLock
                        function isLocked() {
                            return vLock.text === icons.EN_LOCK_CLOSED;
                        }
                        font.family: icons.FAM_ENTYPO
                        font.pixelSize: 20
                        text: icons.EN_LOCK_OPENED;
                        MouseArea {
                            anchors.fill: parent
                            cursorShape: Qt.PointingHandCursor
                            onClicked: {
                                if(vLock.isLocked()) {
                                    vLock.text = icons.EN_LOCK_OPENED;
                                }
                                else {
                                    vLock.text = icons.EN_LOCK_CLOSED;
                                }
                            }
                        }
                    }
                }
                RangeSlider {
                    id: vSlide
                    from: 0
                    to: 255
                    first.value: visionEngine.VMin
                    second.value: visionEngine.VMax
                    snapMode: RangeSlider.SnapAlways
                    stepSize: 1
                    Layout.fillWidth: true

                    first.onMoved: {
                        if(vLock.isLocked()){
                            visionEngine.setVRangeLocked(first.value,second.value)
                        }
                        else {
                            visionEngine.VMin = first.value
                        }
                        vRange.text = Math.round((visionEngine.VMax-visionEngine.VMin)/255*1000)/10;
                        vTxt.text = visionEngine.VMin + "," + visionEngine.VMax;
                    }
                    second.onMoved: {
                        if(vLock.isLocked()){
                            visionEngine.setVRangeLocked(first.value,second.value)
                        }
                        else {
                            visionEngine.VMax = second.value
                        }
                        vRange.text = Math.round((visionEngine.VMax-visionEngine.VMin)/255*1000)/10;
                        vTxt.text = visionEngine.VMin + "," + visionEngine.VMax;
                    }
                }
            }
            // TODO Round in C++ instead
            Text {
                Layout.fillWidth: true
                text: "  TMI: [" + Math.round(visionEngine.TmiMin * 1000) / 1000 + ", " + Math.round(visionEngine.TmiMax * 1000) / 1000 + "]: " + Math.round(visionEngine.Tmi * 1000) / 1000;
            }
            Text {
                Layout.fillWidth: true
                text: "  AMI [" + Math.round(visionEngine.AmiMin * 1000) / 1000 + ", " + Math.round(visionEngine.AmiMax * 1000) / 1000 + "]: " + Math.round(visionEngine.Ami * 1000) / 1000;
            }
            Text {
                Layout.fillWidth: true
                text: "  RMI [" + Math.round(visionEngine.RmiMin * 1000) / 1000 + ", " + Math.round(visionEngine.RmiMax * 1000) / 1000 + "]: " + Math.round(visionEngine.Rmi * 1000) / 1000;
            }
            RowLayout {
                Text {
                    id: iconStdDev
                    text: "  " + icons.AW_ARROW_CW;
                    font.family: icons.FAM_AWESOME
                    MouseArea {
                        anchors.fill : parent
                        cursorShape: Qt.PointingHandCursor // FIXME do this for all icons
                        onClicked : {
                            manualSeed.visible = autoSeed.visible;
                            autoSeed.visible = !manualSeed.visible;
                            visionEngine.setStdDevInitMode(autoSeed.visible);
                            if( autoSeed.visible ) {
                                iconStdDev.text = "  " + icons.AW_ARROW_CW;;
                            }
                            else {
                                iconStdDev.text = "  " + icons.AW_HAND_PAPER_O;
                                manualSeed.text = autoSeed.text;
                                manualSeed.focus = true;
                            }
                            videoSource.refreshImage();
                        }
                    }
                }
                Text {
                    textFormat: Text.RichText
                    text: greek.sigma+"<sub>0</sub>";
                    font.pixelSize: 16
                }
                Text {
                    text: "=";
                }
                Text {
                    id: autoSeed
                    text: Math.round(visionEngine.StdDevInit);
                }
                TextInput {
                    id: manualSeed
                    text: "";
                    color: "#ffffff"
                    visible: false;
                    onAccepted: {
                        visionEngine.StdDevInit = parseFloat(text)
                        focus = false;
                        videoSource.refreshImage();
                    }
                    validator: RegExpValidator { regExp: /[-]?[0-9]+(:?\.[0-9]+)?/ }
                }
            }
            Text {
                Layout.fillWidth: true
                text: "  Gray Mean: [" + Math.round(visionEngine.GrayMeanMin) + ", " + Math.round(visionEngine.GrayMeanMax) + "]: " + Math.round(visionEngine.GrayMean);
            }
            RowLayout {
                Layout.fillWidth: true
                Text {
                    text: "  "+greek.sigma
                    font.pixelSize: 16
                }
                Text {
                    text: "[" + Math.round(visionEngine.StddevMin) + ", " + Math.round(visionEngine.StddevMax) + "]: " + Math.round(visionEngine.Stddev);
                }
            }
            Text {
                Layout.fillWidth: true
                text: "  Read FPS: " + videoSource.Fps;
            }
            Text {
                Layout.fillWidth: true
                text: "  Write FPS: " + videoRecorder.Fps;
            }
        }
    }
}
