//////////////////////////////////////////////////////////////////////////////
/// \file       VideoSource.cpp
/// \brief      Implementation of Video source
/// \copyright  2020 - Simon Dallaire (AGPLv3)
//////////////////////////////////////////////////////////////////////////////
//  This program is free software: you can redistribute it and/or modify
//  it under the terms of the GNU Affero General Public License as
//  published by the Free Software Foundation, either version 3 of the
//  License, or (at your option) any later version.
//
//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU Affero General Public License for more details.
//
//  You should have received a copy of the GNU Affero General Public License
//  along with this program.  If not, see <https://www.gnu.org/licenses/>.
//////////////////////////////////////////////////////////////////////////////
#include"VideoSource.hpp"
#include<cmath>
#include<Utilities/MinMax.hpp>
#include<QDateTime>
#include <chrono>
#include <opencv2/imgcodecs.hpp>

using namespace mfcv;
using namespace mfcv::gui;

namespace {
    bool isVideoFileValid(const std::string& filePath) {
        const cv::VideoCapture test{filePath};
        return test.isOpened();
    }
    constexpr int normToImg(double normVal, int maxVal) {
        double retVal = std::round(normVal*maxVal);
        if(retVal < 0) retVal = 0;
        else if(retVal >= maxVal) retVal = maxVal-1;
        return static_cast<int>(retVal);
    }
}

VideoSource::VideoSource() {
    FrameConverter::sptr_t shFrameConverter = _frameConverter;
    auto shTransforms = _transforms_lst;
    auto shTransformedFrameListener = _transformedFrameListener;
    connect(_frameConverter.get(), &FrameConverter::frameReady, this, &VideoSource::onFrameReady);
    _callback = std::make_shared<CameraProxy::Callback_t>(
        [shFrameConverter, shTransforms, shTransformedFrameListener]
        (const ImageAdapter::csptr_t& image) {
            if(image->empty()) {
                return;
            }
            if(shTransforms->empty()) {
                shFrameConverter->push_image(image);
            }
            else {
                ImageAdapter::uptr_t imageToTransform = image->clone();
                shTransforms->notifyListeners(imageToTransform);

                ImageAdapter::csptr_t transformedImage{std::move(imageToTransform)};
                shTransformedFrameListener->notifyListeners(transformedImage);
                shFrameConverter->push_image(std::move(transformedImage), image);
            }
            emit shFrameConverter->frameReady();
        }
    );
    _cam_proxy->addListener(_callback);
}

VideoSource::~VideoSource() {
    _callback = nullptr;
    _cam_proxy->stop();
    if(m_surface) {
        m_surface->stop();
    }
    for(size_t i{0}; i < _saveFutures.size(); ++i) {
        if(_saveFutures[i].valid()) {
            _saveFutures[i].get();
        }
    }
}

QAbstractVideoSurface* VideoSource::surface() const { return m_surface; }

void VideoSource::setSurface(QAbstractVideoSurface *surface) {
    if (m_surface != surface && m_surface && m_surface->isActive()) {
        m_surface->stop();
    }
    m_surface = surface;
    _frameConverter->setSupportedPixelFormat(m_surface->supportedPixelFormats());
}

void VideoSource::addRawFrameListener(CallbackAdapter *callbackRawPtr) {
    if(callbackRawPtr) {
        _cam_proxy->addListener(callbackRawPtr->data);
    }
}

void VideoSource::addTransformedFrameListener(CallbackAdapter *callbackRawPtr) {
    if(callbackRawPtr) {
        _transformedFrameListener->addListener(callbackRawPtr->data);
    }
}

void VideoSource::addTransform(TransformAdapter *callbackRawPtr) {
    if(callbackRawPtr) {
        _transforms_lst->addListener(callbackRawPtr->data);
    }
}

void VideoSource::refreshCamList(bool reset) {
    QStringList camLst;
    camLst.append("None");
    for(const auto& camera : _cam_proxy->listAvailableCamera(reset)) {
        camLst.append(QString::fromStdString(camera));
    }
    setCamLst(camLst);
}

void VideoSource::openVideoFile(const QUrl &file_url) {
    const std::string file_path = file_url.toLocalFile().toStdString();
    if(::isVideoFileValid(file_path)) {
        _cam_proxy->stop();
        _lastHeight = _lastWidth = 0;
        _cam_proxy->readFile(file_path);
        emit sourceChanged();
    }
    else {
        LOG(WARNING) << "The file '" << file_path << "' is not valid.";
    }
}

void VideoSource::openImageFile(const QUrl &file_url) {
    // FIXME Image could not be read
    // https://docs.opencv.org/4.3.0/d4/da8/group__imgcodecs.html#ga288b8b3da0892bd651fce07b3bbd3a56
    const std::string &file_path = file_url.toLocalFile().toStdString();
    cv::Mat result = cv::imread(file_path);
    if(result.empty()) {
        LOG(ERROR) << "Could not open the file :' " << file_path << "'";
    }
    else {
        LOG(INFO) << "Opening the file :' " << file_path << "'";
        auto frame = ImageAdapter::mk_sptr(std::move(result));
        frame->setStillFps();
        openCamera(-1);
        (*_callback)(frame);
        emit sourceChanged();
    }
}

void VideoSource::openCamera(int idx) {
    idx--;
    _cam_proxy->stop();
    _lastHeight = _lastWidth = 0;
    if(idx < 0) {
        _frameConverter->push_blackframe();
        emit _frameConverter->frameReady();
        sourceChanged();
    }
    else {
        _cam_proxy->start(idx);
        sourceChanged();
    }
}

void VideoSource::onFrameReady() {
    FrameAdapter::uptr_t workingFrame;
    if(m_surface && _frameConverter->pop_frame(workingFrame)) {
        const QVideoFrame& qFrame = workingFrame->qFrame;
        if(_lastWidth != qFrame.width() || _lastHeight != qFrame.height()) {
            m_surface->stop();
            _lastWidth = qFrame.width();
            _lastHeight = qFrame.height();
        }
        if(!m_surface->isActive()) {
            QVideoSurfaceFormat format(qFrame.size(), qFrame.pixelFormat());
            if(!m_surface->start(format)) {
                LOG(ERROR) << "Could not start presentation: " << m_surface->error();
                return;
            }
        }

        setFps(workingFrame->FPS);
        m_surface->present(qFrame);
        setSrcWidth(workingFrame->qImage.width());
        setSrcHeight(workingFrame->qImage.height());
        _last_frame = std::move(workingFrame);
    }
}

QString VideoSource::getColor(const double normX, const double normY) const {
    QString ret_val;
    FrameAdapter::csptr_t workingFrame = _last_frame;
    if(workingFrame && !workingFrame->originalRgbMat.empty()) {
        const int x = ::normToImg(normX, workingFrame->qOriginalImage.width());
        const int y = ::normToImg(normY, workingFrame->qOriginalImage.height());

        // FIXME support non RGB frame
        const cv::Mat& RGB = workingFrame->originalRgbMat(cv::Rect(x, y, 1, 1));

        cv::Mat HSV, gray;
        cv::cvtColor(RGB, HSV, cv::COLOR_RGB2HSV);
        cv::cvtColor(RGB, gray, cv::COLOR_RGB2GRAY);

        char buffer[71];
        buffer[0]=0;
        constexpr size_t buf_len = sizeof(buffer)/sizeof(buffer[0]);

        snprintf(buffer, buf_len, "[%d,%d] -> RGB: (%d,%d,%d) ---- HSV: (%d,%d,%d) ---- Gray: %d",
                x,y,
                RGB.at<cv::Vec3b>(0)[0], RGB.at<cv::Vec3b>(0)[1], RGB.at<cv::Vec3b>(0)[2],
                HSV.at<cv::Vec3b>(0)[0], HSV.at<cv::Vec3b>(0)[1], HSV.at<cv::Vec3b>(0)[2],
                gray.at<uchar>(0));
        ret_val = QString{buffer};
    }
    return ret_val;
}

QPoint VideoSource::toSourcePoint(const double normX, const double normY) const {
    return QPoint{
        ::normToImg(normX, getSrcWidth()),
        ::normToImg(normY, getSrcHeight())
    };
}

void VideoSource::takePicture(const QString& path) {
    //TODO QImageWriter::supportedImageFormats()

    const FrameAdapter::csptr_t workingFrame{_last_frame};
    if(!workingFrame || workingFrame->qImage.isNull()) {
        return;
    }

    auto future = std::async([path, workingFrame](){
        constexpr char format[]{"png"};
        const QString filePath{QString("%1/%2.%3")
                                .arg(path)
                                .arg(QDateTime::currentDateTime().toLocalTime().toString(QStringLiteral("yyyy-mm-dd_hh:mm:ss.zzz")))
                                .arg(format)};

        constexpr int quality{0}; // From max compression (0) to uncompressed (100)
        
        LOG(INFO) << "Saving image...";
        if(workingFrame->qImage.save(filePath, format, quality)) {
            LOG(INFO) << "Image saved to: '" << filePath.toStdString() <<"'.";
        }
        else {
            LOG(ERROR) << "Fail to save image to: '" << filePath.toStdString() <<"'.";
        }
    });

    for(size_t i{0}; i < _saveFutures.size(); ++i) {
        if(!_saveFutures[i].valid()) {
            _saveFutures[i] = std::move(future);
            break;
        }
        else if(std::future_status::timeout != _saveFutures[i].wait_for(std::chrono::microseconds(1))) {
            _saveFutures[i].get();
            _saveFutures[i] = std::move(future);
            break;
        }
    }
    if(future.valid()) {
        _saveFutures.emplace_back(std::move(future));
    }
}

void VideoSource::refreshImage() {
    if(_last_frame
    && _last_frame->original
    && _last_frame->original->isStill()) {
        (*_callback)(_last_frame->original);
    }
}