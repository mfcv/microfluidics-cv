//////////////////////////////////////////////////////////////////////////////
/// \file       FontProxy.hpp
/// \brief      Implementation of FontProxy which control the font access.
/// \copyright  2019 - Simon Dallaire (AGPLv3).
//////////////////////////////////////////////////////////////////////////////
//  This program is free software: you can redistribute it and/or modify
//  it under the terms of the GNU Affero General Public License as
//  published by the Free Software Foundation, either version 3 of the
//  License, or (at your option) any later version.
//
//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU Affero General Public License for more details.
//
//  You should have received a copy of the GNU Affero General Public License
//  along with this program.  If not, see <https://www.gnu.org/licenses/>.
//////////////////////////////////////////////////////////////////////////////
#include"FontProxy.hpp"
#include<Log.hpp>
#include<Utilities/Reflection.hpp>


using namespace mfcv::gui;

FontProxy::FontProxy(const QGuiApplication&)
{
    loadFonts(":/fonts/entypo/font/entypo.ttf");
    loadFonts(":/fonts/font_awesome/font/font_awesome.ttf");
    loadFonts(":/fonts/fontelico/font/fontelico.ttf");
}

void FontProxy::loadFonts(const char font_path[]) noexcept
{
    try
    {
        if (_fontsDatabase.addApplicationFont(font_path) == -1)
        {
            LOG(WARNING) << "Failed to load: '" << font_path << "'.";
        }
    }
    MFCV_CATCH("Failed to load: '", font_path, "'.")
}