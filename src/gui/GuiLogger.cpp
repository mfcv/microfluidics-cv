//////////////////////////////////////////////////////////////////////////////
/// \file       GuiLogger.cpp
/// \brief      Implementation of the GUI Console.
/// \copyright  2019 - Simon Dallaire (AGPLv3).
//////////////////////////////////////////////////////////////////////////////
//  This program is free software: you can redistribute it and/or modify
//  it under the terms of the GNU Affero General Public License as
//  published by the Free Software Foundation, either version 3 of the
//  License, or (at your option) any later version.
//
//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU Affero General Public License for more details.
//
//  You should have received a copy of the GNU Affero General Public License
//  along with this program.  If not, see <https://www.gnu.org/licenses/>.
//////////////////////////////////////////////////////////////////////////////
#include"GuiLogger.hpp"
#include<Utilities/MfcvLogger.hpp>
#include<Utilities/Reflection.hpp>

using namespace mfcv::gui;

void GuiSink::log(g3::LogMessageMover logEntry) {
    std::lock_guard<std::mutex> lock(m_mutex);
    if(m_provider) {
        m_provider->log(logEntry);
    }
}
void GuiSink::setProvider(LogInterface* provider) {
    std::lock_guard<std::mutex> lock(m_mutex);
    m_provider = provider;
}

GuiLogger::GuiLogger() {
    auto gui_sink = std::make_unique<GuiSink>();
    gui_sink->setProvider(this);
    m_handler = MfcvLogger::Instance().addSink(std::move(gui_sink), &GuiSink::log);
}

GuiLogger::~GuiLogger() { 
    m_destroying = true;
    auto result = m_handler->call(&GuiSink::setProvider, nullptr);
    result.get();
}

void GuiLogger::setDocument(QQuickTextDocument *document) {
    if (document == m_document)
        return;

    m_document = document;
    emit documentChanged();
}

void GuiLogger::write(const QString& msg) {
    if(!msg.isEmpty()) {
        LOG(INFO) << msg.toStdString();
    }
}

QString GuiLogger::get_color(const int value) {
    const char* retColor = "white";
    if (value == DEBUG.value) { retColor = "#00ff00"; }
    else if (value == INFO.value) { retColor = "white"; }
    else if (value == WARNING.value) { retColor = "yellow"; }
    else if (value == ERROR.value) { retColor = "red"; }
    else if (value == CRITICAL.value) { retColor = "red"; }
    else if (value >= FATAL.value) { retColor = "red"; }
    return QString(retColor);
}

QString GuiLogger::get_name(const LEVELS& level) {
    if(level.value > INFO.value) {
        return QString::fromStdString(level.text) + ": ";
    }
    else {
        return QString();
    }
}

void GuiLogger::log(g3::LogMessageMover logEntry) {
    if(m_destroying) {
        return;
    }
    try {
        const auto& level = logEntry.get()._level;
        if(level.value > DEBUG.value) {
            emit updated("<font color=\"" + get_color(level.value) + "\">"
                        + get_name(level)
                        + QString::fromStdString(logEntry.get().message())
                        +"</font>");
        }
    }
    MFCV_CATCH("Failed to log message.")
}
