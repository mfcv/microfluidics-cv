//////////////////////////////////////////////////////////////////////////////
/// \file       FrameConverter.cpp
/// \brief      Implementation of the frame converter
/// \copyright  2020 - Simon Dallaire (AGPLv3)
//////////////////////////////////////////////////////////////////////////////
//  This program is free software: you can redistribute it and/or modify
//  it under the terms of the GNU Affero General Public License as
//  published by the Free Software Foundation, either version 3 of the
//  License, or (at your option) any later version.
//
//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU Affero General Public License for more details.
//
//  You should have received a copy of the GNU Affero General Public License
//  along with this program.  If not, see <https://www.gnu.org/licenses/>.
//////////////////////////////////////////////////////////////////////////////
#include"FrameConverter.hpp"

using namespace mfcv::gui;

void FrameConverter::setSupportedPixelFormat(const QList<QVideoFrame::PixelFormat> &pixelFormatList) {
    bool isSupported{false};
    for(const auto format : pixelFormatList) {
        if(setFormat(format)) {
            isSupported = true;
            break;
        }
    }
    if(!isSupported) {
        imgFormat = QImage::Format_Invalid;
        LOG(ERROR) << "Could not find supported frame format.";
    }
}

bool FrameConverter::push_image(ImageAdapter::csptr_t pOriginalImage) {
    if(imgFormat == QImage::Format_Invalid) {
        return false;
    }

    auto frame = FrameAdapter::mk_uptr(pOriginalImage);
    frame->initQFrame(imgFormat);

    return _queue->emplace_back(std::move(frame));
}

bool FrameConverter::push_image(ImageAdapter::csptr_t image, ImageAdapter::csptr_t pOriginalImage) {
    if(imgFormat == QImage::Format_Invalid) {
        return false;
    }

    auto frame = FrameAdapter::mk_uptr(pOriginalImage);
    // FIXME add getter and setters to ensure both buffers are corresponding
    frame->cvMat = image->toRgbImg();
    frame->qImage = QImage(frame->cvMat.data,
                           frame->cvMat.cols,
                           frame->cvMat.rows,
                           frame->cvMat.step,
                           QImage::Format_RGB888);
    frame->initQFrame(imgFormat);
    return _queue->emplace_back(std::move(frame));
}

bool FrameConverter::push_blackframe() {
    if(imgFormat == QImage::Format_Invalid) {
        return false;
    }

    QImage black{1, 1, imgFormat};
    black.fill(QColor("#000000"));

    auto frame = FrameAdapter::mk_uptr(black);
    frame->qFrame = QVideoFrame(frame->qImage);

    return _queue->emplace_back(std::move(frame));
}

bool FrameConverter::pop_frame(FrameAdapter::uptr_t& frame) {
    return _queue->pop_front(frame);
}

bool FrameConverter::setFormat(const QVideoFrame::PixelFormat format) {
    bool isValid{true};
    switch (format) {
        case QVideoFrame::Format_ARGB32:
            imgFormat = QImage::Format_ARGB32;
        break;
        case QVideoFrame::Format_ARGB32_Premultiplied:
            imgFormat = QImage::Format_ARGB32_Premultiplied;
        break;
        case QVideoFrame::Format_RGB32:
            imgFormat = QImage::Format_RGB32;
        break;
        case QVideoFrame::Format_RGB24:
            imgFormat = QImage::Format_RGB888;
        break;
        case QVideoFrame::Format_RGB565:
            imgFormat = QImage::Format_RGB16;
        break;
        case QVideoFrame::Format_RGB555:
            imgFormat = QImage::Format_RGB555;
        break;
        default:
            isValid = false;
        break;
    }
    return isValid;
}
