//////////////////////////////////////////////////////////////////////////////
/// \file       VideoSource.hpp
/// \brief      Definition of the video source
/// \copyright  2020 - Simon Dallaire (AGPLv3)
//////////////////////////////////////////////////////////////////////////////
//  This program is free software: you can redistribute it and/or modify
//  it under the terms of the GNU Affero General Public License as
//  published by the Free Software Foundation, either version 3 of the
//  License, or (at your option) any later version.
//
//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU Affero General Public License for more details.
//
//  You should have received a copy of the GNU Affero General Public License
//  along with this program.  If not, see <https://www.gnu.org/licenses/>.
//////////////////////////////////////////////////////////////////////////////
#pragma once
#include<QObject>
#include<QAbstractVideoSurface>
#include<QVideoSurfaceFormat>
#include<QVideoRendererControl>
#include<QStringList>
#include<QPoint>
#include<QUrl>
#include<vector>
#include"../Global.hpp"


#include<Concurrency/ConcurentQueue.hpp>
#include<VideoIO/CameraProxy.hpp>
#include"FrameConverter.hpp"
#include"GuiAdapters.hpp"

namespace mfcv::gui {
    class VideoSource : public QVideoRendererControl
    {
    public:
        VideoSource();
        virtual ~VideoSource();

    private:

        FrameConverter::sptr_t _frameConverter = FrameConverter::mk_sptr();

        QAbstractVideoSurface *m_surface{nullptr};
        FrameAdapter::sptr_t _last_frame{nullptr};
        std::vector<std::future<void>> _saveFutures{10};

        CameraProxy::Callback_sptr_t _callback;
        CameraProxy::sptr_t _cam_proxy{CameraProxy::mk_sptr()};
        TransformAdapter::CallbackList::sptr_t _transforms_lst{TransformAdapter::CallbackList::mk_sptr()};
        CameraProxy::CallbackList::sptr_t _transformedFrameListener{CameraProxy::CallbackList::mk_sptr()};

        std::atomic_int _lastWidth{0}, _lastHeight{0};

    ////////// Qt properties, signal and slots //////////
        Q_OBJECT
        Q_PROPERTY(QAbstractVideoSurface *videoSurface READ surface WRITE setSurface) // override
        MFCV_PROP_GET_SET(QStringList, CamLst)
        MFCV_PROP_NUMBER_GET(int, SrcWidth)
        MFCV_PROP_NUMBER_GET(int, SrcHeight)
        MFCV_PROP_NUMBER_GET(int, Fps)
    public:
        QAbstractVideoSurface* surface() const override;
        void setSurface(QAbstractVideoSurface *surface) override;

        Q_INVOKABLE void refreshImage();
        Q_INVOKABLE QString getColor(const double normX, const double normY) const;
        Q_INVOKABLE QPoint toSourcePoint(const double normX, const double normY) const;
        Q_INVOKABLE void refreshCamList(bool reset);
        Q_INVOKABLE void openVideoFile(const QUrl &file_url);
        Q_INVOKABLE void openImageFile(const QUrl &file_url);
        Q_INVOKABLE void openCamera(int idx);
        Q_INVOKABLE void addTransform(TransformAdapter *callbackRawPtr);
        Q_INVOKABLE void addRawFrameListener(CallbackAdapter* callbackRawPtr);
        Q_INVOKABLE void addTransformedFrameListener(CallbackAdapter* callbackRawPtr);
        Q_INVOKABLE void takePicture(const QString& path); // TODO Add format choice
    signals:
        void sourceChanged();
    public slots:
        void onFrameReady();
    };
}
