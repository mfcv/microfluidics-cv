//////////////////////////////////////////////////////////////////////////////
/// \file       Greek.hpp
/// \brief      Declaration of the Greek.
/// \copyright  2020 - Simon Dallaire (AGPLv3).
//////////////////////////////////////////////////////////////////////////////
//  This program is free software: you can redistribute it and/or modify
//  it under the terms of the GNU Affero General Public License as
//  published by the Free Software Foundation, either version 3 of the
//  License, or (at your option) any later version.
//
//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU Affero General Public License for more details.
//
//  You should have received a copy of the GNU Affero General Public License
//  along with this program.  If not, see <https://www.gnu.org/licenses/>.
//////////////////////////////////////////////////////////////////////////////
#pragma once
#include<QObject>
#include"../Global.hpp"

namespace mfcv::gui
{
    // Source: http://www.wizcity.com/Computers/Characters/GreekUTF8.php
    class Greek : public QObject
    {
    public:
        Greek() = default;
        virtual ~Greek() = default;

    ////////// Qt properties, signal and slots //////////
        Q_OBJECT

        //////////////////////////////
        // Small Greek
        MFCV_PROP_STR_STATIC(alpha, "\u03B1")
        MFCV_PROP_STR_STATIC(beta, "\u03B2")
        MFCV_PROP_STR_STATIC(gamma, "\u03B3")
        MFCV_PROP_STR_STATIC(delta, "\u03B4")
        MFCV_PROP_STR_STATIC(epsilon, "\u03B5")
        MFCV_PROP_STR_STATIC(zeta, "\u03B6")
        MFCV_PROP_STR_STATIC(eta, "\u03B7")
        MFCV_PROP_STR_STATIC(theta, "\u03B8")
        MFCV_PROP_STR_STATIC(iota, "\u03B9")
        MFCV_PROP_STR_STATIC(kappa, "\u03BA")
        MFCV_PROP_STR_STATIC(lambda, "\u03BB")
        MFCV_PROP_STR_STATIC(mu, "\u03BC")
        MFCV_PROP_STR_STATIC(nu, "\u03BD")
        MFCV_PROP_STR_STATIC(xi, "\u03BE")
        MFCV_PROP_STR_STATIC(omicron, "\u03BF")
        MFCV_PROP_STR_STATIC(pi, "\u03C0")
        MFCV_PROP_STR_STATIC(rho, "\u03C1")
        MFCV_PROP_STR_STATIC(sigmaf, "\u03C2")
        MFCV_PROP_STR_STATIC(sigma, "\u03C3")
        MFCV_PROP_STR_STATIC(tau, "\u03C4")
        MFCV_PROP_STR_STATIC(upsilon, "\u03C5")
        MFCV_PROP_STR_STATIC(phi, "\u03C6")
        MFCV_PROP_STR_STATIC(chi, "\u03C7")
        MFCV_PROP_STR_STATIC(psi, "\u03C8")
        MFCV_PROP_STR_STATIC(omega, "\u03C9")
        MFCV_PROP_STR_STATIC(thetasym, "\u03D1")
        MFCV_PROP_STR_STATIC(upsih, "\u03D2")
        MFCV_PROP_STR_STATIC(piv, "\u03D6")

        //////////////////////////////
        // Capital Greek
        MFCV_PROP_STR_STATIC(Alpha, "\u0391")
        MFCV_PROP_STR_STATIC(Beta, "\u0392")
        MFCV_PROP_STR_STATIC(Gamma, "\u0393")
        MFCV_PROP_STR_STATIC(Delta, "\u0394")
        MFCV_PROP_STR_STATIC(Epsilon, "\u0395")
        MFCV_PROP_STR_STATIC(Zeta, "\u0396")
        MFCV_PROP_STR_STATIC(Eta, "\u0397")
        MFCV_PROP_STR_STATIC(Theta, "\u0398")
        MFCV_PROP_STR_STATIC(Iota, "\u0399")
        MFCV_PROP_STR_STATIC(Kappa, "\u039A")
        MFCV_PROP_STR_STATIC(Lambda, "\u039B")
        MFCV_PROP_STR_STATIC(Mu, "\u039C")
        MFCV_PROP_STR_STATIC(Nu, "\u039D")
        MFCV_PROP_STR_STATIC(Xi, "\u039E")
        MFCV_PROP_STR_STATIC(Omicron, "\u039F")
        MFCV_PROP_STR_STATIC(Pi, "\u03A0")
        MFCV_PROP_STR_STATIC(Rho, "\u03A1")
        MFCV_PROP_STR_STATIC(Sigma, "\u03A3")
        MFCV_PROP_STR_STATIC(Tau, "\u03A4")
        MFCV_PROP_STR_STATIC(Upsilon, "\u03A5")
        MFCV_PROP_STR_STATIC(Phi, "\u03A6")
        MFCV_PROP_STR_STATIC(Chi, "\u03A7")
        MFCV_PROP_STR_STATIC(Psi, "\u03A8")
        MFCV_PROP_STR_STATIC(Omega, "\u03A9")
    };
}