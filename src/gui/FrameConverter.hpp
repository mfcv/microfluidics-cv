//////////////////////////////////////////////////////////////////////////////
/// \file       FrameConverter.hpp
/// \brief      Definition of the frame converter
/// \copyright  2020 - Simon Dallaire (AGPLv3)
//////////////////////////////////////////////////////////////////////////////
//  This program is free software: you can redistribute it and/or modify
//  it under the terms of the GNU Affero General Public License as
//  published by the Free Software Foundation, either version 3 of the
//  License, or (at your option) any later version.
//
//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU Affero General Public License for more details.
//
//  You should have received a copy of the GNU Affero General Public License
//  along with this program.  If not, see <https://www.gnu.org/licenses/>.
//////////////////////////////////////////////////////////////////////////////
#pragma once
#include<QObject>
#include<QVideoSurfaceFormat>
#include<QVideoRendererControl>
#include<QStringList>
#include<Utilities/MinMax.hpp>

#include<Concurrency/ConcurentQueue.hpp>
#include<VideoIO/ImageAdapter.hpp>

namespace mfcv::gui {
    struct FrameAdapter final {
        MFCV_PTR_T(FrameAdapter)

        // FIXME Move all initialisation inside this class
        FrameAdapter(const QImage& pOriginalImage)
            : original(nullptr),
              originalRgbMat(cv::Mat(pOriginalImage.height(),
                                     pOriginalImage.width(),
                                     CV_8UC3,
                                     (void*)pOriginalImage.bits(),
                                     (size_t)pOriginalImage.bytesPerLine())),
              qOriginalImage(pOriginalImage),
              qImage(pOriginalImage),
              cvMat(originalRgbMat),
              FPS(0.0)
        { }
        FrameAdapter(const ImageAdapter::csptr_t& pOriginal)
            : original(pOriginal),
              originalRgbMat(original->toRgbImg()), // FIXME Use BGR image when updating to Qt 5.14
              qOriginalImage(originalRgbMat.data,
                             originalRgbMat.cols,
                             originalRgbMat.rows,
                             originalRgbMat.step,
                             QImage::Format_RGB888),
              qImage(qOriginalImage),
              cvMat(originalRgbMat),
              FPS(pOriginal->getFps())
        { }
        ~FrameAdapter() = default;

        const ImageAdapter::csptr_t original;
        const cv::Mat originalRgbMat;
        const QImage qOriginalImage;

        QImage qImage;
        cv::Mat cvMat;
        QVideoFrame qFrame;

        const double FPS;

        // FIXME Remember CV format to support opencv conversion
        inline void initQFrame(const QImage::Format pFormat) {
            if(pFormat != QImage::Format_RGB888) {
                qImage = qImage.convertToFormat(pFormat);
                cvMat = cv::Mat(qImage.height(),
                                qImage.width(),
                                CV_8UC3,
                                (void*)qImage.bits(),
                                (size_t)qImage.bytesPerLine());
            }
            qFrame = QVideoFrame(qImage);
        }
    };
    class FrameConverter : public QObject {
        public:
        MFCV_PTR_T(FrameConverter)

        FrameConverter() = default;
        virtual ~FrameConverter() = default;

        void setSupportedPixelFormat(const QList<QVideoFrame::PixelFormat> &pixelFormat);
        bool push_image(ImageAdapter::csptr_t image);
        bool push_image(ImageAdapter::csptr_t image, ImageAdapter::csptr_t originalImage);
        bool push_blackframe();
        bool pop_frame(FrameAdapter::uptr_t& frame);

        private:
        using buf_t = ConcurentQueue<10, FrameAdapter::uptr_t>;
        buf_t::sptr_t _queue = buf_t::mk_sptr();

        QVideoFrame::PixelFormat dstFormat{QVideoFrame::Format_Invalid};
        QImage::Format imgFormat{QImage::Format_Invalid};

        bool setFormat(const QVideoFrame::PixelFormat format);
        ////////// Qt properties, signal and slots //////////
        Q_OBJECT
        signals:
        void frameReady();
    };
}