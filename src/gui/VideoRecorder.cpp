//////////////////////////////////////////////////////////////////////////////
/// \file       VideoRecorder.hpp
/// \brief      Declaration of VideoRecorder which control the file saving.
/// \copyright  2020 - Simon Dallaire (AGPLv3).
//////////////////////////////////////////////////////////////////////////////
//  This program is free software: you can redistribute it and/or modify
//  it under the terms of the GNU Affero General Public License as
//  published by the Free Software Foundation, either version 3 of the
//  License, or (at your option) any later version.
//
//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU Affero General Public License for more details.
//
//  You should have received a copy of the GNU Affero General Public License
//  along with this program.  If not, see <https://www.gnu.org/licenses/>.
//////////////////////////////////////////////////////////////////////////////
#include"VideoRecorder.hpp"

#include<QDir>
#include<QDateTime>
#include<QStandardPaths>
#include<cmath>

using namespace mfcv;
using namespace mfcv::gui;

VideoRecorder::VideoRecorder() {
    connect(this, &VideoRecorder::customRecordIconChanged, this, &VideoRecorder::onCustomRecordIconChanged);
}

VideoRecorder::~VideoRecorder() {
    if(isRecording) {
        auto result = stopRecord();
        if(result.valid()) {
            result.get();
        }
    }
    else if(last_result.valid()) {
        last_result.get();
    }
}

void VideoRecorder::toggleRecord(const QString& dir_name) {
    if(isRecording) {
        this->last_result = stopRecord();
        this->setRecordIcon(this->getcustomRecordIcon());
        this->isRecording = false;
    }
    else {
        this->startRecord(dir_name);
        this->setRecordIcon(Icons::EN_STOP());
        this->isRecording = true;
    }
}

void VideoRecorder::startRecord(const QString& dir_name) {
    // FIXME resize image if required
    const auto& stdDirName = mfcv::mk_sptr<std::string>(dir_name.toStdString());
    const QDir dest_dir{dir_name};
    if(!dest_dir.exists()) {
        LOG(ERROR) << "'" << stdDirName << "' does not exists.";
        return;
    }

    auto elapsedFps = mfcv::mk_sptr<double>(-1.0);
    auto writeRatio = mfcv::mk_sptr<double>(-1.0);
    auto mean = Mean<double>::mk_sptr(50.0);
    // FIXME Remove currentDateTime format duplication
    const std::string timeStamp = QDateTime::currentDateTime()
                                            .toLocalTime()
                                            .toString(QStringLiteral("yyyy-mm-dd_hh:mm:ss.zzz"))
                                            .toStdString();

    _writer = VideoFileWriter::mk_sptr(
                    VideoFileWriter::DEFAULT_CODEC,
                    append(*stdDirName,
                                   "/",
                                   timeStamp,
                                   this->_fileSufix.toStdString(),
                                   VideoFileWriter::EXTENSION));

    _writeCallback = std::make_shared<CameraProxy::Callback_t>(
        [mean, elapsedFps, writeRatio, this, stdDirName](const ImageAdapter::csptr_t& frame){

            *elapsedFps = this->_writer->getMeanFps();
            /* FIXME Test this part
            if(*writeRatio < 0.0 && *elapsedFps > 0.0) {
                *elapsedFps = this->_writer->getMeanFps();
                const double writeRatioTmp = this->_writer->getMeanFps()/frame->getFps();
                const bool isSlower{writeRatioTmp < 1.0};
                if(isSlower) { 
                    LOG(WARNING) << "Writing '" << writeRatioTmp << "' times slower than the input, the video will be downsize.";
                    *writeRatio = std::sqrt(writeRatioTmp);
                    this->_writer->release();
                }
            }
            else if (*writeRatio > 0.0) {
                ImageAdapter::sptr_t newFrame = ImageAdapter::mk_sptr();
                newFrame->setFps(frame->getFps());
                // FixMe add choice for the blur
                cv::GaussianBlur(frame->cgetBgrImg(), newFrame->getBgrImg(), cv::Size{5,5}, 0);
                const cv::Size newSize{0,0};
                cv::resize(newFrame->getBgrImg(),
                           newFrame->getBgrImg(),
                           newSize,
                           *writeRatio,
                           *writeRatio,
                           cv::INTER_AREA);
                this->_writer->write_async(newFrame);
            }
            else {
                this->_writer->write_async(frame);
            }
            */
            this->_writer->write_async(frame);
            this->setFps(*elapsedFps);
    });
    
    CallbackAdapter adapter{_writeCallback};
    emit frameListenerChanged(&adapter);
}

std::future<void>  VideoRecorder::stopRecord() {
    _writeCallback.reset();
    return _writer->close_async();
}

QString VideoRecorder::defaultDir() const { 
    const QString ret_val{QStandardPaths::writableLocation(QStandardPaths::MoviesLocation) + QStringLiteral("/mfcv")};
    QDir().mkpath(ret_val);
    return ret_val;
}

void VideoRecorder::onCustomRecordIconChanged(){
    if(!isRecording){
        this->setRecordIcon(this->getcustomRecordIcon());
    }
}