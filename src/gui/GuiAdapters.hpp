//////////////////////////////////////////////////////////////////////////////
/// \file       GuiAdapters.hpp
/// \brief      Definition of adapter class for QML
/// \copyright  2020 - Simon Dallaire (AGPLv3)
//////////////////////////////////////////////////////////////////////////////
//  This program is free software: you can redistribute it and/or modify
//  it under the terms of the GNU Affero General Public License as
//  published by the Free Software Foundation, either version 3 of the
//  License, or (at your option) any later version.
//
//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU Affero General Public License for more details.
//
//  You should have received a copy of the GNU Affero General Public License
//  along with this program.  If not, see <https://www.gnu.org/licenses/>.
//////////////////////////////////////////////////////////////////////////////
#pragma once
#include<CopyMove.hpp>
#include<QObject>
#include<VideoIO/CameraProxy.hpp>

namespace mfcv::gui {
    // TODO Renommer en fonction du type de paramètre d'Entré de la fonction (ex csptr_t image callback)
    struct CallbackAdapter : QObject {
        Q_OBJECT
        public:
        CallbackAdapter() = default;
        virtual ~CallbackAdapter() = default;
        MFCV_DEFAULT_COPY_MOVE(CallbackAdapter)
        CallbackAdapter(const CameraProxy::Callback_wptr_t& dat) : data(dat) { }

        CameraProxy::Callback_wptr_t data;
    };

    // TODO Renommer en fonction du type de paramètre d'Entré de la fonction (ex uptr_t image callback)
    struct TransformAdapter : QObject {
        Q_OBJECT
        public:
        TransformAdapter() = default;
        virtual ~TransformAdapter() = default;
        MFCV_DEFAULT_COPY_MOVE(TransformAdapter)

        using CallbackList = CallbackContainer<void, ImageAdapter::uptr_t&>;
        using Callback_t = std::function<void (ImageAdapter::uptr_t &)>;
        using Callback_sptr_t = std::shared_ptr<Callback_t>;
        using Callback_wptr_t = std::shared_ptr<Callback_t>;
        TransformAdapter(const Callback_wptr_t& dat) : data(dat) { }
        Callback_wptr_t data;
    };
}