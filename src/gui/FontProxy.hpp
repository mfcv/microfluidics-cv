//////////////////////////////////////////////////////////////////////////////
/// \file       FontProxy.hpp
/// \brief      Declaration of FontProxy which control the font access.
/// \copyright  2019 - Simon Dallaire (AGPLv3).
//////////////////////////////////////////////////////////////////////////////
//  This program is free software: you can redistribute it and/or modify
//  it under the terms of the GNU Affero General Public License as
//  published by the Free Software Foundation, either version 3 of the
//  License, or (at your option) any later version.
//
//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU Affero General Public License for more details.
//
//  You should have received a copy of the GNU Affero General Public License
//  along with this program.  If not, see <https://www.gnu.org/licenses/>.
//////////////////////////////////////////////////////////////////////////////
#pragma once
#include<QFontDatabase>
#include<QGuiApplication>

namespace mfcv::gui
{
    class FontProxy final
    {
    public:
        FontProxy(const QGuiApplication& /**/);
    private:
        void loadFonts(const char font_path[]) noexcept;
        QFontDatabase _fontsDatabase;
    };
}