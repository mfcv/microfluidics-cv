//////////////////////////////////////////////////////////////////////////////
/// \file       MainWindow.cpp
/// \brief      Implementation of the main window GUI.
/// \copyright  2019 - Simon Dallaire (AGPLv3)
//////////////////////////////////////////////////////////////////////////////
//  This program is free software: you can redistribute it and/or modify
//  it under the terms of the GNU Affero General Public License as
//  published by the Free Software Foundation, either version 3 of the
//  License, or (at your option) any later version.
//
//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU Affero General Public License for more details.
//
//  You should have received a copy of the GNU Affero General Public License
//  along with this program.  If not, see <https://www.gnu.org/licenses/>.
//////////////////////////////////////////////////////////////////////////////
#include "MainWindow.hpp"
#include "GuiLogger.hpp"
#include <Log.hpp>

#include <QQuickTextDocument>
#include <QTextDocument>
#include <Utilities/Reflection.hpp>
#include"VisionProxy.hpp"
#include"VideoSource.hpp"
#include"VideoRecorder.hpp"
#include"Icons.hpp"
#include "Greek.hpp"

using namespace mfcv;
using namespace mfcv::gui;
namespace
{
    constexpr char QmlUri[] = "mfcv.gui";
    constexpr int QmlMajor = 1;
    constexpr int QmlMinor = 0;

    template<typename T>
    inline void qmlRegister()
    {
        LOG(TRACE) << mfcv::getTypeName<T>();
        qmlRegisterType<T>(QmlUri,
                           QmlMajor,
                           QmlMinor,
                           getClassName<T>().c_str());
    }
}

void MainWindow::load()
{
    ::qmlRegister<GuiLogger>();
    ::qmlRegister<VisionProxy>();
    ::qmlRegister<VideoSource>();
    ::qmlRegister<VideoRecorder>();
    ::qmlRegister<Icons>();
    ::qmlRegister<Greek>();


    engine.load(QStringLiteral("qrc:/main.qml"));
    if (engine.rootObjects().isEmpty()){
        LOG(FATAL) << "Could not load GUI.";
    }
}