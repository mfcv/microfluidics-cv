//////////////////////////////////////////////////////////////////////////////
/// \file       GuiLogger.hpp
/// \brief      Declaration of the GUI Console.
/// \copyright  2019 - Simon Dallaire (AGPLv3).
//////////////////////////////////////////////////////////////////////////////
//  This program is free software: you can redistribute it and/or modify
//  it under the terms of the GNU Affero General Public License as
//  published by the Free Software Foundation, either version 3 of the
//  License, or (at your option) any later version.
//
//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU Affero General Public License for more details.
//
//  You should have received a copy of the GNU Affero General Public License
//  along with this program.  If not, see <https://www.gnu.org/licenses/>.
//////////////////////////////////////////////////////////////////////////////
#pragma once
#include<QObject>
#include<QString>
#include<g3log/logmessage.hpp>
#include<g3log/logworker.hpp>
#include<memory>
#include<atomic>
#include<Global.hpp>


class QQuickTextDocument;

namespace mfcv::gui
{
    class LogInterface
    {
    public:
        LogInterface() = default;
        virtual ~LogInterface() = default;
        virtual void log(g3::LogMessageMover logEntry) = 0;
    };

    class GuiSink final {
    public:
        GuiSink() = default;
        ~GuiSink() = default;
        void log(g3::LogMessageMover logEntry);
        void setProvider(LogInterface* provider);

    private:
        std::mutex m_mutex;
        LogInterface* m_provider{nullptr};
    };

    class GuiLogger : public QObject, public LogInterface
    {
    public:
        GuiLogger();
        virtual ~GuiLogger();

        void log(g3::LogMessageMover logEntry) override;
    private:
        inline QQuickTextDocument* document() const noexcept { return m_document; }
        void setDocument(QQuickTextDocument *document);
        QQuickTextDocument *m_document;
        std::unique_ptr<g3::SinkHandle<GuiSink>> m_handler;
        
        std::atomic_bool m_destroying{false};
        QString get_color(const int value);
        QString get_name(const LEVELS& level);

    ///////////// Qt properties, signals and slots /////////////
        Q_OBJECT
        Q_PROPERTY(QQuickTextDocument* document READ document WRITE setDocument NOTIFY documentChanged)
    public:
        Q_INVOKABLE void write(const QString& msg);
    signals:
        // Call QML
        void documentChanged();
        void updated(const QString &text);
    };
}