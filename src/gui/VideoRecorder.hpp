//////////////////////////////////////////////////////////////////////////////
/// \file       VideoRecorder.hpp
/// \brief      Declaration of VideoRecorder which control the file saving.
/// \copyright  2020 - Simon Dallaire (AGPLv3).
//////////////////////////////////////////////////////////////////////////////
//  This program is free software: you can redistribute it and/or modify
//  it under the terms of the GNU Affero General Public License as
//  published by the Free Software Foundation, either version 3 of the
//  License, or (at your option) any later version.
//
//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU Affero General Public License for more details.
//
//  You should have received a copy of the GNU Affero General Public License
//  along with this program.  If not, see <https://www.gnu.org/licenses/>.
//////////////////////////////////////////////////////////////////////////////
#pragma once
#include<QObject>
#include<VideoIO/VideoFileWriter.hpp>
#include<VideoIO/CameraProxy.hpp>

#include"Global.hpp"
#include"GuiAdapters.hpp"
#include"Icons.hpp"

namespace mfcv::gui
{
    class VideoRecorder : public QObject
    {
    public:
        VideoRecorder();
        virtual ~VideoRecorder();
    private:
        CameraProxy::Callback_sptr_t _writeCallback;
        std::future<void> last_result;
        VideoFileWriter::sptr_t _writer;
        bool isRecording{false};
        void startRecord(const QString& dir_name);
        std::future<void> stopRecord();

    ////////// Qt properties, signal and slots //////////
        Q_OBJECT
        MFCV_PROP_STR_GET(IconsFamily, Icons::FAM_ENTYPO())
        MFCV_PROP_STR_GET(RecordIcon, Icons::EN_RECORD())
        MFCV_PROP_STR_GET_SET(fileSufix, /*EmptyString*/ )
        MFCV_PROP_STR_GET_SET(customRecordIcon, Icons::EN_RECORD())
        MFCV_PROP_NUMBER_GET(int, Fps)
    public:
        Q_INVOKABLE QString defaultDir() const;
        Q_INVOKABLE void toggleRecord(const QString& dir_name); // TODO Enable codec choice
    signals:
        void frameListenerChanged(CallbackAdapter* callbackRawPtr);
    public slots:
        void onCustomRecordIconChanged();
    };
}