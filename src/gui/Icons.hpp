//////////////////////////////////////////////////////////////////////////////
/// \file       Icons.hpp
/// \brief      Declaration of the icons.
/// \copyright  2020 - Simon Dallaire (AGPLv3).
//////////////////////////////////////////////////////////////////////////////
//  This program is free software: you can redistribute it and/or modify
//  it under the terms of the GNU Affero General Public License as
//  published by the Free Software Foundation, either version 3 of the
//  License, or (at your option) any later version.
//
//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU Affero General Public License for more details.
//
//  You should have received a copy of the GNU Affero General Public License
//  along with this program.  If not, see <https://www.gnu.org/licenses/>.
//////////////////////////////////////////////////////////////////////////////
#pragma once
#include<QObject>
#include"../Global.hpp"

namespace mfcv::gui
{
    class Icons : public QObject
    {
    public:
        Icons() = default;
        virtual ~Icons() = default;

    ////////// Qt properties, signal and slots //////////
        Q_OBJECT

        //////////////////////////////
        // Entypo icons
        MFCV_PROP_STR_STATIC(FAM_ENTYPO, "entypo")
        MFCV_PROP_STR_STATIC(EN_PLAY, "\uE897")
        MFCV_PROP_STR_STATIC(EN_STOP, "\uE898")
        MFCV_PROP_STR_STATIC(EN_PAUSE, "\uE899")
        MFCV_PROP_STR_STATIC(EN_RECORD, "\uE89A")
        MFCV_PROP_STR_STATIC(EN_VIDEO, "\uE80D")
        MFCV_PROP_STR_STATIC(EN_LOCK_CLOSED, "\uE824")
        MFCV_PROP_STR_STATIC(EN_LOCK_OPENED, "\uE825")

        //////////////////////////////
        // Font_awesome icons
        MFCV_PROP_STR_STATIC(FAM_AWESOME, "font_awesome")
        MFCV_PROP_STR_STATIC(AW_FOLDER_OPENED, "\uE83E")
        MFCV_PROP_STR_STATIC(AW_CAMCORDER, "\uE80C")
        MFCV_PROP_STR_STATIC(AW_CAMERA, "\uE80E")
        MFCV_PROP_STR_STATIC(AW_VIDEO_FILE, "\uF1C8")
        MFCV_PROP_STR_STATIC(AW_IMAGE_FILE, "\uF1C5")
        MFCV_PROP_STR_STATIC(AW_ARROW_CW, "\uE867")
        MFCV_PROP_STR_STATIC(AW_CANCEL_CIRCLED, "\uE817")
        MFCV_PROP_STR_STATIC(AW_HAND_PAPER_O, "\uF256")

        //////////////////////////////
        // Fontelico icons
        MFCV_PROP_STR_STATIC(FAM_FONTELICO, "fontelico")
    };
}