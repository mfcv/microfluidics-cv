//////////////////////////////////////////////////////////////////////////////
/// \file       VisionProxy.h
/// \brief      Implementation of a class to link computed image for the GUI
/// \copyright  2019 - Simon Dallaire (AGPLv3)
//////////////////////////////////////////////////////////////////////////////
//  This program is free software: you can redistribute it and/or modify
//  it under the terms of the GNU Affero General Public License as
//  published by the Free Software Foundation, either version 3 of the
//  License, or (at your option) any later version.
//
//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU Affero General Public License for more details.
//
//  You should have received a copy of the GNU Affero General Public License
//  along with this program.  If not, see <https://www.gnu.org/licenses/>.
//////////////////////////////////////////////////////////////////////////////
#include "VisionProxy.hpp"

using namespace mfcv;
using namespace mfcv::gui;

namespace {
    constexpr MinMaxVal<double> initialRange{MAX<double>, MIN<double>};
}

void IndexProxy::setROI(const cv::Rect& rect) { 
    const LockGuard lock{aMutex};
    aROI = rect;
    aIsRoiSet = !aROI.empty();
}

MinMax<uint8_t> IndexProxy::getGrayThreashold(){
    const LockGuard lock{aMutex};
    return V;
}

/// Return a copy of HSVTreshold
/// to allow a thread safe utilisation
HSVTreshold IndexProxy::getHSVTreshold() const {
    const LockGuard lock{aMutex};
    return aHsvThreshold;
}

/// Return a copy of ROI
/// to allow a thread safe utilisation
cv::Rect IndexProxy::getROI() const { 
    const LockGuard lock{aMutex};
    return aROI;
}
bool IndexProxy::isRoiSet() { return aIsRoiSet; }

void IndexProxy::onHChanged(int min, int max) {
    const LockGuard lock{aMutex};
    H = MinMax<uint8_t>(min, max);
    aHsvThreshold = HSVTreshold(H, S, V);
}
void IndexProxy::onSChanged(int min, int max) {
    const LockGuard lock{aMutex};
    S = MinMax<uint8_t>(min, max);
    aHsvThreshold = HSVTreshold(H, S, V);
}
void IndexProxy::onVChanged(int min, int max) {
    const LockGuard lock{aMutex};
    V = MinMax<uint8_t>(min, max);
    aHsvThreshold = HSVTreshold(H, S, V);
}

VisionProxy::VisionProxy() {
    _GrayMean.setMinMax(0,255);
    _Stddev.setMinMax(0,MAX<double>);
    _Ami.setMinMax(0,1);
    _Rmi.setMinMax(MIN<double>,1);
    _Tmi.setMinMax(0,1);

    connect(_indexProxy.get(), &IndexProxy::indexChanged, this, &VisionProxy::onIndexChanged);
    connect(this, &VisionProxy::HChanged, _indexProxy.get(), &IndexProxy::onHChanged);
    connect(this, &VisionProxy::SChanged, _indexProxy.get(), &IndexProxy::onSChanged);
    connect(this, &VisionProxy::VChanged, _indexProxy.get(), &IndexProxy::onVChanged);
    connect(this, &VisionProxy::HChanged, this, &VisionProxy::onHSVChanged);
    connect(this, &VisionProxy::SChanged, this, &VisionProxy::onHSVChanged);
    connect(this, &VisionProxy::VChanged, this, &VisionProxy::onHSVChanged);
    connect(this, &VisionProxy::StdDevInitChanged, this, &VisionProxy::onStdDevInitChanged);
}

void VisionProxy::setROI(const int x0, const int y0, const int x1, const int y1) {
    // FIXME ensure the ROI is not bigger than the image
    const MinMax<int,int> x{x0,x1};
    const MinMax<int,int> y{y0,y1};
    setROI_X0(x.getMin());
    setROI_Y0(y.getMin());
    setROI_X1(x.getMax());
    setROI_Y1(y.getMax());

    const int width = getROI_X1() - getROI_X0();
    const int height = getROI_Y1() - getROI_Y0();
    if(width > 0 && height > 0) {
        _indexProxy->setROI(cv::Rect(getROI_X0(), getROI_Y0(), width, height));
    }
    else {
        _indexProxy->setROI(cv::Rect());
    }
    if(_current_transform) {
        resetRanges();
    }
    emit transformChanged();
}

void VisionProxy::stopTransform() {
    _current_transform.reset();
    emit transformChanged();
}

void VisionProxy::setTmiTransformHsv() {
    IndexProxy::sptr_t shIndexProxy = _indexProxy;
    initTransform([shIndexProxy](IndexCalculator& calculator) -> double {
            return calculator.computeTmiHSV(shIndexProxy->getHSVTreshold(), true);
    });
}

void VisionProxy::setTmiTransformGray() {
    IndexProxy::sptr_t shIndexProxy = _indexProxy;
    initTransform([shIndexProxy](IndexCalculator& calculator) -> double {
            return calculator.computeTmiGray(shIndexProxy->getGrayThreashold(), true);
    });
}

void VisionProxy::initTransform(const std::function<double (IndexCalculator&)> &tmiTransform) {
    resetRanges();
    IndexProxy::sptr_t indexProxy = _indexProxy;
    _current_transform = std::make_shared<TransformAdapter::Callback_t>
        ([indexProxy, tmiTransform](ImageAdapter::uptr_t& frameToUpdate) {
            cv::Mat bgrRoi = indexProxy->isRoiSet()
                           ? frameToUpdate->getBgrImg()(indexProxy->getROI())
                           : frameToUpdate->getBgrImg();

            IndexCalculator calculator{bgrRoi};
            const double meanGray = calculator.getLazyMeanGray();
            const double stdDevGray = calculator.getLazyStdDevGray();
            const double ami = calculator.computeAmi();
            const double rmi = calculator.computeRmi(indexProxy->stdDevInitial);
            const double tmi = tmiTransform(calculator);

            cv::Mat &outImg = frameToUpdate->getBgrImg();
            std::stringstream stream;
            stream << "AMI: " << std::fixed << std::setprecision(3) << ami;
            stream << " RMI: " << std::fixed << std::setprecision(3) << rmi;
            stream << " TMI: " << std::fixed << std::setprecision(3) << tmi;
            const std::string label = stream.str();

            // FIXME should be set by the user
            // FIXME scale font with the image
            // https://www.codesofinterest.com/2017/07/more-fonts-on-opencv.html
            const double fontScale{2};
            const int thickness = 2;

            const cv::Point origin{1, outImg.rows-5};
            //cv::FONT_HERSHEY_COMPLEX_SMALL -> if the image is very small
            //cv::FONT_HERSHEY_TRIPLEX
            constexpr auto fontFace{cv::FONT_HERSHEY_TRIPLEX};
            int baseline = 0;
            cv::Size text = cv::getTextSize(label, fontFace, fontScale, thickness, &baseline);
            cv::rectangle(outImg,
                          origin + cv::Point(0, baseline),
                          origin + cv::Point(text.width, -text.height),
                          CV_RGB(0,0,0),
                          cv::FILLED,
                          cv::LINE_AA);
            cv::putText(outImg,
                        label,
                        origin,
                        fontFace,
                        fontScale,
                        CV_RGB(0,255,0),
                        thickness,
                        cv::LINE_AA);

            emit indexProxy->indexChanged(meanGray, stdDevGray, ami, rmi, tmi);
    });
    TransformAdapter adaptor(_current_transform);
    emit transformListenerChanged(&adaptor);
    emit transformChanged();
}

void VisionProxy::resetRanges() {
    _GrayMean= initialRange;
    _Stddev= initialRange;
    _Ami= initialRange;
    _Rmi= initialRange;
    _Tmi= initialRange;
}

void VisionProxy::onIndexChanged(double meanGray, double stdDevGray, double ami, double rmi, double tmi) {
    if(   getRmiMin() == MIN<double>
       || getRmiMax() == MAX<double>) {
        _Rmi= initialRange;
    }

    setGrayMean_ConformRange(meanGray);
    setStddev_ConformRange(stdDevGray);
    setAmi_ConformRange(ami);
    setRmi_ConformRange(rmi);
    setTmi_ConformRange(tmi);
    
    if(_autoSeed) {
        setStdDevInit(getStddevMax());
    }
    _indexProxy->stdDevInitial = getStdDevInit();
}

void VisionProxy::setStdDevInitMode(bool autoSeed) {
    if(autoSeed) {
        _Stddev= initialRange;
    }
    _autoSeed = autoSeed;
}

void VisionProxy::onStdDevInitChanged() {
    _Rmi.setMinMax(initialRange.getMinMax());
    if(!_autoSeed && _current_transform) {
        _Stddev= initialRange;
        emit transformChanged();
    }
}

void VisionProxy::onHSVChanged() {
    _Tmi= initialRange;
    emit transformChanged();
}