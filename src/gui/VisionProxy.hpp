//////////////////////////////////////////////////////////////////////////////
/// \file       VisionProxy.hpp
/// \brief      Definition of a class to link computed image for the GUI
/// \copyright  2019 - Simon Dallaire (AGPLv3)
//////////////////////////////////////////////////////////////////////////////
//  This program is free software: you can redistribute it and/or modify
//  it under the terms of the GNU Affero General Public License as
//  published by the Free Software Foundation, either version 3 of the
//  License, or (at your option) any later version.
//
//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU Affero General Public License for more details.
//
//  You should have received a copy of the GNU Affero General Public License
//  along with this program.  If not, see <https://www.gnu.org/licenses/>.
//////////////////////////////////////////////////////////////////////////////
#pragma once
#include<QQuickImageProvider>
#include<VideoIO/CameraProxy.hpp>
#include<Utilities/MinMax.hpp>
#include"../Global.hpp"
#include"GuiAdapters.hpp"
#include<QObject>
#include<opencv2/opencv.hpp>
#include<atomic>
#include<Vision/IndexCalculator.hpp>
#include<mutex>

namespace mfcv::gui {
    class IndexProxy : public QObject // FIXME Move to it own file
    {
    public:
        MFCV_PTR_T(IndexProxy)

        IndexProxy() = default;
        virtual ~IndexProxy() = default;

        std::atomic<double> stdDevInitial{0.0};
        void setROI(const cv::Rect& rect);
        
        MinMax<uint8_t> getGrayThreashold();
        HSVTreshold getHSVTreshold() const;
        cv::Rect getROI() const;
        bool isRoiSet();

    private:
        HSVTreshold aHsvThreshold;
        MinMax<uint8_t> H,S,V;
        cv::Rect aROI;
        std::atomic_bool aIsRoiSet{false};

        using LockGuard = std::lock_guard<std::recursive_mutex>;
        mutable std::recursive_mutex aMutex;

        //////// Qt definitions ////////
        Q_OBJECT
    signals:
        void indexChanged(double meanGray, double stdDevGray, double ami, double rmi, double tmi);
    public slots:
        void onHChanged(int min, int max);
        void onSChanged(int min, int max);
        void onVChanged(int min, int max);
    };

    class VisionProxy : public QObject
    {
    public:
        VisionProxy();
        virtual ~VisionProxy() = default;
    private:
        TransformAdapter::Callback_sptr_t _current_transform;
        IndexProxy::sptr_t _indexProxy{IndexProxy::mk_sptr()};
        std::atomic_bool _autoSeed{true};

        void initTransform(const std::function<double (mfcv::IndexCalculator&)> &tmiTransform);
        void resetRanges();


    //////// Qt definitions ////////
    Q_OBJECT
    MFCV_PROP_NUMBER_GET(int, ROI_X0)
    MFCV_PROP_NUMBER_GET(int, ROI_Y0)
    MFCV_PROP_NUMBER_GET(int, ROI_X1)
    MFCV_PROP_NUMBER_GET(int, ROI_Y1)

    MFCV_PROP_NUMBER_GET_SET(double, StdDevInit)

    MFCV_PROP_MIN_MAX(int, uint8_t, H)
    MFCV_PROP_MIN_MAX(int, uint8_t, S)
    MFCV_PROP_MIN_MAX(int, uint8_t, V)

    MFCV_PROP_MIN_MAX_VAL(double, Tmi)
    MFCV_PROP_MIN_MAX_VAL(double, Ami)
    MFCV_PROP_MIN_MAX_VAL(double, Rmi)
    MFCV_PROP_MIN_MAX_VAL(double, GrayMean)
    MFCV_PROP_MIN_MAX_VAL(double, Stddev)

    public:
        Q_INVOKABLE void setROI(const int x0, const int y0, const int x1, const int y1);
        Q_INVOKABLE void stopTransform();
        Q_INVOKABLE void setTmiTransformHsv();
        Q_INVOKABLE void setTmiTransformGray();
        Q_INVOKABLE void setStdDevInitMode(bool autoSeed);
    
    signals:
        void transformListenerChanged(TransformAdapter *callbackRawPtr);
        void transformChanged();
    public slots:
        void onIndexChanged(double meanGray, double stdDevGray, double ami, double rmi, double tmi);
        void onStdDevInitChanged();
        void onHSVChanged();
    };
}
