//////////////////////////////////////////////////////////////////////////////
/// \file       ImageAdapter.hpp
/// \brief      Pylon camera wraper
/// \copyright  2019 - Simon Dallaire (AGPLv3)
//////////////////////////////////////////////////////////////////////////////
//  This program is free software: you can redistribute it and/or modify
//  it under the terms of the GNU Affero General Public License as
//  published by the Free Software Foundation, either version 3 of the
//  License, or (at your option) any later version.
//
//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU Affero General Public License for more details.
//
//  You should have received a copy of the GNU Affero General Public License
//  along with this program.  If not, see <https://www.gnu.org/licenses/>.
//////////////////////////////////////////////////////////////////////////////
#pragma once
#ifdef WITH_PYLON
#include"ICamProxy.hpp"
#include<memory>
#include<Ptr.hpp>

namespace mfcv
{
    class PylonCamProxy final : public ICamProxy, public std::enable_shared_from_this<PylonCamProxy>
    {
    public:
        MFCV_SHARED_FROM_THIS(PylonCamProxy)
        ~PylonCamProxy();

        std::vector<CamItem> listAvailableCamera() override;

        void open(const CamItem& cam) override;
        bool grab(ImageAdapter::sptr_t& ret_img) override;
        void stop() override;

    private:
        // To keep Pylon dependencie private,
        // the private implementation is hidden
        struct Impl;
        std::unique_ptr<Impl> _pImpl;
        Impl& _getImpl();

        PylonCamProxy() noexcept;
    };
}
#endif