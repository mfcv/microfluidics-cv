//////////////////////////////////////////////////////////////////////////////
/// \file       VideoFileWriter.hpp
/// \brief      Declaration of the video file output
/// \copyright  2019 - Simon Dallaire (AGPLv3)
//////////////////////////////////////////////////////////////////////////////
//  This program is free software: you can redistribute it and/or modify
//  it under the terms of the GNU Affero General Public License as
//  published by the Free Software Foundation, either version 3 of the
//  License, or (at your option) any later version.
//
//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU Affero General Public License for more details.
//
//  You should have received a copy of the GNU Affero General Public License
//  along with this program.  If not, see <https://www.gnu.org/licenses/>.
//////////////////////////////////////////////////////////////////////////////
#pragma once
#include<opencv2/opencv.hpp>
#include"ImageAdapter.hpp"
#include<Concurrency/ConcurentQueue.hpp>
#include<atomic>
#include<Concurrency/ThreadLoop.hpp>
#include<future>
#include<Ptr.hpp>
#include"../Utilities/Mean.hpp"

///////////////////////////////////////////
/// Declare a mfcv::Codec value.
/// Ex: MFCV_CODEC(H264) -> mfcv::Codec::H264
///////////////////////////////////////////
#define MFCV_CODEC(NAME) NAME = FourCC(#NAME)

namespace mfcv
{
    ///////////////////////////////////////////
    /// Compute the FourCC value from it name.
    ///////////////////////////////////////////
    inline constexpr int FourCC(const char fourcc[]) {
        return (((fourcc[0]) & 255)
             + (((fourcc[1]) & 255) << 8)
             + (((fourcc[2]) & 255) << 16)
             + (((fourcc[3]) & 255) << 24));
    }

    // FIXME Rename it format, not codec...
    ///////////////////////////////////////////
    /// Enum to list knowend formats.
    ///////////////////////////////////////////
    enum class Codec : int {
        // Lossless
        MFCV_CODEC(FFV1), // Slow
        MFCV_CODEC(HFYU), // Not tested
        MFCV_CODEC(LAGS), // Not tested
        // Lossly
        MFCV_CODEC(X264), // Use all cores
        MFCV_CODEC(H264), // Not tested
        MFCV_CODEC(H265), // Not tested
        MFCV_CODEC(XVID), // Not tested
    };

    class VideoFileWriter final : public std::enable_shared_from_this<VideoFileWriter>
    {
    private:
        // TODO Set quality: bool cv::VideoWriter::set(cv::VIDEOWRITER_PROP_QUALITY, double)
        //      and the number of threads: bool cv::VideoWriter::set(cv::VIDEOWRITER_PROP_STRIPES, double)
        VideoFileWriter() = default;
        explicit VideoFileWriter(const Codec codec,
                                 const std::string& fileName);
    public:
        MFCV_SHARED_FROM_THIS(VideoFileWriter)
        ~VideoFileWriter();

        static constexpr Codec DEFAULT_CODEC{Codec::FFV1};

        void write_async(ImageAdapter::csptr_t frame);
        std::future<void> close_async();
        void release();
        inline double getMeanFps() const noexcept { return _lastMeanFps; }

        static constexpr char EXTENSION[]{".mkv"};

    private:
        void _open(const cv::Mat &cv_frame, double fps);
        void _write(const ImageAdapter& frame, bool warn = true);
        void _close();

    private:
        cv::VideoWriter _writer;
        const std::underlying_type_t<Codec> _codec;
        std::string _fileName;
        std::atomic_bool _isFinishing{false};
        std::atomic<double> _fps{0.0};
        double _lastMeanFps{0.0};
        Mean<double> _meanFps{50};

        using queue_t = ConcurentQueue<0, ImageAdapter::csptr_t>;
        queue_t::sptr_t _queue = queue_t::mk_sptr();
        ThreadLoop<0>::sptr_t _thread_loop = ThreadLoop<0>::mk_sptr(appendToClassName<VideoFileWriter>("WriteLoop"));
    };
}