//////////////////////////////////////////////////////////////////////////////
/// \file       OcvCamProxy.hpp
/// \brief      OpenCV camera wraper
/// \copyright  2019 - Simon Dallaire (AGPLv3)
//////////////////////////////////////////////////////////////////////////////
//  This program is free software: you can redistribute it and/or modify
//  it under the terms of the GNU Affero General Public License as
//  published by the Free Software Foundation, either version 3 of the
//  License, or (at your option) any later version.
//
//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU Affero General Public License for more details.
//
//  You should have received a copy of the GNU Affero General Public License
//  along with this program.  If not, see <https://www.gnu.org/licenses/>.
//////////////////////////////////////////////////////////////////////////////
#pragma once
#include"ICamProxy.hpp"
#include<Utilities/Timer.hpp>
#include<Ptr.hpp>

namespace mfcv
{
    class OcvCamProxy final : public ICamProxy, public std::enable_shared_from_this<OcvCamProxy>
    {
    public:
        MFCV_SHARED_FROM_THIS(OcvCamProxy)
        ~OcvCamProxy() = default;

        std::vector<CamItem> listAvailableCamera() override;
        void open(const CamItem& cam) override;
        bool openFile(const std::string& fileName);

        bool grab(ImageAdapter::sptr_t& ret_img) override;
        void stop() override;

    private:
        OcvCamProxy() = default;
        cv::VideoCapture _ocv_cam;
        double _mWaitTimeMs{0.0};
        Timer timer;
    };
}