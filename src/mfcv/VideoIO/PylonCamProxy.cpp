//////////////////////////////////////////////////////////////////////////////
/// \file       PylonCamProxy.hpp
/// \brief      Wrap the Pylon API
/// \copyright  2019 - Simon Dallaire (AGPLv3)
//////////////////////////////////////////////////////////////////////////////
//  This program is free software: you can redistribute it and/or modify
//  it under the terms of the GNU Affero General Public License as
//  published by the Free Software Foundation, either version 3 of the
//  License, or (at your option) any later version.
//
//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU Affero General Public License for more details.
//
//  You should have received a copy of the GNU Affero General Public License
//  along with this program.  If not, see <https://www.gnu.org/licenses/>.
//////////////////////////////////////////////////////////////////////////////
#ifdef WITH_PYLON
#include"PylonCamProxy.hpp"
#include <pylon/PylonIncludes.h>
#include<Log.hpp>

// TODO remove pylon dependency if asked
using namespace mfcv;

namespace
{
    #define PYLON_CATCH(msg)\
    catch(const Pylon::GenericException& e)\
    {\
        LOG(ERROR) << mfcv::append(msg," - ",e.GetDescription());\
    }\
    MFCV_CATCH(msg)

    // This struct keep a reference to the pylon image so the buffer is not deleted.
    class PylonImg final : public ImageAdapter {
    public:
        PylonImg() = default;
        PylonImg(Pylon::CPylonImage&& img)
            : ImageAdapter(cv::Mat(img.GetHeight(),
                                   img.GetWidth(),
                                   CV_8UC3,
                                   img.GetBuffer())),
              _pylon_img(std::move(img))
        { }

    private:
        Pylon::CPylonImage _pylon_img;
    };
}

struct PylonCamProxy::Impl
{
private:
    PylonCamProxy::wptr_t parent;
    Pylon::PylonAutoInitTerm autoInitTerm;
    Pylon::CImageFormatConverter converter;
    Pylon::DeviceInfoList_t devices_list;
    std::unique_ptr<Pylon::CInstantCamera> inst_cam;
    static constexpr int timout = 5 * 1000;

public:
    Impl(PylonCamProxy::wptr_t parent)
        :parent(parent)
    {
            converter.OutputPixelFormat = Pylon::PixelType_BGR8packed;
    }
    ~Impl() { LOG(TRACE); }

    std::vector<CamItem> listAvailableCamera()
    {
            std::vector<CamItem> ret_val;
        const int nb_devices = Pylon::CTlFactory::GetInstance().EnumerateDevices(devices_list);
        if(nb_devices <= 0)
        {
            return ret_val;
        }

        ret_val.reserve(devices_list.size());
        for(size_t i{0}; i < devices_list.size(); ++i)
        {
            const auto& device = devices_list[i];
            ret_val.emplace_back(
                CamItem(i,
                device.GetFriendlyName().c_str(),
                parent.lock()
                ));
        }
        return ret_val;
    }

    void open(size_t index)
    {
            if(   inst_cam
           || devices_list.empty()
           || index >= devices_list.size() )
        {
            LOG(ERROR) << mfcv::append("Device: '",index,"' was not found.");
            return;
        }

        const auto createDevice = [](auto device){ return Pylon::CTlFactory::GetInstance().CreateDevice(device); };
        inst_cam = std::make_unique<Pylon::CInstantCamera>(
            createDevice(devices_list[index]), Pylon::Cleanup_Delete);
        inst_cam->StartGrabbing(Pylon::GrabStrategy_LatestImageOnly, Pylon::GrabLoop_ProvidedByUser);
        LOG(INFO) << mfcv::append("Using device: ",inst_cam->GetDeviceInfo().GetFriendlyName());
    }

    ImageAdapter::sptr_t read()
    {
            const auto make_default = [](){ return std::make_shared<PylonImg>(); };
        if(!inst_cam)
        {
            return make_default();
        }

        Pylon::CGrabResultPtr ptrGrabResult;
        inst_cam->RetrieveResult(timout, ptrGrabResult, Pylon::TimeoutHandling_Return);
        if (ptrGrabResult->GrabSucceeded())
        {
            Pylon::CPylonImage img;
            converter.Convert(img, ptrGrabResult);
            return std::make_shared<PylonImg>(std::move(img));
        }
        else
        {
            LOG(ERROR) << "The camera timed out.";
            return make_default();
        }
    }

    void stop()
    {
            if(!inst_cam)
        {
            return;
        }
        if(inst_cam->IsGrabbing())
        {
            inst_cam->StopGrabbing();
        }
        if(inst_cam->IsPylonDeviceAttached())
        {
            inst_cam->DetachDevice();
        }
        inst_cam.reset();
    }
};

PylonCamProxy::PylonCamProxy() noexcept { LOG(TRACE); }

PylonCamProxy::~PylonCamProxy() { LOG(TRACE); }

PylonCamProxy::Impl& PylonCamProxy::_getImpl()
{
    if(!_pImpl)
    {
        _pImpl = std::make_unique<PylonCamProxy::Impl>(weak_from_this());
    }
    return *_pImpl;
}

std::vector<CamItem> PylonCamProxy::listAvailableCamera()
{
    return _getImpl().listAvailableCamera();
}

void PylonCamProxy::open(const CamItem& cam )
{
    _getImpl().open(cam.id);
}

bool PylonCamProxy::grab(ImageAdapter::sptr_t& ret_img)
{
    bool isValid{false};
    try
    {
        ret_img = _getImpl().read();
        isValid = !ret_img->cgetBgrImg().empty();

        // FIXME Move this part to the VideoRecorder::startRecord
        //       There is allready code there
        if(isValid) {
            cv::pyrDown(ret_img->cgetBgrImg(), ret_img->getBgrImg());
        }
        
    }
    PYLON_CATCH("Could not grab an image.")
    return isValid;
}

void PylonCamProxy::stop()
{
    if(_pImpl) // Don't create it for nothing
    {
        _pImpl->stop();
    }
}
#endif