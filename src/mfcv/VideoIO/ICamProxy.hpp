//////////////////////////////////////////////////////////////////////////////
/// \file       ICamProxy.hpp
/// \brief      Interface for camera provider
/// \copyright  2019 - Simon Dallaire (AGPLv3)
//////////////////////////////////////////////////////////////////////////////
//  This program is free software: you can redistribute it and/or modify
//  it under the terms of the GNU Affero General Public License as
//  published by the Free Software Foundation, either version 3 of the
//  License, or (at your option) any later version.
//
//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU Affero General Public License for more details.
//
//  You should have received a copy of the GNU Affero General Public License
//  along with this program.  If not, see <https://www.gnu.org/licenses/>.
//////////////////////////////////////////////////////////////////////////////
#pragma once

#include"ImageAdapter.hpp"
#include<vector>
#include<memory>
#include<Ptr.hpp>

namespace mfcv
{
    struct CamItem;
    class ICamProxy
    {
    public:
        MFCV_PTR_T(ICamProxy)
        MFCV_DELETE_CP_MV(ICamProxy)

        ICamProxy() noexcept { LOG(TRACE); }
        
        virtual ~ICamProxy() { LOG(TRACE); }

        virtual std::vector<CamItem> listAvailableCamera() = 0;

        virtual void open(const CamItem& cam) = 0;
        virtual bool grab(ImageAdapter::sptr_t& ret_img) = 0;
        virtual void stop() = 0;
    };

    struct CamItem
    {
        MFCV_PTR_T(CamItem)
        MFCV_DEFAULT_COPY_MOVE(CamItem)

        CamItem() { LOG(TRACE); }
        CamItem(size_t cam_id, const std::string& cam_name, ICamProxy::sptr_t cam)
            : id(cam_id),
              display_name(cam_name),
              provider(cam)
        {
            LOG(TRACE) << append("id: ", id, " display_name: ", display_name );
            
            const bool isNull{!provider};
            LOG_IF(ERROR, isNull) << "The specified provider is not defined.";
        }

        virtual ~CamItem() { LOG(TRACE); }

        size_t id{std::numeric_limits<size_t>::max()};
        std::string display_name;
        ICamProxy::sptr_t provider;
    };

    inline ICamProxy& operator>>(ICamProxy& icam, ImageAdapter::sptr_t& ret_img) {
            icam.grab(ret_img);
        return icam;
    }
}