//////////////////////////////////////////////////////////////////////////////
/// \file       CameraProxy.hpp
/// \brief      Declaration of the video camera input
/// \copyright  2019 - Simon Dallaire (AGPLv3)
//////////////////////////////////////////////////////////////////////////////
//  This program is free software: you can redistribute it and/or modify
//  it under the terms of the GNU Affero General Public License as
//  published by the Free Software Foundation, either version 3 of the
//  License, or (at your option) any later version.
//
//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU Affero General Public License for more details.
//
//  You should have received a copy of the GNU Affero General Public License
//  along with this program.  If not, see <https://www.gnu.org/licenses/>.
//////////////////////////////////////////////////////////////////////////////
#include"CameraProxy.hpp"
#include <Log.hpp>
#include"OcvCamProxy.hpp"
#include"PylonCamProxy.hpp"
#include"../Utilities/Mean.hpp"

using namespace mfcv;

CameraProxy::CameraProxy() {
}

CameraProxy::~CameraProxy() {
    stop();
}

std::vector<std::string> CameraProxy::listAvailableCamera(bool reset) {
    _buildCamLst(reset);
    std::vector<std::string> ret_val;
    ret_val.reserve(_cam_lst.size());
    for(const auto& item : _cam_lst) {
        ret_val.emplace_back(item.display_name);
    }
    return ret_val;
}

void CameraProxy::readFile(const std::string& fileName) {
    autolock_t read_lock(_read_mut);
    stop(); // stop will reset everithing

    auto cam_provider = OcvCamProxy::mk_sptr();
    cam_provider->openFile(fileName);
    _startLoop(std::move(cam_provider));
}

void CameraProxy::start(int id) {
    if(   _cam_id == id || id < 0) {
        return;
    }
    autolock_t read_lock(_read_mut);

    stop(); // stop will reset everithing
    _buildCamLst(false);
    _cam_id = id;
    if(static_cast<size_t>(_cam_id) < _cam_lst.size()) {
        auto cam_item = _cam_lst[_cam_id];
        cam_item.provider->open(cam_item);
        _startLoop(cam_item.provider);
    }
    else {
        LOG(ERROR) << mfcv::append("Camera '",_cam_id,"' not found.");
    }
}

void CameraProxy::_startLoop(ICamProxy::sptr_t provider) {
    if(!_started) {
        if(!provider) {
            LOG(ERROR) << mfcv::append("The camera provider is NULL");
            return;
        }
        _started = true;
        LOG(DEBUG) << "Starting loop";

        auto handlePtr = mfcv::mk_sptr<std::future<void>>();
        auto meanFpsPtr = mfcv::mk_sptr<Mean<double>>(30);
        auto lastMeanFpsPtr = mfcv::mk_sptr<int64_t>(0);
        auto& listeners = _listener_lst;
        _loop->start([provider, listeners, meanFpsPtr, lastMeanFpsPtr, handlePtr](){
            LOG(TRACE) << appendToClassName<CameraProxy>("GrabLamda");
            ImageAdapter::sptr_t frame;
            RaiiTimer timer;
            *provider >> frame;
            
            meanFpsPtr->add(timer.GetElapsedTimeInFps());
            const double currentMean = meanFpsPtr->mean();
            int64_t &lastMean = *lastMeanFpsPtr;
            constexpr double diff{0.5};
            if(   to<double>(lastMean) < currentMean-diff
               || to<double>(lastMean) > currentMean+diff) {
                lastMean = currentMean;
                meanFpsPtr->setMaxCount(to<size_t>(currentMean));
            }
            frame->setFps(currentMean);

            // FIXME Use a thread pool instead
            *handlePtr = std::async(std::launch::async,
                             [frame, listeners](){
                                 listeners->notifyListeners(frame);
                             });
        });
    }
}

void CameraProxy::stop() {
    if(_started) {
        _started = false;

        LOG(DEBUG) << "Stoping loop";
        _loop->stop();
        if(_cam_id >= 0)
        {
            _cam_lst[_cam_id].provider->stop();
            _cam_id = -1;
        }
    }
}

void CameraProxy::_buildCamLst(bool reset) {
    if(reset || _cam_lst.empty()) {
        autolock_t lock{_cam_lst_mut};
        _cam_lst.clear();
        auto ocv_proxy = OcvCamProxy::mk_sptr();
        _cam_lst = ocv_proxy->listAvailableCamera();
        _addPylonCam();
    }
}

void CameraProxy::_addPylonCam() {
#ifdef WITH_PYLON
    auto pylon_proxy = PylonCamProxy::mk_sptr();
        auto pylon_cam_lst = pylon_proxy->listAvailableCamera();

        _cam_lst.reserve(_cam_lst.size() + pylon_cam_lst.size());
        const auto build_ret_val = [&](auto source) {
            for(auto&& item : source)
            {
                _cam_lst.emplace_back(std::move(item));
            }
        };
        build_ret_val(pylon_cam_lst);
#endif
}

void CameraProxy::addListener(Callback_wptr_t listener) {
    _listener_lst->addListener(listener);
}