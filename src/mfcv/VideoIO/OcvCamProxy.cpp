//////////////////////////////////////////////////////////////////////////////
/// \file       OcvCamProxy.hpp
/// \brief      OpenCV camera wraper
/// \copyright  2019 - Simon Dallaire (AGPLv3)
//////////////////////////////////////////////////////////////////////////////
//  This program is free software: you can redistribute it and/or modify
//  it under the terms of the GNU Affero General Public License as
//  published by the Free Software Foundation, either version 3 of the
//  License, or (at your option) any later version.
//
//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU Affero General Public License for more details.
//
//  You should have received a copy of the GNU Affero General Public License
//  along with this program.  If not, see <https://www.gnu.org/licenses/>.
//////////////////////////////////////////////////////////////////////////////
#include"OcvCamProxy.hpp"
#include<Log.hpp>
#include<Concurrency/ThreadLoop.hpp>

using namespace mfcv;

std::vector<CamItem> OcvCamProxy::listAvailableCamera()
{
    std::vector<CamItem> ret_val;
    ret_val.reserve(10);

    cv::VideoCapture testCamInput;
    cv::Mat testFrame;
    size_t i = 0;
    while (testCamInput.open(i)) {
        if(i > ret_val.capacity()) {
            ret_val.reserve(i + 10);
        }
        testCamInput >> testFrame;
        if (!testFrame.empty()) {
            ret_val.emplace_back(i, "Camera: " + std::to_string(i), shared_from_this());
        }
        ++i;
    }
    return ret_val;
}

void OcvCamProxy::open(const CamItem& cam)
{
    LOG(INFO) << mfcv::append("Opening camera '",std::to_string(cam.id), "'.");
    _ocv_cam.open(cam.id);
    _mWaitTimeMs = 0.0;
    timer.Stop();
}

bool OcvCamProxy::openFile(const std::string& fileName)
{
    LOG(INFO) << mfcv::append("Opening file '", fileName, "'.");
    if(! _ocv_cam.open(fileName) ) {
        LOG(ERROR) << mfcv::append("Could not read the file '", fileName, "'.");
        return false;
    }

    const double fps = _ocv_cam.get(cv::CAP_PROP_FPS);
    if(fps>0){
        _mWaitTimeMs = 1000.0/fps;
        timer.Start();
    }
    return true;
}

bool OcvCamProxy::grab(ImageAdapter::sptr_t& ret_img)
{
    if(_mWaitTimeMs > 0) {
        double remaining =_mWaitTimeMs - timer.GetElapsedTimeInMs();
        if(remaining > 0.0) {
            mfcv::wait_ms(remaining);
        }
        timer.Start();
    }
    cv::Mat bgrframe;
    _ocv_cam >> bgrframe;
    if(bgrframe.empty() && _mWaitTimeMs > 0.0) {
        _ocv_cam.set(cv::CAP_PROP_POS_FRAMES, 0);
        _ocv_cam >> bgrframe;
    }
    ret_img = std::make_shared<ImageAdapter>(std::move(bgrframe));
    return !ret_img->cgetBgrImg().empty();
}

void OcvCamProxy::stop()
{
    _ocv_cam.release();
}