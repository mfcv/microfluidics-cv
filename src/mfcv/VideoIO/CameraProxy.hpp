//////////////////////////////////////////////////////////////////////////////
/// \file       CameraProxy.hpp
/// \brief      Declaration of the video camera input
/// \copyright  2019 - Simon Dallaire (AGPLv3)
//////////////////////////////////////////////////////////////////////////////
//  This program is free software: you can redistribute it and/or modify
//  it under the terms of the GNU Affero General Public License as
//  published by the Free Software Foundation, either version 3 of the
//  License, or (at your option) any later version.
//
//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU Affero General Public License for more details.
//
//  You should have received a copy of the GNU Affero General Public License
//  along with this program.  If not, see <https://www.gnu.org/licenses/>.
//////////////////////////////////////////////////////////////////////////////
#pragma once
#include<vector>
#include<string>
#include<opencv2/opencv.hpp>
#include<Concurrency/ThreadLoop.hpp>
#include<atomic>
#include"ICamProxy.hpp"
#include<memory>
#include<Ptr.hpp>
#include"../Concurrency/CallbackContainer.hpp"

namespace mfcv
{
    class CameraProxy final : public std::enable_shared_from_this<CameraProxy>
    {
    public:
        MFCV_SHARED_FROM_THIS(CameraProxy)
        ~CameraProxy();

        using CallbackList = CallbackContainer<void, const ImageAdapter::csptr_t&>;
        using Callback_t = std::function<void (const ImageAdapter::csptr_t&)>;
        using Callback_sptr_t = std::shared_ptr<Callback_t>;
        using Callback_wptr_t = std::shared_ptr<Callback_t>;
        void addListener(Callback_wptr_t listener);

        std::vector<std::string> listAvailableCamera(bool reset);
        void start(int cam_id);
        void readFile(const std::string& fileName);

        void stop();
    private:
        CameraProxy();

        std::vector<CamItem> _cam_lst; // TODO move in it own object or struct
        void _buildCamLst(bool reset);
        void _addPylonCam();
        void _startLoop(ICamProxy::sptr_t provider);

        // TODO set correct timer
        ThreadLoop<0>::sptr_t _loop = ThreadLoop<0>::mk_sptr(appendToClassName<CameraProxy>("GrabLoop"));

        CallbackList::sptr_t _listener_lst = CallbackList::mk_sptr();

        std::atomic_int _cam_id{-1};
        std::atomic_bool _started{false};
        
        using autolock_t = std::lock_guard<std::recursive_mutex>;
        std::recursive_mutex _read_mut;
        std::recursive_mutex _cam_lst_mut;
    };
}
