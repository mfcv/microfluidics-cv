//////////////////////////////////////////////////////////////////////////////
/// \file       VideoFileWriter.cpp
/// \brief      Implementation of the video file output
/// \copyright  2019 - Simon Dallaire (AGPLv3)
//////////////////////////////////////////////////////////////////////////////
//  This program is free software: you can redistribute it and/or modify
//  it under the terms of the GNU Affero General Public License as
//  published by the Free Software Foundation, either version 3 of the
//  License, or (at your option) any later version.
//
//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU Affero General Public License for more details.
//
//  You should have received a copy of the GNU Affero General Public License
//  along with this program.  If not, see <https://www.gnu.org/licenses/>.
//////////////////////////////////////////////////////////////////////////////
#include"VideoFileWriter.hpp"
#include"../Log.hpp"
#include"../Utilities/Reflection.hpp"
#include<Macro/Enum.hpp>

using namespace mfcv;

VideoFileWriter::VideoFileWriter(const Codec codec,
                                 const std::string& fileName)
    : _codec(to_int(codec)),
     _fileName(fileName) {
}

VideoFileWriter::~VideoFileWriter() {
    _close();
}

void VideoFileWriter::write_async(ImageAdapter::csptr_t frame) {
    if(_isFinishing) {
        return;
    }

    _queue->emplace_back(frame);

    // TODO move in it own function...
    if(!_thread_loop->isRunning()) {
        const double waitDuration{1000/frame->getFps()};

        _thread_loop->start([this, waitDuration]() {
            ImageAdapter::csptr_t frame;
            if(this->_queue->pop_front(frame)) {
                RaiiTimer timer;
                this->_write(*frame);
                this->_fps = timer.GetElapsedTimeInFps();
            }
            else {
                mfcv::wait_ms(waitDuration);
            }
        });
        LOG(INFO) << "Recording to '" << _fileName << "'";
    }
    else {
        _meanFps.add(_fps);
        const double currentMean = _meanFps.mean();
        constexpr double diff{0.5};
        if(   _lastMeanFps < currentMean-diff
           || _lastMeanFps > currentMean+diff) {
            _lastMeanFps = currentMean;
            _meanFps.setMaxCount(to<size_t>(currentMean));
        }
    }
}

std::future<void> VideoFileWriter::close_async() {
    std::future<void> retVal;
    // TODO Select a good filename/path...
    if(!_isFinishing) {
        retVal = std::async([this]() {
            this->_close();
            this->_meanFps.reset();
            LOG(INFO) << "Finish recording to '" << this->_fileName << "'";
        });
    }
    return retVal;
}

void VideoFileWriter::_write(const ImageAdapter& frame, bool warn) {
    _open(frame.cgetBgrImg(), frame.getFps());
    
    RaiiTimer timer;
    _writer.write(frame.cgetBgrImg());
    const double fps = timer.GetElapsedTimeInFps();
    LOG_IF(WARNING,
              warn
           && fps < frame.getFps()
           && _queue->size() > 100
           && _queue->size()%10 == 0 )
        << "Writing " << static_cast<uint16_t>(std::nearbyint(frame.getFps()/fps))
        <<  " time slower than the camera.\r\n\t";
}

void VideoFileWriter::_open(const cv::Mat &cv_frame, double fps)  {
    if (_writer.isOpened()) {
        return;
	}

    const bool isColor = (cv_frame.type() == CV_8UC3);
    const auto& frameSize = cv_frame.size();
    if (!_writer.open(_fileName, _codec, fps, frameSize, isColor)) {
        LOG(ERROR) << mfcv::append("Could not open: ",_fileName,".");
        throw std::runtime_error("Could not open the output video file for write.");
    }
}

void VideoFileWriter::_close() {
    if(_isFinishing) {
        return;
    }
    _isFinishing = true;
    try {
        LOG(INFO) << "Recording last frames...";
        _thread_loop->stop();
        
        ImageAdapter::csptr_t frame;
        while(_queue->pop_front(frame)) {
            _write(*frame, false);
            LOG_IF(INFO, _queue->size()%10 == 0)
                << "Remaining "
                << _queue->size()
                << " frames...";
        }

        _queue->clear();
        _writer.release();
        LOG(TRACE) << "DONE";
    }
    MFCV_CATCH("Error while writing last frames")

    _isFinishing = false;
}

void VideoFileWriter::release() {
    if(_isFinishing) {
        return;
    }
    try {
        _isFinishing = true;
        _queue->clear();
        _thread_loop->stop();
        _writer.release();

    }
    MFCV_CATCH("Error while stoping.")
    _isFinishing = false;
}