//////////////////////////////////////////////////////////////////////////////
/// \file       ImageAdapter.hpp
/// \brief      Wrap a third party image to an opencv image
/// \copyright  2019 - Simon Dallaire (AGPLv3)
//////////////////////////////////////////////////////////////////////////////
//  This program is free software: you can redistribute it and/or modify
//  it under the terms of the GNU Affero General Public License as
//  published by the Free Software Foundation, either version 3 of the
//  License, or (at your option) any later version.
//
//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU Affero General Public License for more details.
//
//  You should have received a copy of the GNU Affero General Public License
//  along with this program.  If not, see <https://www.gnu.org/licenses/>.
//////////////////////////////////////////////////////////////////////////////
#pragma once
#include<opencv2/opencv.hpp>
#include<memory>
#include<Log.hpp>
#include<mutex>
#include<CopyMove.hpp>
#include<Ptr.hpp>
#include "../Utilities/MinMax.hpp"

namespace mfcv
{
    class ImageAdapter
    {
    public:
        MFCV_PTR_T(ImageAdapter)
        MFCV_DELETE_CP_MV(ImageAdapter)

        ImageAdapter() = default;
        ImageAdapter(cv::Mat bgrImg)
            : _mat(bgrImg)
        { }
        virtual ~ImageAdapter() = default;

        inline ImageAdapter::uptr_t clone() const {
            auto retVal = ImageAdapter::mk_uptr(_mat.clone());
            retVal->_fps = _fps;
            return retVal;
        }
        inline bool empty() const noexcept {
            return _mat.empty();
        }

        inline cv::Mat& getBgrImg() noexcept { return _mat; }
        inline const cv::Mat& cgetBgrImg() const noexcept { return _mat; }
        inline cv::Mat toRgbImg() const {
            cv::Mat ret_val;
            try {
                if(!_mat.empty() ) {
                    cv::cvtColor(_mat, ret_val, cv::COLOR_BGR2RGB);
                }
            }
            MFCV_CATCH("Could not convert the BGR image to RGB.")
            return ret_val;
        }

        inline double getFps() const noexcept {  return _fps; }
        inline void setFps(const double fps) noexcept {  _fps = fps; }

        inline void setStillFps() noexcept {  _fps = MIN<double>; }
        inline bool isStill() const noexcept { return _fps == MIN<double>; }
    private:
        double _fps{0};

        cv::Mat _mat;
    };
}