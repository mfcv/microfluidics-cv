//////////////////////////////////////////////////////////////////////////////
/// \file       Log.hpp
/// \brief      Declaration of loggin values.
/// \copyright  2019 - Simon Dallaire (AGPLv3)
/// \details
///     The available loggin values are g3log default levels plus custom
///     levels. Available log levels: DEBUG, INFO, WARNING, ERROR,
///     CRITICAL and FATAL. If \code{.cpp} LOG(FATAL) << "Message"; \endcode
///     is called, the programm will terminate immediatly.
//////////////////////////////////////////////////////////////////////////////
//  This program is free software: you can redistribute it and/or modify
//  it under the terms of the GNU Affero General Public License as
//  published by the Free Software Foundation, either version 3 of the
//  License, or (at your option) any later version.
//
//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU Affero General Public License for more details.
//
//  You should have received a copy of the GNU Affero General Public License
//  along with this program.  If not, see <https://www.gnu.org/licenses/>.
//////////////////////////////////////////////////////////////////////////////
#pragma once
#include <iostream>
#include <g3log/g3log.hpp>

// TODO Add localized logs for user ex: USR_INFO
const LEVELS TRACE {DEBUG.value - 1, {"TRACE"}};
const LEVELS ERROR {WARNING.value + 1, {"ERROR"}};
const LEVELS CRITICAL {ERROR.value + 1, {"CRITICAL"}};
const std::vector<const LEVELS*> AvailableLevels{
    &TRACE, &DEBUG, &INFO, &WARNING, &ERROR, &CRITICAL, &FATAL};

namespace mfcv
{
    // TODO check https://github.com/fmtlib/fmt
    inline std::string append(std::string&& ret_str) {
        return std::move(ret_str);
    }
    template<typename T, typename... Args>
    std::string append(std::string&& ret_str, T&& arg1, Args&&... other_args) {
        ret_str += std::forward<T>(arg1);
        return mfcv::append(std::move(ret_str), std::forward<Args>(other_args)...);
    }
    template<typename T,
             typename... Args,
             std::enable_if_t<std::is_same_v<T, std::string> == false>
             >
    std::string append(std::string&& ret_str, T&& arg1, Args&&... other_args) {
        ret_str += std::to_string(std::forward<T>(arg1));
        return mfcv::append(std::move(ret_str), std::forward<Args>(other_args)...);
    }
    template<typename T, typename... Args>
    std::string append(const std::string& tmp_str, T&& arg1, Args&&... other_args) {
        std::string ret_str{tmp_str}; // create a copy of the string
        return mfcv::append(std::move(ret_str), std::forward<T>(arg1), std::forward<Args>(other_args)...);
    }
}

/// Catch std::exception and ... then log MSG with the exception error message.
#define MFCV_CATCH(...)\
catch(const std::exception& e)\
{\
    LOG(ERROR) << mfcv::append(__VA_ARGS__," - ",e.what());\
}\
catch(...)\
{\
    LOG(ERROR) << mfcv::append(__VA_ARGS__," - Unexepected error");\
}

