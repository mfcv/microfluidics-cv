//////////////////////////////////////////////////////////////////////////////
/// \file       Mean.hpp
/// \brief      Define MinMax struct with usefull utilities
/// \copyright  2020 - Simon Dallaire (AGPLv3)
//////////////////////////////////////////////////////////////////////////////
//  This program is free software: you can redistribute it and/or modify
//  it under the terms of the GNU Affero General Public License as
//  published by the Free Software Foundation, either version 3 of the
//  License, or (at your option) any later version.
//
//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU Affero General Public License for more details.
//
//  You should have received a copy of the GNU Affero General Public License
//  along with this program.  If not, see <https://www.gnu.org/licenses/>.
//////////////////////////////////////////////////////////////////////////////
#pragma once
#include"../Ptr.hpp"
#include"MinMax.hpp"
#include"Timer.hpp"

namespace mfcv {
    template<typename T>
    class Mean final {
        public:
            MFCV_PTR_T(Mean<T>)
            MFCV_DEFAULT_COPY_MOVE(Mean)

            Mean() = default;
            inline Mean(const size_t pMaxCount)
                : maxCount(pMaxCount),
                  index(pMaxCount)
            {
                values.reserve(pMaxCount);
            }
            ~Mean() = default;
            
            template<typename SRC>
            void add(SRC &&pVal) {
                if(values.size() < maxCount ) {
                    values.emplace_back(std::forward<SRC>(pVal));
                }
                else {
                    ++index;
                    if(index >= maxCount) {
                        index = 0;
                    }
                    values[index] = to<T>(pVal);
                }
                if(!timer.Counting()) {
                    timer.Start();
                }
            }

            inline void reset() { 
                values.clear();
                index = maxCount;
                lastMean = to<T>(0);
                timer.Stop();
            }

            void setMaxCount(const size_t pMaxCount) {
                if(   pMaxCount == maxCount
                   || pMaxCount == 0) {
                    return;
                }

                maxCount = pMaxCount;
                index = pMaxCount;
                if (pMaxCount < values.size()) {
                    values.resize(pMaxCount);
                }
                else {
                    values.reserve(pMaxCount);
                }
            }

            inline T computeMean() {
                if(values.size() == 0) {
                    return 0;
                }

                double sum{0.0};
                for(const T val : values) {
                    sum += to<double>(val);
                }
                const double mean = sum/to<double>(values.size());
                return to<T>(mean);
            }

            inline T mean(const double refreshRateMs = 1000.0) {
                const auto lastMs = timer.GetElapsedTimeInMs();
                if(refreshRateMs <= lastMs) {
                    lastMean = computeMean();
                    timer.Start();
                }
                return lastMean;
            }

            inline size_t getMaxCount() { return maxCount; }
        private:
            size_t maxCount{2};
            size_t index{maxCount};
            std::vector<T> values;
            T lastMean{0};
            Timer timer;
    };
}