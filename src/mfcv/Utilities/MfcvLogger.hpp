//////////////////////////////////////////////////////////////////////////////
/// \file       MfcvLogger.hpp
/// \brief      Declaration of loggin functionnality.
/// \copyright  2019 - Simon Dallaire (AGPLv3)
//////////////////////////////////////////////////////////////////////////////
//  This program is free software: you can redistribute it and/or modify
//  it under the terms of the GNU Affero General Public License as
//  published by the Free Software Foundation, either version 3 of the
//  License, or (at your option) any later version.
//
//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU Affero General Public License for more details.
//
//  You should have received a copy of the GNU Affero General Public License
//  along with this program.  If not, see <https://www.gnu.org/licenses/>.
//////////////////////////////////////////////////////////////////////////////
#pragma once
#include <memory>
#include "Log.hpp"
#include <g3log/logworker.hpp>
#include "Ptr.hpp"

namespace mfcv {
    class MfcvLogger
    {
    private:
        MfcvLogger();
        MFCV_DELETE_CP_MV(MfcvLogger)

        std::unique_ptr<g3::LogWorker> logworker;
        static constexpr const char _env_verbose[]{"MFCV_VERBOSE"};
        static constexpr const char _env_verbose_filter[]{"MFCV_VERBOSE_FILTER"};
    public:
        /// \brief Get the instance of the logger
        inline static MfcvLogger& Instance() {
            static MfcvLogger instance;
            return instance;
        }
        
        /// \brief Add a sink handler
        template<typename T, typename Callback>
        std::unique_ptr<g3::SinkHandle<T>> addSink(std::unique_ptr<T>&& real_sink, Callback&& callback) {
            return logworker->addSink(std::move(real_sink), std::forward<Callback>(callback));
        }

        static int getVerboseLevelFromEnv() noexcept;
        static std::string getVerboseFilterFromEnv() noexcept;
    };
}