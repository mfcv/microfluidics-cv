//////////////////////////////////////////////////////////////////////////////
/// \file       Timer.cpp
/// \brief      Implementation of the timer class
/// \copyright  2019 - Simon Dallaire (AGPLv3)
//////////////////////////////////////////////////////////////////////////////
//  This program is free software: you can redistribute it and/or modify
//  it under the terms of the GNU Affero General Public License as
//  published by the Free Software Foundation, either version 3 of the
//  License, or (at your option) any later version.
//
//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU Affero General Public License for more details.
//
//  You should have received a copy of the GNU Affero General Public License
//  along with this program.  If not, see <https://www.gnu.org/licenses/>.
//////////////////////////////////////////////////////////////////////////////
#include "Timer.hpp"

using namespace mfcv;

void Timer::Start() noexcept {
    startedTime = std::chrono::steady_clock::now();
    isCounting = true;
}

void Timer::Stop() noexcept {
    isCounting = false;
}

double Timer::GetElapsedTimeInMs() {
    if(isCounting) {
        std::chrono::duration<double, std::milli>
                returnValue = std::chrono::steady_clock::now() - startedTime;
        return returnValue.count();
    }
    return 0.0;
}

double Timer::GetElapsedTimeInFps() {
    if(isCounting) {
        return 1000.0/GetElapsedTimeInMs();
    }
    else {
        return 0.0;
    }
}

int64_t Timer::GetRemainingTimeMs(uint64_t maxTimeMs){
    if(isCounting) {
        std::chrono::duration<double, std::milli>
                elapsedDuration = std::chrono::steady_clock::now() - startedTime;

        int64_t elapsedTime = elapsedDuration.count();
        return maxTimeMs - elapsedTime;
    }
    return 0;
}
