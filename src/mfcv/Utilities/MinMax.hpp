//////////////////////////////////////////////////////////////////////////////
/// \file       MinMax.hpp
/// \brief      Define MinMax struct with usefull utilities
/// \copyright  2020 - Simon Dallaire (AGPLv3)
//////////////////////////////////////////////////////////////////////////////
//  This program is free software: you can redistribute it and/or modify
//  it under the terms of the GNU Affero General Public License as
//  published by the Free Software Foundation, either version 3 of the
//  License, or (at your option) any later version.
//
//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU Affero General Public License for more details.
//
//  You should have received a copy of the GNU Affero General Public License
//  along with this program.  If not, see <https://www.gnu.org/licenses/>.
//////////////////////////////////////////////////////////////////////////////
#pragma once
#include"../CopyMove.hpp"
#include"../Ptr.hpp"
#include<type_traits>
#include<cmath>

// TODO Add unit tests for all functions of this header
namespace mfcv {
    /// Return the lowest finite number.
    template<typename T>
    constexpr T MIN_FINITE = std::numeric_limits<T>::lowest();

    /// Return the higest finite number.
    template<typename T>
    constexpr T MAX_FINITE = std::numeric_limits<T>::max();

    /// Minus infinity if available or the lowest finite number.
    template<typename T>
    constexpr T MIN =  std::numeric_limits<T>::has_infinity
                    ? -std::numeric_limits<T>::infinity()
                    :  std::numeric_limits<T>::lowest();
    
    /// Infinity if available or the higest finite number.
    template<typename T>
    constexpr T MAX = std::numeric_limits<T>::has_infinity
                    ? std::numeric_limits<T>::infinity()
                    : std::numeric_limits<T>::max();

    template<typename T,
             typename = std::enable_if_t< std::is_arithmetic_v<T> >>
    constexpr T to(const T pVal) { return pVal; }

    template<typename DST, typename SRC,
             typename = std::enable_if_t<  std::is_arithmetic_v<DST>
                                       &&  std::is_arithmetic_v<SRC>
                                       && !std::is_same_v<std::remove_cv<DST>,std::remove_cv<SRC>> >
    >
    constexpr DST to(const SRC pVal) {
        constexpr long double MAX_DST = MAX_FINITE<DST>;
        constexpr long double MAX_SRC = MAX_FINITE<SRC>;
        constexpr long double MIN_DST = MIN_FINITE<DST>;
        constexpr long double MIN_SRC = MIN_FINITE<SRC>;

        constexpr bool hasSmallerMax = MAX_DST < MAX_SRC;
        constexpr bool hasBiggerMin  = MIN_DST > MIN_SRC;

        if (hasSmallerMax && pVal > (SRC)MAX_FINITE<DST>) { return MAX<DST>; }
        if (hasBiggerMin  && pVal < (SRC)MIN_FINITE<DST>) { return MIN<DST>; }

        constexpr bool mustRound = std::is_floating_point_v<SRC> && std::is_integral_v<DST>;
        return mustRound
             ? (DST)std::round(pVal)
             : (DST)pVal;
    }

    template<typename T,
             typename = std::enable_if_t<std::is_arithmetic_v<T>>>
    class MinMax final {
        public:
            MFCV_DEFAULT_COPY_MOVE(MinMax)
            MFCV_PTR_T(MinMax)

            constexpr MinMax() = default;
            ~MinMax() noexcept = default;

            template<typename SRC1, typename SRC2>
            constexpr MinMax(const SRC1 pMin, const SRC2 pMax)
                : aMin(to<T>(pMin)), aMax(to<T>(pMax))
            { }

            constexpr T getMin() const noexcept { return aMin; }
            constexpr T getMax() const noexcept { return aMax; }

            template<typename DST>
            constexpr DST getMinTo() const noexcept { return to<DST>(aMin); }
            template<typename DST>
            constexpr DST getMaxTo() const noexcept { return to<DST>(aMax); }
            template<typename DST>
            constexpr MinMax<DST> cloneTo() { return MinMax<DST>(getMinTo<DST>(),getMaxTo<DST>()); }

            template<typename SRC>
            void setMin(const SRC pVal) noexcept { aMin = to<T>(pVal); }
            template<typename SRC>
            void setMax(const SRC pVal) noexcept { aMax = to<T>(pVal); }
        private:
            T aMin{MIN<T>};
            T aMax{MAX<T>};
    };

    template<typename T,
             typename = std::enable_if_t<std::is_arithmetic_v<T>>>
    class MinMaxLimited final {
        public:
        MFCV_DEFAULT_COPY_MOVE(MinMaxLimited)
        MFCV_PTR_T(MinMaxLimited)

        constexpr MinMaxLimited() {}
        ~MinMaxLimited() noexcept = default;

        constexpr MinMaxLimited(const MinMax<T> pValue, const MinMax<T> pAbsLimits = MinMax<T>())
            : aVal(pValue), aLimits(pAbsLimits)
        { }
        constexpr T getMin() const noexcept { return aVal.getMin(); }
        constexpr T getMax() const noexcept { return aVal.getMax(); }
        constexpr T& getAbsLimit() const noexcept { return aLimits; }

        template<typename DST>
        constexpr DST getMinTo() const noexcept { return to<DST>(aVal.getMin()); }
        template<typename DST>
        constexpr DST getMaxTo() const noexcept { return to<DST>(aVal.getMax()); }

        template<typename SRC>
        void setMin(const SRC pMin) noexcept { aVal.setMin(pMin); }
        template<typename SRC>
        void setMax(const SRC pMax) noexcept { aVal.setMax(pMax); }
        template<typename SRC>
        void setMinInLimits(const SRC pMin) noexcept { 
            setMin(pMin);
            if(getMin() < aLimits.getMin()) { setMin(aLimits.getMin()); }
            else if(getMin() > aLimits.getMax()) { setMin(aLimits.getMax()); }
        }
        template<typename SRC>
        void setMaxInLimits(const SRC pMax) noexcept {
            setMax(pMax);
            if(getMax() > aLimits.getMax()) { setMax(aLimits.getMax()); }
            else if(getMax() < aLimits.getMin()) { setMax(aLimits.getMin()); }
        }
        void setLimits(const MinMax<T> pLimits) noexcept { aLimits = pLimits; }
        void setMinMax(const MinMax<T>& pVal) noexcept { aVal = pVal; }
        template<typename SRC1, typename SRC2>
        void setMinMaxInputSafe(const SRC1 pV1, const SRC2 pV2) noexcept { 
            if(pV1 <= pV2) { setMinInLimits(pV1); setMaxInLimits(pV2); }
            else           { setMinInLimits(pV2); setMaxInLimits(pV1); }
        }
        template<typename SRC1, typename SRC2>
        void setMinMaxKeepRange(const SRC1 pV1, const SRC2 pV2) noexcept { 
            setMinMaxInputSafe(getMin(),getMax()); // Ensure the curent min is the min
            const T range = (getMax()-getMin())/2;
            const T absMin = aLimits.getMin();
            const T absMax = aLimits.getMax();

            const T absMiddle = (absMax-absMin)/2;
            if(range >= absMiddle) {
                setMin(absMin);
                setMax(absMax);
            }
            else {
                setMinMaxInputSafe(pV1,pV2); // Ensure the input min is the min
                const T middle = ((getMax()-getMin())/2) + getMin();
                setFromRange(middle, range);
            }
        }
        template<typename SRC>
        void setMinMaxFromPercentage(const SRC pPercentage) noexcept { 
            if(pPercentage >= to<SRC>(100)) {
                setMin(aLimits.getMin());
                setMax(aLimits.getMax());
                return;
            }

            setMinMaxInputSafe(getMin(),getMax()); // Ensure the curent min is the min
            const T middle = ((getMax()-getMin())/2) + getMin();
            if (pPercentage <= to<SRC>(0)) {
                setMin(middle);
                setMax(middle);
            }
            else {
                const long double coefficient = to<long double>(aLimits.getMax()-aLimits.getMin())/to<long double>(100.0);
                const long double range = (to<long double>(pPercentage)*coefficient)/to<long double>(2);
                setFromRange(middle, range);
            }
        }
        private:
            MinMax<T> aVal;
            MinMax<T> aLimits;

            template<typename RG>
            void setFromRange(const T pMiddle, const RG pRange) noexcept {
                const T absMin = aLimits.getMin();
                const T absMax = aLimits.getMax();
                const T range  = to<T>(pRange);

                const bool hasUnderflow = pMiddle < (absMin + range);
                const bool hasOverflow =  pMiddle > (absMax - range);
                if(hasUnderflow) {
                    setMin(absMin);
                    setMax(absMin + (2*range));
                }
                else if (hasOverflow) {
                    setMin(absMax - (2*range));
                    setMax(absMax);
                }
                else {
                    setMin(pMiddle - range);
                    setMax(pMiddle + range);
                }
            }
    };

    template<typename T, 
             typename = std::enable_if<std::is_arithmetic_v<T>>>
    class MinMaxVal final {
        public:
            MFCV_DEFAULT_COPY_MOVE(MinMaxVal)
            MFCV_PTR_T(MinMaxVal)

            constexpr MinMaxVal() = default;
            ~MinMaxVal() noexcept = default;
            
            template<typename SRC1, typename SRC2>
            constexpr MinMaxVal(const SRC1 pMin, const SRC2 pMax)
                : aRange(MinMax<T>(pMin, pMax))
            { }
            template<typename SRC1, typename SRC2, typename SRC3>
            constexpr MinMaxVal(const SRC1 pMin, const SRC2 pMax, const SRC3 pVal)
                : aRange(MinMax<T>(pMin, pMax)), aVal(to<T>(pVal))
            { }
                        constexpr T getMin() const noexcept { return aRange.getMin(); }
            constexpr T getMax() const noexcept { return aRange.getMax(); }
            constexpr T getVal() const noexcept { return aVal; }

            template<typename DST>
            constexpr DST getMinTo() const noexcept { return to<DST>(getMin()); }
            template<typename DST>
            constexpr DST getMaxTo() const noexcept { return to<DST>(getMax()); }
            template<typename DST>
            constexpr DST getValTo() const noexcept { return to<DST>(getVal()); }

            template<typename SRC>
            void setMin(const SRC pMin) noexcept { aRange.setMin(pMin); }
            template<typename SRC>
            void setMax(const SRC pMax) noexcept { aRange.setMax(pMax); }
            template<typename SRC1, typename SRC2>
            void setMinMax(const SRC1 pMin, const SRC2 pMax) noexcept { setMin(pMin); setMax(pMax); }

            const MinMax<T>& getMinMax() const noexcept { return aRange; }
            void setMinMax(const MinMax<T>& pVal) noexcept { aRange = pVal; }
            template<typename SRC>
            void setVal(const SRC pVal) noexcept { aVal = to<T>(pVal); }

            template<typename SRC>
            void setValConformRange(const SRC pVal) noexcept { 
                setVal(pVal);
                if     (getVal() < getMin()) { setMin(getVal()); }
                else if(getVal() > getMax()) { setMax(getVal()); }
            }
            template<typename SRC>
            void setValWithinRange(const SRC pVal) noexcept { 
                setVal(pVal);
                if     (getVal() < getMin()) { setVal(getMin()); }
                else if(getVal() > getMax()) { setVal(getMax()); }
            }
        private:
            T aVal{0};
            MinMax<T> aRange;
    };
}