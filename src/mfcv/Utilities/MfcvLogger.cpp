//////////////////////////////////////////////////////////////////////////////
/// \file       MfcvLogger.h
/// \brief      Implementation of loggin functionnality.
/// \copyright  2019 - Simon Dallaire (AGPLv3)
//////////////////////////////////////////////////////////////////////////////
//  This program is free software: you can redistribute it and/or modify
//  it under the terms of the GNU Affero General Public License as
//  published by the Free Software Foundation, either version 3 of the
//  License, or (at your option) any later version.
//
//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU Affero General Public License for more details.
//
//  You should have received a copy of the GNU Affero General Public License
//  along with this program.  If not, see <https://www.gnu.org/licenses/>.
//////////////////////////////////////////////////////////////////////////////
#include"MfcvLogger.hpp"

#include <stdlib.h>
#include <iostream>
#include <g3sinks/LogRotate.h>

using namespace mfcv;
namespace
{
    struct ConsoleSink {
        int verbose_lvl{MfcvLogger::getVerboseLevelFromEnv()};
        std::string filter{MfcvLogger::getVerboseFilterFromEnv()};

        // Linux xterm color
        // http://stackoverflow.com/questions/2616906/how-do-i-output-coloured-text-to-a-linux-terminal
        enum class FG_Color : uint_least8_t {YELLOW = 33, RED = 31, GREEN=32, WHITE = 97};
        FG_Color GetColor(const LEVELS level) const 
        {
            FG_Color retColor{ FG_Color::WHITE };
            if (level.value == DEBUG.value) { retColor = FG_Color::GREEN; }
            else if (level.value == INFO.value) { retColor = FG_Color::WHITE; }
            else if (level.value == WARNING.value) { retColor = FG_Color::YELLOW; }
            else if (level.value == ERROR.value) { retColor = FG_Color::RED; }
            else if (level.value == CRITICAL.value) { retColor = FG_Color::RED; }
            else if (g3::internal::wasFatal(level)) { retColor = FG_Color::RED; }

            return retColor;
        }
        constexpr size_t toInt(const FG_Color color) {
            return static_cast<std::underlying_type<FG_Color>::type>(color);
        }

        void ReceiveLogMessage(g3::LogMessageMover logEntry)
        {
            LEVELS level = logEntry.get()._level;
            if(level.value < verbose_lvl)
            {
                return;
            }
            const std::string& entry_str = logEntry.get().toString();
            if(   !filter.empty()
               && entry_str.find(filter) == std::string::npos) {
                   return;
            }

            size_t color = toInt(GetColor(level));
            if (level.value > WARNING.value)
            {
                std::cerr << "\033[" << color << "m" << entry_str
                          << "\033[m" << std::endl;
            }
            else
            {
                std::cout << "\033[" << color << "m" << entry_str
                          << "\033[m" << std::endl;
            }
        }
    };
}

//FIXME Don't add the comsol sink if not run in the terminal.
MfcvLogger::MfcvLogger()
 : logworker( g3::LogWorker::createLogWorker() )
{
    addSink(std::make_unique<ConsoleSink>(), &ConsoleSink::ReceiveLogMessage);

    g3::initializeLogging(logworker.get());

    LOG(DEBUG) << "Initialization done";
}

int MfcvLogger::getVerboseLevelFromEnv() noexcept
{
    const int ret_default{WARNING.value};
    
    char* env_val = getenv(_env_verbose);
    if(env_val == nullptr || *env_val == 0) {
        return ret_default;
    }
    for(const LEVELS* lvl : AvailableLevels) {
        if(lvl->text.compare(env_val) == 0) {
            return lvl->value;
        }
    }
    return ret_default;
}

std::string MfcvLogger::getVerboseFilterFromEnv() noexcept
{
    char* env_val = getenv(_env_verbose_filter);
    if(env_val == nullptr || *env_val == 0)
    {    
        return std::string{};
    }
    return std::string{env_val};
}