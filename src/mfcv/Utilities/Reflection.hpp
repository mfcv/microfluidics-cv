//////////////////////////////////////////////////////////////////////////////
/// \file       Reflection.hpp
/// \brief      Declaration of utilities template to bring some reflection
///             to C++.
/// \copyright  2019 - Simon Dallaire (AGPLv3)
//////////////////////////////////////////////////////////////////////////////
//  This program is free software: you can redistribute it and/or modify
//  it under the terms of the GNU Affero General Public License as
//  published by the Free Software Foundation, either version 3 of the
//  License, or (at your option) any later version.
//
//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU Affero General Public License for more details.
//
//  You should have received a copy of the GNU Affero General Public License
//  along with this program.  If not, see <https://www.gnu.org/licenses/>.
//////////////////////////////////////////////////////////////////////////////
#pragma once
#include <boost/type_index.hpp>
#include <string>
#include <Log.hpp>

#define MFCV_EXCEPTION(msg) ReflectiveException(__FILE__, __func__, __LINE__, msg)

namespace mfcv
{
    template<typename T>
    std::string getTypeName()
    {
            return boost::typeindex::type_id_with_cvr<T>().pretty_name();
    }

    template<typename T>
    std::string getClassName()
    {
            const std::string full_name = getTypeName<T>();
        const size_t last_colon = full_name.find_last_of(":");
        if(last_colon == std::string::npos)
        {
            LOG(ERROR) << mfcv::append("Not a class: ",full_name);
            return full_name;
        }
        else
        {
            const std::string class_name = full_name.substr(last_colon+1);
            return class_name;
        }
    }

    template<typename T, typename... Args>
    std::string appendToClassName(Args&&... args)
    {
            return mfcv::append(getClassName<T>(), "::", std::forward<Args>(args)...);
    }

    template<typename T>
    std::string getTypeName(T&& param)
    {
            return boost::typeindex::type_id_with_cvr<decltype(param)>().pretty_name();
    }

    template<typename T>
    std::string getMemberName(const char func[])
    {
            return mfcv::append(getTypeName<T>(),"::",func);
    }
    
    template<typename T>
    std::string getErrorMessage(T&& e)
    {
            return mfcv::append("Error [",getTypeName(std::forward<T>(e)),"]: ",e.what());
    }

    // Example:
    // throw MFCV_EXCEPTION("Error message")
    class ReflectiveException: public std::runtime_error {
    public:
        template<typename Msg>
        ReflectiveException(const char *const filename, const char *const func_name, int file_line, Msg&& err_msg)
            :runtime_error(mfcv::append(
                           filename,"->",func_name,"[", std::to_string(file_line),"]: "
                           , std::forward<Msg>(err_msg)))
            { LOG(TRACE); }

        virtual ~ReflectiveException() { LOG(TRACE); }
    };

    inline std::string getErrorMessage(const ReflectiveException& e)
    {
            return mfcv::append("Error: ",e.what());
    }
}