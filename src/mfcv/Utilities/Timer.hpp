//////////////////////////////////////////////////////////////////////////////
/// \file       Timer.hpp
/// \brief      Declaration of a timer class
/// \copyright  2019 - Simon Dallaire (AGPLv3)
//////////////////////////////////////////////////////////////////////////////
//  This program is free software: you can redistribute it and/or modify
//  it under the terms of the GNU Affero General Public License as
//  published by the Free Software Foundation, either version 3 of the
//  License, or (at your option) any later version.
//
//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU Affero General Public License for more details.
//
//  You should have received a copy of the GNU Affero General Public License
//  along with this program.  If not, see <https://www.gnu.org/licenses/>.
//////////////////////////////////////////////////////////////////////////////
#pragma once

#include <chrono>
#include<Log.hpp>

namespace mfcv {
    class Timer {
    public:
        Timer() = default;
        virtual ~Timer() = default;
        void Start() noexcept;
        void Stop() noexcept;
        double GetElapsedTimeInMs();
        double GetElapsedTimeInFps();
        inline bool Counting() const noexcept { return isCounting; }

        int64_t GetRemainingTimeMs(uint64_t maxTimeMs);
    private:
        std::chrono::steady_clock::time_point startedTime;
        bool isCounting{false};
    };

    class RaiiTimer final: public Timer
    {
    public:
        inline RaiiTimer() {
            Timer::Start();
        }

        template<typename Callable, typename ...Args>
        static double Run(Callable&& callback, Args&&... args)
        {
            RaiiTimer timer;
            callback(args...);
            return timer.GetElapsedTimeInFps();
        }
    };    
}
