//////////////////////////////////////////////////////////////////////////////
/// \file       Ptr.hpp
/// \brief      Declaration of macro and shorten nomenclature for smart pointers.
/// \copyright  2020 - Simon Dallaire (AGPLv3)
/// \details
///     This file contains macro and functions to declare shorten
///     standards smart pointer names.
//////////////////////////////////////////////////////////////////////////////
//  This program is free software: you can redistribute it and/or modify
//  it under the terms of the GNU Affero General Public License as
//  published by the Free Software Foundation, either version 3 of the
//  License, or (at your option) any later version.
//
//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU Affero General Public License for more details.
//
//  You should have received a copy of the GNU Affero General Public License
//  along with this program.  If not, see <https://www.gnu.org/licenses/>.
//////////////////////////////////////////////////////////////////////////////
#pragma once
#include <memory>
#include"Log.hpp"
#include"CopyMove.hpp"

namespace mfcv {
    template<typename T>
    using csptr_t = std::shared_ptr<const T>;
    template<typename T>
    using sptr_t = std::shared_ptr<T>;
    template<typename T>
    using cwptr_t = std::weak_ptr<const T>;
    template<typename T>
    using wptr_t = std::weak_ptr<T>;
    template<typename T>
    using cuptr_t = std::unique_ptr<const T>;
    template<typename T>
    using uptr_t = std::unique_ptr<T>;

    template<typename T, typename ...Args>
    sptr_t<T> mk_sptr(Args&&... args) {
            return std::make_shared<T>(std::forward<Args>(args)...);
    }

    template<typename T, typename ...Args>
    uptr_t<T> mk_uptr(Args&&... args) {
            return std::make_unique<T>(std::forward<Args>(args)...);
    }

    template<typename T, typename ...Args>
    csptr_t<T> mk_csptr(Args&&... args) {
            return std::make_shared<const T>(std::forward<Args>(args)...);
    }

    template<typename T, typename ...Args>
    cuptr_t<T> mk_cuptr(Args&&... args) {
            return std::make_unique<const T>(std::forward<Args>(args)...);
    }
}

// TODO Maybe remove the syntax ClassName::ptr_t because it is not intuitive...
/// Define sptr_t and wptr_t.
/// as standard shared or weak ptr for the type TYPE
/// It is intended to be used inside a class or a struct.
#define MFCV_SPTR_T(...)\
using csptr_t = mfcv::csptr_t<__VA_ARGS__>;\
using sptr_t = mfcv::sptr_t<__VA_ARGS__>;\
using cwptr_t = mfcv::cwptr_t<__VA_ARGS__>;\
using wptr_t = mfcv::wptr_t<__VA_ARGS__>;\
template<typename ...Args>\
inline static sptr_t mk_sptr(Args&&... args)\
{ LOG(TRACE); return sptr_t(new __VA_ARGS__(std::forward<Args>(args)...)); }\
template<typename ...Args>\
inline static csptr_t mk_csptr(Args&&... args)\
{ LOG(TRACE); return csptr_t(new __VA_ARGS__(std::forward<Args>(args)...)); }

#define MFCV_UPTR_T(...)\
using cuptr_t = std::unique_ptr<const __VA_ARGS__>;\
using uptr_t = std::unique_ptr<__VA_ARGS__>;\
template<typename ...Args>\
inline static uptr_t mk_uptr(Args&&... args)\
{ LOG(TRACE); return uptr_t(new __VA_ARGS__(std::forward<Args>(args)...)); }\
template<typename ...Args>\
inline static cuptr_t mk_cuptr(Args&&... args)\
{ LOG(TRACE); return cuptr_t(new __VA_ARGS__(std::forward<Args>(args)...)); }

/// Define uptr_t, sptr_t and wptr_t.
/// as standard unique, shared or weak ptr for the type TYPE
/// It is intended to be used inside a class or a struct.
/// @see MFCV_SPTR_T
#define MFCV_PTR_T(...)\
MFCV_UPTR_T(__VA_ARGS__)\
MFCV_SPTR_T(__VA_ARGS__)

/// Define class wptr_t and sptr_t, delete the copy and move operator
/// and implement a perfect forwarding make_shared function.
/// The calling class must derive :
///     public std::enable_shared_from_this<CALLING_CLASS>
/// @see MFCV_SPTR_T
/// @see MFCV_DELETE_CP_MV
#define MFCV_SHARED_FROM_THIS(...)\
MFCV_SPTR_T(__VA_ARGS__)\
MFCV_DELETE_CP_MV(__VA_ARGS__)
