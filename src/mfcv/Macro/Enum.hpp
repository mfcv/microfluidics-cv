//////////////////////////////////////////////////////////////////////////////
/// \file       Enum.hpp
/// \brief      Define usefull macro for Enums
/// \copyright  2020 - Simon Dallaire (AGPLv3)
/// \details
///     This file contains macro for enum. All macro must start with MFCV_.
//////////////////////////////////////////////////////////////////////////////
//  This program is free software: you can redistribute it and/or modify
//  it under the terms of the GNU Affero General Public License as
//  published by the Free Software Foundation, either version 3 of the
//  License, or (at your option) any later version.
//
//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU Affero General Public License for more details.
//
//  You should have received a copy of the GNU Affero General Public License
//  along with this program.  If not, see <https://www.gnu.org/licenses/>.
//////////////////////////////////////////////////////////////////////////////
#pragma once
#include<type_traits>

namespace mfcv {
    template<typename T>
    constexpr std::underlying_type_t<T> to_int(T pVal) noexcept {
        return static_cast<std::underlying_type_t<T>>(pVal);
    }
}

// TODO May be usefull :
//     https://codereview.stackexchange.com/questions/14309/conversion-between-enum-and-string-in-c-class-header/14315
//     https://codereview.stackexchange.com/questions/43725/automatic-c-enum-to-string-mapping-macro?rq=1

/*
// Example:
enum class ROITransform { None, GrayThreshold, HSVThreshold };
constexpr ROITransform toROITransform(std::underlying_type_t<ROITransform> val) {
    switch(val){
        MFCV_CASE_FROM_INT(ROITransform, None);
        MFCV_CASE_FROM_INT(ROITransform, GrayThreshold);
        MFCV_CASE_FROM_INT(ROITransform, HSVThreshold);
    }
    return ROITransform::None;
}
*/
#define MFCV_CASE_FROM_INT(NAME, VAL)\
    case to_int(NAME::VAL)  : return NAME::VAL

/*
Example:
enum class ROITransform { None, GrayThreshold, HSVThreshold };
constexpr const char* toString(ROITransform val) {
    switch(val){
        MFCV_CASE_TO_STRING(ROITransform, None);
        MFCV_CASE_TO_STRING(ROITransform, GrayThreshold);
        MFCV_CASE_TO_STRING(ROITransform, HSVThreshold);
    }
    return toString(ROITransform::None);
}
*/
#define MFCV_CASE_TO_STRING(NAME, VAL)\
    case NAME::VAL  : return #VAL


/*
Example:
enum class ROITransform { None, GrayThreshold, HSVThreshold };
constexpr const char* fromString(const std::string_view& pString) {
    MFCV_IS_STRING(ROITransform, None);
    MFCV_IS_STRING(ROITransform, GrayThreshold);
    MFCV_IS_STRING(ROITransform, HSVThreshold);

    return ROITransform::None;
}
*/
#define MFCV_IS_STRING(NAME, VAL)\
    if(pString == std::string_view{#VAL}) { return NAME::VAL; }

