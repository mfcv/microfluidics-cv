//////////////////////////////////////////////////////////////////////////////
/// \file       CopyMove.hpp
/// \brief      Declaration of copy moves macros.
/// \copyright  2020 - Simon Dallaire (AGPLv3)
/// \details
///     This file contains macro and functions to declare shorten
///     standards smart pointer names.
//////////////////////////////////////////////////////////////////////////////
//  This program is free software: you can redistribute it and/or modify
//  it under the terms of the GNU Affero General Public License as
//  published by the Free Software Foundation, either version 3 of the
//  License, or (at your option) any later version.
//
//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU Affero General Public License for more details.
//
//  You should have received a copy of the GNU Affero General Public License
//  along with this program.  If not, see <https://www.gnu.org/licenses/>.
//////////////////////////////////////////////////////////////////////////////
#pragma once

/// Add the default copy constructor and default copy operator
#define MFCV_DEFAULT_COPY(...)\
__VA_ARGS__(const __VA_ARGS__&) = default;\
__VA_ARGS__& operator=(const __VA_ARGS__&) = default;

/// Add the default move constructor and default move operator
#define MFCV_DEFAULT_MOVE(...)\
__VA_ARGS__(__VA_ARGS__&&) noexcept = default;\
__VA_ARGS__& operator=(__VA_ARGS__&&) noexcept = default;

/// Add default copy and move operation
/// @see MFCV_DEFAULT_COPY
/// @see MFCV_DEFAULT_MOVE
#define MFCV_DEFAULT_COPY_MOVE(...)\
MFCV_DEFAULT_COPY(__VA_ARGS__)\
MFCV_DEFAULT_MOVE(__VA_ARGS__)

/// Delete the copy constructor and the copy assignment operator
#define MFCV_DELETE_COPY(...)\
__VA_ARGS__(const __VA_ARGS__&) = delete;\
__VA_ARGS__& operator=(const __VA_ARGS__&) = delete;

/// Delete the move constructor and the move assignment operator
#define MFCV_DELETE_MOVE(...)\
__VA_ARGS__(__VA_ARGS__&&) = delete;\
__VA_ARGS__& operator=(__VA_ARGS__&&) = delete;

/// Delete default copy and move operator
/// @see MFCV_DELETE_COPY
/// @see MFCV_DELETE_MOVE
#define MFCV_DELETE_CP_MV(...)\
MFCV_DELETE_COPY(__VA_ARGS__)\
MFCV_DELETE_MOVE(__VA_ARGS__)
