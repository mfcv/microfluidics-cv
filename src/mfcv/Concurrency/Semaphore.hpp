//////////////////////////////////////////////////////////////////////////////
/// \file       Semaphore.hpp
/// \brief      Declaration of a semaphore.
/// \copyright  2020 - Simon Dallaire (AGPLv3)
//////////////////////////////////////////////////////////////////////////////
//  This program is free software: you can redistribute it and/or modify
//  it under the terms of the GNU Affero General Public License as
//  published by the Free Software Foundation, either version 3 of the
//  License, or (at your option) any later version.
//
//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU Affero General Public License for more details.
//
//  You should have received a copy of the GNU Affero General Public License
//  along with this program.  If not, see <https://www.gnu.org/licenses/>.
//////////////////////////////////////////////////////////////////////////////
#pragma once
#include<mutex>
#include<atomic>

namespace mfcv {

    // https://en.cppreference.com/w/cpp/named_req/Mutex
    template<size_t MAX,
             typename MUTEX = std::recursive_mutex>
    class Semaphore final {
    public:
        Semaphore() = default;
        ~Semaphore() = default;
        
        void lock() { }
        bool try_lock() { return false; }
        void unlock() { }
    private:
        using LockGard = std::lock_guard<MUTEX>;
        MUTEX aMutex;
        std::atomic_size_t aMax{MAX};
    }
}