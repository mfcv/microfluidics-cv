//////////////////////////////////////////////////////////////////////////////
/// \file       ThreadLoop.hpp
/// \brief      Declaration of a looping thread worker.
/// \copyright  2019 - Simon Dallaire (AGPLv3)
//////////////////////////////////////////////////////////////////////////////
//  This program is free software: you can redistribute it and/or modify
//  it under the terms of the GNU Affero General Public License as
//  published by the Free Software Foundation, either version 3 of the
//  License, or (at your option) any later version.
//
//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU Affero General Public License for more details.
//
//  You should have received a copy of the GNU Affero General Public License
//  along with this program.  If not, see <https://www.gnu.org/licenses/>.
//////////////////////////////////////////////////////////////////////////////
#pragma once
#include<atomic>
#include<thread>
#include<Utilities/Reflection.hpp>
#include<Utilities/Timer.hpp>
#include<future>
#include <Ptr.hpp>

namespace mfcv
{
    template<typename Period, typename T>
    void wait(T pTime) {
        auto waitTime = std::chrono::duration<T, Period>(pTime);
        std::this_thread::sleep_for(waitTime);
    }

    template<typename T>
    void wait_ms(T pMs) {
        mfcv::wait<std::milli>(pMs);
    }

    /*
        Count how many threads loop are runnings and send a kill signal if required.
    */
    class ThreadTracker
    {
        public:
            static void killAll();
            static bool canRun();

            ThreadTracker();
            ~ThreadTracker();
    };

    template<uint64_t SleepMs>
    class ThreadLoop final :  public std::enable_shared_from_this<ThreadLoop<SleepMs>>
    {
    public:
        MFCV_SHARED_FROM_THIS(ThreadLoop<SleepMs>)
        ~ThreadLoop() { LOG(TRACE) << _name; stop(); }

        template<typename Callable, typename ...Args>
        void start(Callable&& callback, Args&&... args) {
            LOG(TRACE) << _name;
            if(!_loop) {
                _running = true;
                auto wthis = this->weak_from_this();
                // TODO: Make sure it runs on an other thread...
                _loop = std::make_unique<std::future<bool>>(
                    std::async([wthis, callback, args...]() -> bool {
                        if(auto sthis = wthis.lock()) {
                            sthis->_threadBody(callback, args...);
                        }
                        return true;
                    })
                );
            }
        }

        inline bool isRunning() noexcept { LOG(TRACE) << _name; return _running; }
        void stop() {
            LOG(TRACE) << _name;
            try {
                if(_running) {
                    _running = false;
                    if(_loop) {
                        _loop->get();
                        _loop.reset();
                    }
                }
            }
            MFCV_CATCH(_name, "Error while stoping thread")
        }

    private:
        ThreadLoop(const std::string& name) : _name(name) { LOG(TRACE) << _name; }

        std::atomic_bool _running{false};
        std::unique_ptr<std::future<bool>> _loop;
        Timer _timer;
        const std::string _name;

        template<typename Callable, typename ...Args>
        void _threadBody(Callable&& callback, Args&&... args) noexcept {
            ThreadTracker tracker;
            try {
                LOG(TRACE) << _name;
                while(_running) {
                    // TODO Create a function without timer to avoid 
                    //      Unnecessary comparaison
                    if(SleepMs != 0) {
                        _timer.Start();
                    }

                    callback(std::forward<Args>(args)...);

                    _running = _running && ThreadTracker::canRun();
                    if(SleepMs != 0 && _running) {
                        // TODO remove unnecessary convertion
                        int64_t rem_time = _timer.GetRemainingTimeMs(SleepMs);
                        wait_ms(rem_time);
                        _running = _running && ThreadTracker::canRun();
                    }
                }
            }
            MFCV_CATCH(_name, "Error while running thread")
        }
    };
}