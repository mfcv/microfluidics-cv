//////////////////////////////////////////////////////////////////////////////
/// \file       ConcurentQueue.hpp
/// \brief      Declaration of a concurrent queue for a safe multythreaded access.
/// \copyright  2019 - Simon Dallaire (AGPLv3)
//////////////////////////////////////////////////////////////////////////////
//  This program is free software: you can redistribute it and/or modify
//  it under the terms of the GNU Affero General Public License as
//  published by the Free Software Foundation, either version 3 of the
//  License, or (at your option) any later version.
//
//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU Affero General Public License for more details.
//
//  You should have received a copy of the GNU Affero General Public License
//  along with this program.  If not, see <https://www.gnu.org/licenses/>.
//////////////////////////////////////////////////////////////////////////////
#pragma once
#include <list>
#include <mutex>
#include <memory>
#include <Ptr.hpp>

namespace mfcv
{
    // TODO Add semaphore support and sync getters (with timouts to avoid interlock)
    template<size_t Max, typename T>
    class ConcurentQueue final
    {
    public:
        MFCV_PTR_T(ConcurentQueue<Max,T>)
        MFCV_DEFAULT_COPY_MOVE(ConcurentQueue<Max,T>)
        ~ConcurentQueue() { LOG(TRACE); }

        ////////////////////////////////////////
        // Read section
        inline bool pop_front(T& ret_val) noexcept {
                    if(!_buffer.empty())
            {
                std::lock_guard lock(_read_mut);
                ret_val = std::move(_buffer.front());
                _buffer.pop_front();
                return true;
            }
            return false;
        }

        inline bool empty() const noexcept {
                    // Safe
            return _buffer.empty();
        }

        inline size_t size() const noexcept {
                    return _buffer.size();
        }

        ////////////////////////////////////////
        // Write section
        inline void clear() {
                    std::lock_guard lock(_write_mut);
            _buffer.clear();
        }
        bool push_back(const T& item) {
                    if(Max == 0 || _buffer.size() <= Max) {
                std::lock_guard lock(_write_mut);
                _buffer.push_back(item);
                return true;
            }
            return false;
        }
        bool push_back(T&& item) {
                    if(Max == 0 || _buffer.size() <= Max) {
                std::lock_guard lock(_write_mut);
                _buffer.push_back(std::move(item));
                return true;
            }
            return false;
        }
        template<typename... _Args>
        bool emplace_back(_Args&&... __args) {
                    if(Max == 0 || _buffer.size() <= Max) {
                std::lock_guard lock(_write_mut);
                _buffer.emplace_back(std::forward<_Args>(__args)...);
                return true;
            }
            return false;
        }

    private:
        std::list<T> _buffer;
        std::recursive_mutex _read_mut;
        std::recursive_mutex _write_mut;

        ////////////////////////////////////////
        // Constructors
        ConcurentQueue() = default;
        ConcurentQueue(const std::list<T>& lst)
            :_buffer(lst)
        { LOG(TRACE); }
        ConcurentQueue(std::list<T>&& lst)
            :_buffer(std::move(lst))
        { LOG(TRACE); }
    };
}