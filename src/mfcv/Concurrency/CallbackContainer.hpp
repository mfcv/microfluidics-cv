//////////////////////////////////////////////////////////////////////////////
/// \file       CallbackContainer.hpp
/// \brief      Declaration of a concurrent queue for a safe multythreaded access.
/// \copyright  2020 - Simon Dallaire (AGPLv3)
//////////////////////////////////////////////////////////////////////////////
//  This program is free software: you can redistribute it and/or modify
//  it under the terms of the GNU Affero General Public License as
//  published by the Free Software Foundation, either version 3 of the
//  License, or (at your option) any later version.
//
//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU Affero General Public License for more details.
//
//  You should have received a copy of the GNU Affero General Public License
//  along with this program.  If not, see <https://www.gnu.org/licenses/>.
//////////////////////////////////////////////////////////////////////////////
#pragma once
#include"../CopyMove.hpp"
#include"../Ptr.hpp"
#include<mutex>

namespace mfcv
{
    template<typename CallbackRet, typename ...CallbackArgs>
    class CallbackContainer final {
        public:
            MFCV_PTR_T(CallbackContainer)
            MFCV_DEFAULT_COPY_MOVE(CallbackContainer)

            CallbackContainer() = default;
            ~CallbackContainer() = default;

            using callback_t = std::function<CallbackRet (CallbackArgs... args)>;
            using callback_sptr_t = std::shared_ptr<callback_t>;
            using callback_wptr_t = std::weak_ptr<callback_t>;
            void addListener(callback_wptr_t listener) {
                            if(listener.expired()) {
                    LOG(DEBUG) << "Empty pointer, will be ignored.";
                }
                else {
                    autolock_t lock{_listener_mut};
                    _listener_lst.emplace_back(listener);
                }
            }
            void notifyListeners(CallbackArgs... args) {
                if(_listener_lst.empty()) {
                    LOG(TRACE) << "Ignoring empty listener.";
                    return;
                }

                autolock_t lk_gard{_listener_mut};
                for (auto it = _listener_lst.begin(); it != _listener_lst.end(); ++it ) {
                    bool isValidCallback{false};
                    try {
                        if(auto callback_ptr = it->lock()) {
                            auto& callback = *callback_ptr;
                            callback(std::forward<CallbackArgs>(args)...);
                            isValidCallback = true;
                        }
                    }
                    // FIXME Add the callback name (use a macro to get the file and line). Do the same thing for all callbacks in the software (ex: the thread main function)
                    MFCV_CATCH("Error while running listener.")

                    if (!isValidCallback) {
                        LOG(TRACE) << "Removing invalid iterator";
                        it = _listener_lst.erase(it);
                    }
                }
            }
            bool empty() const noexcept { return _listener_lst.empty(); }
        private:
            std::list<callback_wptr_t> _listener_lst;
            using autolock_t = std::lock_guard<std::recursive_mutex>;
            std::recursive_mutex _listener_mut;
    };
}