//////////////////////////////////////////////////////////////////////////////
/// \file       ThreadLoop.cpp
/// \brief      Implementation of a looping thread worker.
/// \copyright  2019 - Simon Dallaire (AGPLv3)
//////////////////////////////////////////////////////////////////////////////
//  This program is free software: you can redistribute it and/or modify
//  it under the terms of the GNU Affero General Public License as
//  published by the Free Software Foundation, either version 3 of the
//  License, or (at your option) any later version.
//
//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU Affero General Public License for more details.
//
//  You should have received a copy of the GNU Affero General Public License
//  along with this program.  If not, see <https://www.gnu.org/licenses/>.
//////////////////////////////////////////////////////////////////////////////
#include"ThreadLoop.hpp"

using namespace mfcv;

namespace
{
    std::atomic_bool canRun{true};
    std::atomic_int32_t active_threads{0};
}

void ThreadTracker::killAll()
{
    ::canRun = false;

    while(::active_threads > 0) {
        mfcv::wait_ms<int>(1);
    }
}

bool ThreadTracker::canRun()
{
    return ::canRun;
}

ThreadTracker::ThreadTracker() {
    ++::active_threads;
}
ThreadTracker::~ThreadTracker() {
    --::active_threads;
}
