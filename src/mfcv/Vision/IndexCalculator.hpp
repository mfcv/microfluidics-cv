//////////////////////////////////////////////////////////////////////////////
/// \file       IndexCalculator.hpp
/// \brief      Define a class to compute the mixing index for a given image
/// \copyright  2020 - Simon Dallaire (AGPLv3)
//////////////////////////////////////////////////////////////////////////////
//  This program is free software: you can redistribute it and/or modify
//  it under the terms of the GNU Affero General Public License as
//  published by the Free Software Foundation, either version 3 of the
//  License, or (at your option) any later version.
//
//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU Affero General Public License for more details.
//
//  You should have received a copy of the GNU Affero General Public License
//  along with this program.  If not, see <https://www.gnu.org/licenses/>.
//////////////////////////////////////////////////////////////////////////////
#include"../CopyMove.hpp"
#include"../Ptr.hpp"
#include"../Utilities/MinMax.hpp"

#include<opencv2/opencv.hpp>
#include<limits>

namespace mfcv {
    struct HSVTreshold final {
        HSVTreshold() = default;
        ~HSVTreshold() = default;
        HSVTreshold(MinMax<uint8_t> H, MinMax<uint8_t> S, MinMax<uint8_t> V)
            : min(cv::Scalar(H.getMin(), S.getMin(), V.getMin())),
              max(cv::Scalar(H.getMax(), S.getMax(), V.getMax()))
        { }
        
        cv::Scalar min{MIN_FINITE<uint8_t>, MIN_FINITE<uint8_t>, MIN_FINITE<uint8_t>};
        cv::Scalar max{MAX_FINITE<uint8_t>, MAX_FINITE<uint8_t>, MAX_FINITE<uint8_t>};
    };
    
    class IndexCalculator final {
    public:
        MFCV_DEFAULT_COPY_MOVE(IndexCalculator)
        MFCV_PTR_T(IndexCalculator)

        IndexCalculator() = default;
        IndexCalculator(cv::Mat& bgr_roi,
                        const cv::Mat& gray_roi = cv::Mat(),
                        const cv::Mat& hsv_roi = cv::Mat());
        ~IndexCalculator() = default;

        double getLazyMeanGray();
        double getLazyStdDevGray();
        inline const cv::Mat& getRoiGray() const noexcept { return _gray_roi; }
        inline const cv::Mat& getRoiHsv() const noexcept { return _hsv_roi; }

        double computeAmi();
        double computeRmi(const double initialRMI);
        double computeTmiGray(const MinMax<uint8_t>& gray, const bool updateOriginalRoi = false);
        double computeTmiHSV(const HSVTreshold& hsv, const bool updateOriginalRoi = false);

        private:
            cv::Mat _original_roi;
            cv::Mat _gray_roi;
            cv::Mat _hsv_roi;
            static constexpr double INITIAL_VALUE = MIN<double>;
            double _mean_gray{INITIAL_VALUE};
            double _std_dev_gray{INITIAL_VALUE};

            cv::Mat& getLazyRoiGray();
            cv::Mat& getLazyRoiHsv();

            double computeTmi(cv::Mat& srcROI,  const cv::Scalar& min,
                              const cv::Scalar& max, const bool updateOriginalRoi);
    };
}