//////////////////////////////////////////////////////////////////////////////
/// \file       IndexCalculator.cpp
/// \brief      Implementation of the class that compute the mixing index
///             for a given image.
/// \copyright  2020 - Simon Dallaire (AGPLv3)
//////////////////////////////////////////////////////////////////////////////
//  This program is free software: you can redistribute it and/or modify
//  it under the terms of the GNU Affero General Public License as
//  published by the Free Software Foundation, either version 3 of the
//  License, or (at your option) any later version.
//
//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU Affero General Public License for more details.
//
//  You should have received a copy of the GNU Affero General Public License
//  along with this program.  If not, see <https://www.gnu.org/licenses/>.
//////////////////////////////////////////////////////////////////////////////
#include"IndexCalculator.hpp"
#include<Log.hpp>

using namespace mfcv;

// FIXME Add try/catch in every functions

IndexCalculator::IndexCalculator(cv::Mat& bgr_roi, const cv::Mat& gray_roi, const cv::Mat& hsv_roi)
    :_original_roi(bgr_roi),
     _gray_roi(gray_roi),
     _hsv_roi(hsv_roi)
{ }

cv::Mat& IndexCalculator::getLazyRoiGray() {
    if(_gray_roi.empty()) {
        cv::cvtColor(_original_roi, _gray_roi, cv::COLOR_BGR2GRAY);
    }
    return _gray_roi;
}

cv::Mat& IndexCalculator::getLazyRoiHsv() {
    if(_hsv_roi.empty()) {
        cv::cvtColor(_original_roi, _hsv_roi, cv::COLOR_BGR2HSV);
    }
    return _hsv_roi;
}

double IndexCalculator::getLazyMeanGray() {
    if(_mean_gray == INITIAL_VALUE) {
        cv::Scalar mean, stddev;
        cv::meanStdDev(getLazyRoiGray(), mean, stddev);
        _mean_gray = mean[0];
        _std_dev_gray = stddev[0];
    }
    return _mean_gray;
}

double IndexCalculator::getLazyStdDevGray() {
    if(_std_dev_gray == INITIAL_VALUE) {
        cv::Scalar mean, stddev;
        cv::meanStdDev(getLazyRoiGray(), mean, stddev);
        _mean_gray = mean[0];
        _std_dev_gray = stddev[0];
    }
    return _std_dev_gray;
}

///////////////////////////////////////////
/// Compute the Absolute Mixing Index (AMI) based on:
///     Hashmi, A., & Xu, J. (2014).
///     On the Quantification of Mixing in Microfluidics.
///     Journal of Laboratory Automation, 19(5), 488–491.
///     https://doi.org/10.1177/2211068214540156
///
/// Where:
///    - 0=Unmixed fluid
///    - 1=Fully mixed fluid
///////////////////////////////////////////
double IndexCalculator::computeAmi() {
    double AMI = getLazyStdDevGray()/getLazyMeanGray();
    AMI = 1.0-AMI;
    return AMI;
}

///////////////////////////////////////////
/// Compute the Relative Mixing Index (RMI) based on:
///     Hashmi, A., & Xu, J. (2014).
///     On the Quantification of Mixing in Microfluidics.
///     Journal of Laboratory Automation, 19(5), 488–491.
///     https://doi.org/10.1177/2211068214540156
///
/// Where:
///    - 0=Unmixed fluid
///    - 1=Fully mixed fluid
///////////////////////////////////////////
double IndexCalculator::computeRmi(const double initialRMI) {
    double RMI = getLazyStdDevGray()/initialRMI;
    RMI = 1.0-RMI;
    return RMI;
}

///////////////////////////////////////////
/// The threshold mixing index compute the amount
/// of pixel inside the selected threshold (GRAY).
/// Where:
///  - 0=All pixel where removed (should be the unmixed state)
///  - 1=All pixel where accepted
///////////////////////////////////////////
double IndexCalculator::computeTmiGray(const MinMax<uint8_t>& gray, const bool updateOriginalRoi) {
    const auto scalar = [](const uint8_t val) {
        return cv::Scalar(val);
    };
    return computeTmi(getLazyRoiGray(), scalar(gray.getMin()), scalar(gray.getMax()), updateOriginalRoi);
}

///////////////////////////////////////////
/// The threshold mixing index compute the amount
/// of pixel inside the selected threshold (HSV).
/// Where:
///  - 0=All pixel where removed (should be the unmixed state)
///  - 1=All pixel where accepted
///////////////////////////////////////////
double IndexCalculator::computeTmiHSV(const HSVTreshold& hsv, const bool updateOriginalRoi) {
    return computeTmi(getLazyRoiHsv(), hsv.min, hsv.max, updateOriginalRoi);;
}

///////////////////////////////////////////
/// The threshold mixing index compute the amount
/// of pixel inside the selected threshold.
/// Where:
///  - 0=All pixel where removed (should be the unmixed state)
///  - 1=All pixel where accepted
///////////////////////////////////////////
double IndexCalculator::computeTmi(cv::Mat& srcROI,  const cv::Scalar& min,
                                   const cv::Scalar& max, const bool updateOriginalRoi) {
    double TMI{0.0};
    try {
        cv::Mat threshold;
        cv::inRange(srcROI, min, max, threshold);
        TMI = cv::mean(threshold)[0]/255.0;

        if(updateOriginalRoi) {
            cv::cvtColor(threshold, _original_roi, cv::COLOR_GRAY2BGR);
        }
    }
    MFCV_CATCH("Cannot compute TMI.")

    return TMI;
}