//////////////////////////////////////////////////////////////////////////////
/// \file       main.cpp
/// \brief      Entry point of MicrofluidicsCV.
/// \copyright  2019 - Simon Dallaire (AGPLv3)
//////////////////////////////////////////////////////////////////////////////
//  This program is free software: you can redistribute it and/or modify
//  it under the terms of the GNU Affero General Public License as
//  published by the Free Software Foundation, either version 3 of the
//  License, or (at your option) any later version.
//
//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU Affero General Public License for more details.
//
//  You should have received a copy of the GNU Affero General Public License
//  along with this program.  If not, see <https://www.gnu.org/licenses/>.
//////////////////////////////////////////////////////////////////////////////

#include<Utilities/MfcvLogger.hpp>
namespace
{
    // MfcvLogger must be the first thing initialized and the last destroyed.
    const auto& gLogger = mfcv::MfcvLogger::Instance();
}

#include <QGuiApplication>
#include<QRegularExpression>
#include"gui/MainWindow.hpp"
#include"gui/FontProxy.hpp"
#include <QtGlobal>
#include <opencv2/core/ocl.hpp>

using namespace mfcv;
using namespace mfcv::gui;

namespace
{
    /// @brief Log handler for Qt
    void qtMessageHandler(QtMsgType qMsgType,
                          const QMessageLogContext& qMsgContext,
                          const QString& qMsgString)
    {
        //FIXME Remove the FileDialog warning instead of ignoring it
        static QRegularExpression ignoredMsg{QStringLiteral("Model size of -[0-9]+ is less than 0")};
        if(   qMsgString.isEmpty()
           || ignoredMsg.match(qMsgString).hasMatch()) {
           return;
        }

        LEVELS logLevel{ WARNING };
        switch (qMsgType) {
            case QtDebugMsg:
                logLevel = DEBUG;
                break;
            case QtInfoMsg:
                logLevel = INFO;
                break;
            case QtWarningMsg:
                logLevel = WARNING;
                break;
          //case QtSystemMsg:
            case QtCriticalMsg:
                logLevel = CRITICAL;
                break;
            case QtFatalMsg:
                logLevel = FATAL;
                break;
        }
        if(   qMsgContext.file && *qMsgContext.file
           && qMsgContext.function && *qMsgContext.function) {
               LOG(logLevel) << "(" << qMsgContext.file << "->"<< qMsgContext.function
                             << ":" << qMsgContext.line << ")" << qMsgString.toStdString();
        }
        else {
            LOG(logLevel) << qMsgString.toStdString();
        }
    }
}

/// @brief MicrofluidicsCV entry point.
int main(int argc, char *argv[]) {
    try {
    
        qInstallMessageHandler(::qtMessageHandler);
        QCoreApplication::setAttribute(Qt::AA_EnableHighDpiScaling);
        QGuiApplication app(argc, argv);
        app.setOrganizationName("MicrofluidicsCV");
        app.setOrganizationDomain("Microfluidics Research");
        FontProxy app_fonts(app);

        MainWindow mainWindow;
        mainWindow.load();

        return app.exec();
    }
    MFCV_CATCH("Fatal error: The program will terminate")
}
