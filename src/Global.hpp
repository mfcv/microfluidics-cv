//////////////////////////////////////////////////////////////////////////////
/// \file       Global.hpp
/// \brief      Global definition for the application
/// \copyright  2019 - Simon Dallaire (AGPLv3)
//////////////////////////////////////////////////////////////////////////////
//  This program is free software: you can redistribute it and/or modify
//  it under the terms of the GNU Affero General Public License as
//  published by the Free Software Foundation, either version 3 of the
//  License, or (at your option) any later version.
//
//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU Affero General Public License for more details.
//
//  You should have received a copy of the GNU Affero General Public License
//  along with this program.  If not, see <https://www.gnu.org/licenses/>.
//////////////////////////////////////////////////////////////////////////////
#pragma once
#include<type_traits>
#include<Utilities/MinMax.hpp>

// FIXME Update all macro with default value, ex: #define MFCV_PROP_GET(TYPE, NAME, DEFAULT_VALUE)
// FIXME Update all macro to use a lowercase first char for compatibility with QML's setters (Except for the statics ones)
// FIXME Remove all QStringLiteral and to make sure MACRO are usable for all types

//////////// Qt Utilities ////////////
#define MFCV_PROP_CONST(TYPE, NAME)\
    Q_PROPERTY(TYPE NAME READ get ## NAME NOTIFY NAME ## Changed)\
    public:\
        inline const TYPE& get ## NAME() const noexcept { return _##NAME; }\
        Q_SIGNAL void NAME ## Changed();\
    private:\
        const TYPE _##NAME;

#define MFCV_PROP_GET(TYPE, NAME)\
    Q_PROPERTY(TYPE NAME READ get ## NAME NOTIFY NAME ## Changed)\
    public:\
        inline const TYPE& get ## NAME() const noexcept { return _##NAME; }\
        Q_SIGNAL void NAME ## Changed();\
    private:\
        TYPE _##NAME;\
        inline void set ## NAME(const TYPE &val) { _##NAME= val; NAME ## Changed(); }

#define MFCV_PROP_GET_SET(TYPE, NAME)\
    Q_PROPERTY(TYPE NAME READ get ## NAME WRITE set ## NAME NOTIFY NAME ## Changed)\
    public:\
        inline void set ## NAME(const TYPE &val) { _##NAME= val; NAME ## Changed(); }\
        inline const TYPE& get ## NAME() const noexcept { return _##NAME; }\
        Q_SIGNAL void NAME ## Changed();\
    private:\
        TYPE _##NAME;

#define MFCV_PROP_NUMBER_GET(TYPE, NAME)\
    Q_PROPERTY(TYPE NAME READ get ## NAME NOTIFY NAME ## Changed)\
    public:\
        inline TYPE get ## NAME() const noexcept { return _##NAME; }\
        Q_SIGNAL void NAME ## Changed();\
    private:\
        TYPE _##NAME{0};\
        template<typename SRC>\
        inline void set ## NAME(const SRC pVal) noexcept { \
            const TYPE val = mfcv::to<TYPE>(pVal);\
            if(_##NAME!= val ) {\
                _##NAME= val;\
                NAME ## Changed();\
            }\
        }

#define MFCV_PROP_NUMBER_GET_SET(TYPE, NAME)\
    Q_PROPERTY(TYPE NAME READ get ## NAME WRITE set ## NAME NOTIFY NAME ## Changed)\
    public:\
        inline TYPE get ## NAME() const noexcept { return _##NAME; }\
        inline void set ## NAME(const TYPE val) { if(_##NAME!= val ) { _##NAME= val; NAME ## Changed(); } }\
        Q_SIGNAL void NAME ## Changed();\
    private:\
        TYPE _##NAME{0};\
        template<typename SRC>\
        inline void set ## NAME(const SRC pVal) noexcept { \
            const TYPE val = mfcv::to<TYPE>(pVal);\
            if(_##NAME!= val ) {\
                _##NAME= val;\
                NAME ## Changed();\
            }\
        }
#define MFCV_PROP_MIN_MAX(QTYPE, TYPE, NAME)\
    Q_PROPERTY(QTYPE NAME##Min READ get ## NAME##Min WRITE set ## NAME##Min NOTIFY NAME##Min ## Changed)\
    Q_PROPERTY(QTYPE NAME##Max READ get ## NAME##Max WRITE set ## NAME##Max NOTIFY NAME##Max ## Changed)\
    public:\
        inline QTYPE get ##NAME##Min () const noexcept { return _##NAME.getMinTo<QTYPE>(); }\
        inline QTYPE get ##NAME##Max () const noexcept { return _##NAME.getMaxTo<QTYPE>(); }\
        inline void set ## NAME##Min (const QTYPE pVal) {const TYPE val = mfcv::to<TYPE>(pVal); if(_##NAME.getMin() != val ) { _##NAME.setMin(val); NAME##Min ## Changed(); NAME##Changed(_##NAME.getMin(), _##NAME.getMax()); } } \
        inline void set ## NAME##Max (const QTYPE pVal) {const TYPE val = mfcv::to<TYPE>(pVal); if(_##NAME.getMax() != val ) { _##NAME.setMax(val); NAME##Max ## Changed(); NAME##Changed(_##NAME.getMin(), _##NAME.getMax()); } } \
        Q_SIGNAL void NAME##Min ## Changed();\
        Q_SIGNAL void NAME##Max ## Changed();\
        Q_SIGNAL void NAME ## Changed(QTYPE pMin, QTYPE pMax);\
        \
        Q_INVOKABLE inline void set ## NAME##Range (const QTYPE pVal1, const QTYPE pVal2)       { _##NAME.setMinMaxInputSafe(pVal1, pVal2); NAME##Min ## Changed(); NAME##Max ## Changed(); NAME##Changed(_##NAME.getMin(), _##NAME.getMax()); } \
        Q_INVOKABLE inline void set ## NAME##RangeFromPercentage (const double pVal)            { _##NAME.setMinMaxFromPercentage(pVal);    NAME##Min ## Changed(); NAME##Max ## Changed(); NAME##Changed(_##NAME.getMin(), _##NAME.getMax()); } \
        Q_INVOKABLE inline void set ## NAME##RangeLocked (const QTYPE pVal1, const QTYPE pVal2) { _##NAME.setMinMaxKeepRange(pVal1, pVal2); NAME##Min ## Changed(); NAME##Max ## Changed(); NAME##Changed(_##NAME.getMinTo<QTYPE>(), _##NAME.getMaxTo<QTYPE>()); } \
    private:\
        MinMaxLimited<TYPE> _##NAME;\
        template<typename SRC>\
        void set ## NAME##Min (SRC pVal) {TYPE val = mfcv::to<TYPE>(pVal); if(_##NAME.getMin() != val ) { _##NAME.setMin(val); NAME##Min ## Changed(); } } \
        template<typename SRC>\
        void set ## NAME##Max (SRC pVal) {TYPE val = mfcv::to<TYPE>(pVal); if(_##NAME.getMax() != val ) { _##NAME.setMax(val); NAME##Max ## Changed(); } }

#define MFCV_PROP_MIN_MAX_VAL(TYPE, NAME)\
        Q_PROPERTY(TYPE NAME##Min READ get ## NAME##Min WRITE set ## NAME##Min NOTIFY NAME##Min ## Changed)\
        Q_PROPERTY(TYPE NAME      READ get ## NAME      WRITE set ## NAME      NOTIFY NAME      ## Changed)\
        Q_PROPERTY(TYPE NAME##Max READ get ## NAME##Max WRITE set ## NAME##Max NOTIFY NAME##Max ## Changed)\
    public:\
        inline TYPE get ## NAME##Min () const noexcept { return _##NAME.getMin(); }\
        inline TYPE get ## NAME      () const noexcept { return _##NAME.getVal(); }\
        inline TYPE get ## NAME##Max () const noexcept { return _##NAME.getMax(); }\
        Q_SIGNAL void NAME##Min ## Changed();\
        Q_SIGNAL void NAME      ## Changed();\
        Q_SIGNAL void NAME##Max ## Changed();\
    private:\
        MinMaxVal<TYPE> _##NAME;\
        inline void set ## NAME##Min (const TYPE val) {if(_##NAME.getMin() != val ) { _##NAME.setMin(val); NAME##Min ## Changed(); } } \
        inline void set ## NAME      (const TYPE val) {if(_##NAME.getVal() != val ) { _##NAME.setVal(val); NAME      ## Changed(); } } \
        inline void set ## NAME##Max (const TYPE val) {if(_##NAME.getMax() != val ) { _##NAME.setMax(val); NAME##Max ## Changed(); } } \
        inline void all ## NAME ## Changed() { \
            NAME##Min ## Changed();\
            NAME      ## Changed();\
            NAME##Max ## Changed();\
        } \
        inline void set ## NAME ## _ConformRange(const TYPE val) { \
            _##NAME.setValConformRange(val);\
            all ## NAME ## Changed();\
        }

#define MFCV_PROP_STR_STATIC(NAME, VALUE)\
    Q_PROPERTY(QString NAME READ get ## NAME NOTIFY NAME ## Changed)\
    public:\
        static const QString& NAME() noexcept { \
            static const QString NAME = QStringLiteral(VALUE);\
            return NAME;\
        }\
        const QString& get ## NAME() const noexcept { return NAME(); }\
        Q_SIGNAL void NAME ## Changed();

#define MFCV_PROP_STR_GET(NAME, VALUE)\
    Q_PROPERTY(QString NAME READ get ## NAME NOTIFY NAME ## Changed)\
    public:\
        inline const QString& get ## NAME() const noexcept { return _##NAME; }\
        Q_SIGNAL void NAME ## Changed();\
    private:\
        QString _##NAME{VALUE};\
        inline void set ## NAME(const QString &val) { _##NAME= val; NAME ## Changed(); }

#define MFCV_PROP_STR_GET_SET(NAME, VALUE)\
Q_PROPERTY(QString NAME READ get ## NAME WRITE set ## NAME NOTIFY NAME ## Changed)\
    public:\
        inline void set ## NAME(const QString &val) { _##NAME= val; NAME ## Changed(); }\
        inline const QString& get ## NAME() const noexcept { return _##NAME; }\
        Q_SIGNAL void NAME ## Changed();\
    private:\
        QString _##NAME{VALUE};
