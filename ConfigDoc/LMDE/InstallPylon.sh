#!/bin/bash
#####################################################################
#  InstallDependencies.sh - This script install dependencies for LMDE 4 "Debbie"
#
#  Copyright (C) 2020  Simon Dallaire
#
#  This program is free software: you can redistribute it and/or modify
#  it under the terms of the GNU Affero General Public License as
#  published by the Free Software Foundation, either version 3 of the
#  License, or (at your option) any later version.
#
#  This program is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#  GNU Affero General Public License for more details.
#
#  You should have received a copy of the GNU Affero General Public License
#  along with this program.  If not, see <https://www.gnu.org/licenses/>.
#
#####################################################################

############## Install pylon from Basler ##############
if [ ! -f "./pylon_6.1.0.19674-deb0_amd64.deb" ]; then
    wget https://www.baslerweb.com/fp-1584619019/media/downloads/software/pylon_software/pylon_6.1.0.19674-deb0_amd64.deb
fi
sudo dpkg -i ./pylon_6.1.0.19674-deb0_amd64.deb

sudo ln -sf /opt/pylon/bin/* /usr/local/bin/
sudo ln -sf /opt/pylon/include/* /usr/local/include/
sudo ln -sf /opt/pylon/lib/* /usr/local/lib/
sudo ln -sf /opt/pylon/lib/*.so /usr/local/bin/
sudo ln -sf /opt/pylon/share/* /usr/local/share/
