#!/bin/bash
#####################################################################
#  InstallDependencies.sh - This script install dependencies for LMDE 4 "Debbie"
#
#  Copyright (C) 2020  Simon Dallaire
#
#  This program is free software: you can redistribute it and/or modify
#  it under the terms of the GNU Affero General Public License as
#  published by the Free Software Foundation, either version 3 of the
#  License, or (at your option) any later version.
#
#  This program is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#  GNU Affero General Public License for more details.
#
#  You should have received a copy of the GNU Affero General Public License
#  along with this program.  If not, see <https://www.gnu.org/licenses/>.
#
#####################################################################

############## Install Dependencies ##############
# Dev Tools
sudo apt-get install -y \
    wget dpkg ca-certificates gnupg libcurl4-openssl-dev \
    git git-gui ccache cmake cmake-extras ninja-build \
    g++-multilib libboost-all-dev build-essential

# Qt
sudo apt-get install -y \
    qtcreator qtdeclarative5-dev qttools5-dev qtmultimedia5-dev libqt5multimedia5-plugins \
    qml-module-qtquick2 qml-module-qtquick-window2 \
    qml-module-qtquick-dialogs qml-module-qt-labs-folderlistmodel qml-module-qt-labs-settings \
    qml-module-qtquick-layouts qml-module-qtquick-controls2 qml-module-qtmultimedia

# OpenCV codecs
sudo apt-get install -y \
    libdc1394-22 libdc1394-22-dev \
    ffmpeg libavcodec-dev libavformat-dev libavutil-dev libswscale-dev libavresample-dev \
    libgstreamer1.0-dev libgstreamer-plugins-base1.0-dev \
    libv4l-dev v4l-utils

# Ohers codecs
sudo apt-get install -y \
    libjpeg-dev libpng-dev libtiff-dev libxvidcore-dev \
    libx264-dev libx265-dev \
    libxine2-dev libvorbis-dev libmp3lame-dev

# OpenCV dependencies
sudo apt-get install -y \
    liblapack-dev libeigen3-dev doxygen libvtk7-dev libgtk-3-dev 

#    libatlas-base-dev gfortran libtbb-dev \
#    libopencore-amrnb-dev libopencore-amrwb-dev libtheora-dev  \
#    libogre-1.9-dev libopenblas-dev 

