#!/bin/bash
#####################################################################
#  InstallDependencies.sh - This script install dependencies for LMDE 4 "Debbie"
#
#  Copyright (C) 2020  Simon Dallaire
#
#  This program is free software: you can redistribute it and/or modify
#  it under the terms of the GNU Affero General Public License as
#  published by the Free Software Foundation, either version 3 of the
#  License, or (at your option) any later version.
#
#  This program is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#  GNU Affero General Public License for more details.
#
#  You should have received a copy of the GNU Affero General Public License
#  along with this program.  If not, see <https://www.gnu.org/licenses/>.
#
#####################################################################

INSTALL_DIR="/usr/local"
OCV_VERSION="4.3.0"
NPROC=$(( $(nproc --all) + 2 ))

############## Install OpenCV from source ##############
OCV_DIR="opencv"
CONTRIB_DIR="opencv_contrib"

# Fetching contrib
if [ ! -d "$CONTRIB_DIR" ]; then
    git clone https://github.com/opencv/opencv_contrib.git
fi
cd "$CONTRIB_DIR"
git fetch
git checkout $OCV_VERSION
cd ..

# Fetching OpenCV
if [ ! -d "$OCV_DIR" ]; then
    git clone https://github.com/opencv/opencv.git
fi
cd "$OCV_DIR"
git fetch
git checkout $OCV_VERSION

# Cleaning old build and installation
if [ -d "build" ]; then
    echo "Uninstalling old OpenCV version"
    sudo ninja uninstall
fi
rm -rf build

# Building and installing OpenCV
mkdir build
cd build
cmake .. -G "Ninja" \
     -DOPENCV_EXTRA_MODULES_PATH="../../$CONTRIB_DIR/modules" \
     -DENABLE_CXX11=ON \
     -DWITH_QT=ON \
     -DBUILD_DOCS=ON \
     -DBUILD_PERF_TESTS=OFF \
     -DBUILD_TESTS=OFF \
     -DWITH_OPENCL=ON \
     -DWITH_OPENGL=ON \
     -DWITH_TBB=ON \
     -DWITH_V4L=ON \
     -DCMAKE_BUILD_TYPE=RELEASE \
     -DCMAKE_INSTALL_PREFIX="${INSTALL_DIR}" &&
ninja -j$NPROC && sudo ninja install

