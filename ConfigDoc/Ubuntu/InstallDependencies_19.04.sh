#!/bin/bash
#####################################################################
#  InstallDependencies.sh - This script install dependencies for Ubuntu 18.04.1
#
#  Copyright (C) 2019  Simon Dallaire
#
#  This program is free software: you can redistribute it and/or modify
#  it under the terms of the GNU Affero General Public License as
#  published by the Free Software Foundation, either version 3 of the
#  License, or (at your option) any later version.
#
#  This program is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#  GNU Affero General Public License for more details.
#
#  You should have received a copy of the GNU Affero General Public License
#  along with this program.  If not, see <https://www.gnu.org/licenses/>.
#
#####################################################################

INSTALL_DIR="/usr/local"
OCV_VERSION="4.1.1"
NPROC=$(( $(nproc --all) + 2 ))

sudo apt-get install \
    wget dpkg ca-certificates gnupg libcurl4-openssl-dev \
    g++-multilib git ccache cmake cmake-extras libboost-all-dev \
    libdc1394-22-dev libavcodec-dev libavformat-dev libavutil-dev libswscale-dev libv4l-dev \
    libavresample-dev libgstreamer1.0-dev libgstreamer-plugins-base1.0-dev liblapack-dev liblapacke-dev libeigen3-dev \
    libjpeg-dev libpng-dev libtiff-dev libxvidcore-dev libx264-dev libx265-dev libxine2-dev \
    libatlas-base-dev gfortran libtbb-dev libmp3lame-dev \
    libopencore-amrnb-dev libopencore-amrwb-dev libtheora-dev libvorbis-dev v4l-utils \
    libvtk7-dev \
    libogre-1.9-dev libopenblas-dev doxygen \
    qtdeclarative5-dev qtcreator \
    qml-module-qtquick2 qml-module-qtquick-window2 \
    qml-module-qtquick-dialogs qml-module-qt-labs-folderlistmodel qml-module-qt-labs-settings \
    qml-module-qtquick-layouts qml-module-qtquick-controls2

git clone https://github.com/opencv/opencv_contrib.git
cd opencv_contrib
git fetch
git checkout $OCV_VERSION
cd ..
git clone https://github.com/opencv/opencv.git
cd opencv
git fetch
git checkout $OCV_VERSION
rm -rf build
mkdir build
cd build
cmake .. \
     -DOPENCV_EXTRA_MODULES_PATH="../../opencv_contrib/modules" \
     -DWITH_QT=ON \
     -DWITH_OPENCL=ON \
     -DWITH_OPENGL=ON \
     -DWITH_TBB=ON \
     -DWITH_V4L=ON \
     -DCMAKE_BUILD_TYPE=RELEASE \
     -DCMAKE_INSTALL_PREFIX="$INSTALL_DIR"
make -j$NPROC && sudo make install

cd ../../
wget https://www.baslerweb.com/fp-1535524595/media/downloads/software/pylon_software/pylon_5.1.0.12682-deb0_amd64.deb
sudo dpkg -i pylon_5.1.0.12682-deb0_amd64.deb
