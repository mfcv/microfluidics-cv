#####################################################################
#  Dockerfile - This script make a Docker image to build MicrofluidicsCV
#
#  Copyright (C) 2019  Simon Dallaire
#
#  This program is free software: you can redistribute it and/or modify
#  it under the terms of the GNU Affero General Public License as
#  published by the Free Software Foundation, either version 3 of the
#  License, or (at your option) any later version.
#
#  This program is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#  GNU Affero General Public License for more details.
#
#  You should have received a copy of the GNU Affero General Public License
#  along with this program.  If not, see <https://www.gnu.org/licenses/>.
#
#####################################################################
FROM ubuntu:18.04

CMD ["/bin/bash"]

ARG DEBIAN_FRONTEND=noninteractive

###################################################################
# Custom Script
###################################################################

RUN apt-get update \
    && apt-get -y --no-install-recommends install \
         cmake g++-multilib libopencv-dev qtdeclarative5-dev \
         libboost-all-dev ccache gdbserver \
         wget dpkg grep ca-certificates gnupg \
    && apt-get -y --no-install-recommends install \
         qml-module-qtquick2 qml-module-qtquick-window2 \
         qml-module-qtquick-layouts qml-module-qtquick-controls \
         qml-module-qtqml-models2 qml-module-qtgraphicaleffects

########################################
# Inspired by: http://fabiorehm.com/blog/2014/09/11/running-gui-apps-with-docker/
# Replace 1000 with your user / group id
RUN export uid=1000 gid=1000 \
    && mkdir /run; mkdir /run/user; mkdir /run/user/1000 \
    && chmod 0700 /run/user/1000 \
    && mkdir -p /home/developer \
    && echo "developer:x:${uid}:${gid}:Developer,,,:/home/developer:/bin/bash" >> /etc/passwd \
    && echo "developer:x:${uid}:" >> /etc/group \
    chown ${uid}:${gid} -R /home/developer
########################################

# Install pylon if not found
RUN  wget --no-check-certificate https://www.baslerweb.com/fp-1535524595/media/downloads/software/pylon_software/pylon_5.1.0.12682-deb0_amd64.deb \
     && dpkg -i pylon_5.1.0.12682-deb0_amd64.deb \
     && rm pylon_5.1.0.12682-deb0_amd64.deb

#RUN echo "deb [arch=amd64] http://packages.microsoft.com/repos/vscode stable main" > /etc/apt/sources.list.d/vscode.list \
#    && apt-key adv --keyserver keyserver.ubuntu.com --recv-keys EB3E94ADBE1229CF \
#    && apt-get --allow-unauthenticated update \
#    && apt-get --allow-unauthenticated -y --no-install-recommends install \
#       code libxtst-dev libasound2-dev

###################################################################
# Clean up APT when done.
RUN apt-get -y clean \
    && rm -rf /var/lib/apt/lists/* /tmp/* /var/tmp/*
